<?php

/**
 * Loop Room title
 *
 * This template can be overridden by copying it to %theme%/hotel-booking/loop-room-type/title.php.
 *
 */

if ( !defined( 'ABSPATH' ) ) {
	exit;
}
?>


<?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>	

<ul class="loop_info block">
    <?php if( get_sub_field('basic_capacity') ): ?><li>
        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_guests_black.svg" alt="capacity">
        <?php the_sub_field('basic_capacity'); if( get_sub_field('spare_capacity') ): ?> + <?php the_sub_field('spare_capacity'); endif; ?>   
    </li><?php endif; ?>   
    <?php if( get_sub_field('beds') ): ?><li>
        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/bed.svg" alt="beds">
        <?php the_sub_field('beds'); if( get_sub_field('spare_beds') ): ?> + <?php the_sub_field('spare_beds'); endif; ?>   
    </li><?php endif; ?>   
    <?php if( get_sub_field('bathrooms') ): ?><li>
        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/shower.svg" alt="bathroom">
        <?php the_sub_field('bathrooms'); ?></li><?php endif; ?>   
    <?php if( get_sub_field('space') ): ?><li>
        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/room.svg" alt="room">
        <?php the_sub_field('space'); ?>m<sup>2</sup>
    </li><?php endif; ?>   
</ul>

<?php $linkClass = apply_filters( 'mphb_loop_room_type_title_link_class', 'mphb-room-type-title' ); ?>

<h6><?php esc_html( the_title() ); ?></h6>
<p class="mphb-regular-price">
    <?php if( get_sub_field('price') ): ?><span class="app_regular"><strong><?php if(ICL_LANGUAGE_CODE=='hr'): ?>Cijena od:<?php elseif(ICL_LANGUAGE_CODE=='en'): ?>Price from:<?php elseif(ICL_LANGUAGE_CODE=='de'): ?>Preis von:<?php elseif(ICL_LANGUAGE_CODE=='it'): ?>Prezzo da:<?php endif; ?></strong> <span class="mphb-price"><?php the_sub_field('price'); ?> <span class="mphb-currency">€</span></span></span><?php endif; ?>	
    <span class="app_longterm">Cijena na upit</span>
</p>

<a class="button" href="<?php esc_url( the_permalink() ); ?>"><?php if(ICL_LANGUAGE_CODE=='hr'): ?>Rezerviraj odmah<?php elseif(ICL_LANGUAGE_CODE=='en'): ?>Book now!<?php elseif(ICL_LANGUAGE_CODE=='de'): ?>Buchen Sie jetzt<?php elseif(ICL_LANGUAGE_CODE=='it'): ?>Prenota ora<?php endif; ?></a><?php endwhile; endif; ?>	