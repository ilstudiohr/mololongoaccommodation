<?php
if ( !defined( 'ABSPATH' ) ) {
	exit;
}
?>

<?php if ( has_post_thumbnail() ) : ?>	
	<div class="img_box">
        <a href="<?php esc_url( the_permalink() ); ?>">
			<?php mphb_tmpl_the_loop_room_type_thumbnail('thumbnail'); ?>
            <?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>	
            <span><?php the_sub_field('city'); ?></span>
            <?php endwhile; endif; ?>	
        </a>
	</div>
	<?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>	
    <span class="room_stars room_stars_left<?php the_sub_field('stars'); ?>"></span>
    <p class="loop_address"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/search_pin.svg" alt="pin">
	<?php the_sub_field('address'); ?>, <?php the_sub_field('city'); ?></p>
    <?php endwhile; endif; ?>	
    
    <?php else: ?>

<?php endif; ?>