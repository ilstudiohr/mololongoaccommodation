<?php
/**
 *
 * Avaialable variables
 * - DateTime $checkInDate
 * - DateTime $checkOutDate
 * - int $adults
 * - int $children
 * - bool $isShowGallery
 * - bool $isShowImage
 * - bool $isShowTitle
 * - bool $isShowExcerpt
 * - bool $isShowDetails
 * - bool $isShowPrice
 * - bool $isShowViewButton
 * - bool $isShowBookButton
 *
 * @version 2.0.0
 */
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

if ( post_password_required() ) {
	$isShowGallery = $isShowImage = $isShowDetails = $isShowPrice = $isShowViewButton = $isShowBookButton = false;
}

do_action( 'mphb_sc_search_results_before_room' );

$wrapperClass = apply_filters( 'mphb_sc_search_results_room_type_class', join( ' ', mphb_tmpl_get_filtered_post_class( 'mphb-room-type' ) ) );
?>
<li class="app-list_single">
    <?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>
    <div class="app-image">
        <div class="ribbon-left">
            <div><?php the_sub_field('price'); ?> <?php _e('eur/noć', 'molo'); ?></div>
        </div>
        <div class="swiper swiper_app-list">
            <div class="swiper-wrapper">
                <?php $images = get_field('room_slider'); if ($images) { $counter = 1; ?>
                    <?php foreach( $images as $image ) { ?>	
                    	<div class="swiper-slide">
                            <a href="<?php echo $image['url']; ?>" rel="lightbox">
                                 <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                            </a>
                        </div>
                    <?php $counter++; if ($counter == 4) { break; }} ?>
                <?php } ?>
                
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div>
        
    </div>
    <div class="app-content">
        <h3 class="app-title"><?php the_title(); ?></h3>
        <div class="app-info"><?php echo custom_excerpt(100); ?></div>
        <ul class="app-icons">
            <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/pin-color.svg" alt="<?php _e('Lokacija', 'molo'); ?>"> <?php the_sub_field('city'); ?>	</li>
            <?php if( get_sub_field('basic_capacity') ): ?>
            <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico_guests-color.svg" alt="<?php _e('Kapacitet', 'molo'); ?>"> <?php the_sub_field('basic_capacity'); if( get_sub_field('spare_capacity') ): ?> + <?php the_sub_field('spare_capacity'); endif; ?></li>
            <?php endif; ?>  
            <?php if( get_sub_field('beds') ): ?>
            <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico_bed-color.svg" alt="<?php _e('Broj kreveta', 'molo'); ?>"> <?php the_sub_field('beds'); if( get_sub_field('spare_beds') ): ?> + <?php the_sub_field('spare_beds'); endif; ?> </li>
            <?php endif; ?>
            <?php if( get_sub_field('bathrooms') ): ?>
            <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-bath.svg" alt="<?php _e('Broj kupaona', 'molo'); ?>"> <?php the_sub_field('bathrooms'); ?> </li>
            <?php endif; ?>
        </ul>
        <div class="app-button">
            <a href="<?php the_permalink() ?>" class="button-primary"><?php _e('Rezerviraj', 'molo'); ?></a>
        </div>
     </div>
     
    <?php endwhile; endif; ?>
</li>

<?php
do_action( 'mphb_sc_search_results_after_room' );
