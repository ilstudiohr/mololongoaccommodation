<?php
if ( !defined( 'ABSPATH' ) ) {
	exit;
}
?>

<?php get_header(); ?>	

<section class="app-top pb-0">
    <div class="container-fluid">
        <div class="row mobile-order small-gutters">
            <div class="col-md-8 mobile-one">
                <div class="app_feat-img">
                    <?php 
                    $images = get_field('room_slider');
                        if($images): ?>
                        <div class="gallery">   
                            <?php $i=0; foreach( $images as $image ) : ?>
                              <a href="<?php echo $image['url']; ?>" rel="lightbox" data-lightbox="gallery">
                                <?php if( $i==0 ) : ?>
                                    <?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?>
                                <?php endif; ?>
                              </a>
                            <?php $i++; endforeach; ?>
                        </div>
                    <?php endif; ?>
                    
                </div>
            </div>
            <div class="col-md-4 mobile-hidden">
                <div class="app-gallery">
                    <?php $images = get_field('room_slider'); if ($images) { $counter = 1; ?>
                        <?php foreach( $images as $image ) { ?>	
                        	<div class="gallery-side">
                                <a href="<?php echo $image['url']; ?>" rel="lightbox" data-lightbox="gallery">
                                     <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                                </a>
                            </div>
                        <?php $counter++; if ($counter == 5) { break; }} ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="pt-0">
    <div class="container-sm">
        <div class="row">
            <div class="col-md-12">
                <div class="info-box mt-n5">
                    <?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>	
                    <div class="info-box_single">
                        <?php if( get_sub_field('basic_capacity') ): ?>
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_guests.svg" alt="<?php _e('Kapacitet', 'molo'); ?>">
                            <span><?php the_sub_field('basic_capacity'); if( get_sub_field('spare_capacity') ): ?> + <?php the_sub_field('spare_capacity'); endif; ?><?php _e(' gosti', 'molo'); ?></span>
                        <?php endif; ?> 
                    </div>
                    <div class="info-box_single">
                        <?php if( get_sub_field('beds') ): ?>
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_bed.svg" alt="beds">
                            <span><?php the_sub_field('beds'); if( get_sub_field('spare_beds') ): ?> + <?php the_sub_field('spare_beds'); endif; ?><?php _e(' kreveti', 'molo'); ?></span>
                        <?php endif; ?> 
                    </div>
                    <div class="info-box_single">
                        <?php if( get_sub_field('bathrooms') ): ?>
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_bathroom.svg" alt="<?php _e('Kupaonica', 'molo'); ?>">
                            <span><?php the_sub_field('bathrooms'); ?><?php _e(' kupaonica', 'molo'); ?></span>
                        <?php endif; ?>
                    </div>
                    <div class="info-box_single">
                        <?php if( get_sub_field('space') ): ?>
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_villa_size.svg" alt="<?php _e('Soba', 'molo'); ?>">
                            <span><?php the_sub_field('space'); ?>m<sup>2</sup></span>
                        <?php endif; ?>  
                    </div>
                    <?php endwhile; endif; ?>
                    <div class="info-box_single price_box d-none">
                        <?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>
                            <span><?php _e('Cijena od', 'molo'); ?></span><?php the_sub_field('price'); ?> <?php _e('eur/noć', 'molo'); ?>
                        <?php endwhile; endif; ?>
                    </div>
                    <div class="info-box_single box_button">
 		    <?php if( have_rows('basic_info') ): ?>
                        <?php 
                             $rentlioUnitID = NULL;
                        ?>
                    <?php while ( have_rows('basic_info') ) : the_row(); 
                        $rentlioUnitID = get_sub_field('rentlioUnitID');
                        ?>
                    <?php endwhile; endif; ?>

                    <?php if($rentlioUnitID): ?>
                        <a href="#calendar"><?php _e('Rezerviraj', 'molo'); ?></a>
                    <?php else: ?>
                        <a href="#modal_contact"><?php _e('Pošalji upit', 'molo'); ?></a>
                    <?php endif; ?>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="row app-content mt-2">
            <div class="col-md-8">
                <div class="app-info pt-4">
                    <div class="mobile-two pt-xs-0">
                        <div class="apartment-title d-flex align-center">
                            <h1 class="h2"><?php the_title(); ?></h1>
                            <?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>	
                            <span class="ml-4 room_stars room_stars_left<?php the_sub_field('stars'); ?>"></span>
                            <?php endwhile; endif; ?>
                            <a href="<?php echo do_shortcode('[supsystic-show-popup id=100]');?>"><i class="fa fa-share-alt" aria-hidden="true"></i></a>

                        </div>
                    </div>
                    <p class="app-desc">
                        <?php the_content();?>
                    </p>
                </div>
                <div class="app-info pt-4">
                    <?php do_action( 'mphb_render_single_room_type_before_content' ); ?>
        
                    <?php
                    /**
                     * @hooked \MPHB\Views\SingleRoomTypeView::renderTitle				- 10
                     * @hooked \MPHB\Views\SingleRoomTypeView::renderFeaturedImage		- 20
                     * @hooked \MPHB\Views\SingleRoomTypeView::renderDescription		- 30
                     * @hooked \MPHB\Views\SingleRoomTypeView::renderPrice				- 40
                     * @hooked \MPHB\Views\SingleRoomTypeView::renderAttributes			- 50
                     * @hooked \MPHB\Views\SingleRoomTypeView::renderCalendar			- 60
                     * @hooked \MPHB\Views\SingleRoomTypeView::renderReservationForm	- 70
                     */
                    do_action( 'mphb_render_single_room_type_content' );
                    ?>
            
                    <?php do_action( 'mphb_render_single_room_type_after_content' ); ?>
                
                </div>
                
                
            </div>
            <div class="col-md-4">
                <div id="sidebar">
                    <div class="sidebar__inner">
                        <div class="sidebar-list pt-4">
                            <ul>
                                <?php if( have_rows('amenities') ): while ( have_rows('amenities') ) : the_row(); ?>
                                    <?php if( have_rows('sidebar_lista') ): while ( have_rows('sidebar_lista') ) : the_row(); ?>
                                        <?php if( get_sub_field('best_price') ): ?>
                                            <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-best_price.svg"/><span><?php _e('Best price guarantee!', 'molo'); ?></span></li>
                                        <?php endif; ?> 
                                    <?php endwhile; endif; ?>
                                <?php endwhile; endif; ?>
                                
                                <?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>
                                    <?php if( get_sub_field('address') ): ?>
                                        <li>
                                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_pin.svg" alt="room">
                                            <a href="#dd_map"><?php the_sub_field('address'); if( get_sub_field('city') ): ?>, <?php the_sub_field('city'); endif; ?> 
                                            <?php if( get_sub_field('floor') ): ?>, <?php the_sub_field('floor'); _e(' kat', 'molo'); endif; ?>
                                            <?php if( have_rows('amenities') ): while ( have_rows('amenities') ) : the_row(); ?>
                                                <?php if( get_sub_field('lift') ): ?>
                                    			<?php _e(' (Ima lift)', 'molo'); ?>
                                    			<?php endif; ?>    
                                            <?php endwhile; endif; ?>
                                            </a>
                                            
                                        </li>
                                    <?php endif; ?> 
                                <?php endwhile; endif; ?>
                                <?php if( have_rows('amenities') ): while ( have_rows('amenities') ) : the_row(); ?>
                                    <?php if( get_sub_field('parking') ): ?>
                                        <li>
                                        	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_parking.svg"/>
                                            <span><?php _e('Privatni parking', 'molo'); ?></span>
                                        </li>
                        			<?php endif; ?>  
                    			<?php endwhile; endif; ?>
                    			<?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>
                                    <?php if( get_sub_field('check_1') ): ?>
                                        <li>
                                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_clock.svg" />
                                            <span><?php _e('Check-in:', 'molo'); ?> <?php the_sub_field('check_1'); ?> <br><?php _e('Check-out:', 'molo'); ?> <?php the_sub_field('check_out'); ?></span>
                                        </li>
                                    <?php endif; ?>	
                                <?php endwhile; endif; ?>
                                <?php if( have_rows('amenities') ): while ( have_rows('amenities') ) : the_row(); ?>
                                    <?php if( have_rows('sidebar_lista') ): while ( have_rows('sidebar_lista') ) : the_row(); ?>
                                        <?php if( get_sub_field('self') ): ?>
                                            <li>
                                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_clock.svg" />
                                                <span><?php _e('Self check-in', 'molo'); ?> </span>
                                            </li>
                                        <?php endif; ?>	
                                        <?php
                                        $zabava = get_sub_field('dopustena_organizacija_zabava');
                                        if( $zabava && in_array('yes', $zabava) ) :?> 
                                            <li>
                                            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/zabava.svg"/>
                                                <span><?php _e('Dopuštena organizacija zabava', 'molo'); ?></span>
                                            </li>
                                        <?php elseif( $zabava && in_array('no', $zabava) ) :?> 
                                            <li>
                                            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/zabava.svg"/>
                                                <span><?php _e('Nije dopuštena organizacija zabava', 'molo'); ?></span>
                                            </li>
                                        <?php endif;?>
                                        
                                        <?php
                                        $ljubimci = get_sub_field('dopusteni_ljubimci');
                                        if( $ljubimci && in_array('yes', $ljubimci) ) :?> 
                                            <li>
                                            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ljubimci.svg"/>
                                                <span><?php _e('Dopušteni ljubimci', 'molo'); ?></span>
                                            </li>
                                        <?php elseif( $ljubimci && in_array('no', $ljubimci) ) :?> 
                                            <li>
                                            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ljubimci.svg"/>
                                                <span><?php _e('Nisu dopušteni ljubimci', 'molo'); ?></span>
                                            </li>
                                        <?php elseif( $ljubimci && in_array('upit', $ljubimci) ) :?> 
                                            <li>
                                            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ljubimci.svg"/>
                                                <span><?php _e('Ljubimci na upit', 'molo'); ?></span>
                                            </li>
                                        <?php endif;?>
                                        
                                        <?php
                                        $pusenje = get_sub_field('dopusteno_pusenje');
                                        if( $pusenje && in_array('yes', $pusenje) ) :?> 
                                            <li>
                                            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/pusenje.svg"/>
                                                <span><?php _e('Dopušteno pušenje', 'molo'); ?></span>
                                            </li>
                                        <?php elseif( $pusenje && in_array('no', $pusenje) ) :?> 
                                            <li>
                                            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/pusenje.svg"/>
                                                <span><?php _e('Nije dopušteno pušenje', 'molo'); ?></span>
                                            </li>
                                        <?php endif;?>

                                    <?php endwhile; endif; ?>
                                <?php endwhile; endif; ?>
                                <li>
                                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/phone.svg"/>
                                    <div class="d-flex flex-column">
                                        <span><?php _e('Trebate pomoć?', 'molo'); ?></span>
                                    <span>+385 91 600 6646</span>
                                    </div>
                                     
                                    
                                </li>
                            </ul>
                            <div class="sidebar-button_group">
 
                    		<?php if( have_rows('basic_info') ): ?>
                        	  <?php 
                             	    $rentlioUnitID = NULL;
                                    $isEnquiry = NULL;
                                  ?>
                       		<?php while ( have_rows('basic_info') ) : the_row(); 
                            	    $rentlioUnitID = get_sub_field('rentlioUnitID');
                                    $isEnquiry = get_sub_field('no_instant_booking');
                                ?>
                     		<?php endwhile; endif; ?>

                     		<?php if($rentlioUnitID && !$isEnquiry): ?>
            			   <a href="#calendar" class="button-primary" style="border-top-right-radius: 0;border-bottom-right-radius: 0;width: 100%;"><?php _e('Rezerviraj', 'molo'); ?></a>
                                   <button id="modal_contact" class="button-secondary" style="border-top-left-radius: 0;border-bottom-left-radius: 0;width: 100%;"><?php _e('Pošalji upit', 'molo'); ?></button>
                    		<?php else: ?>
                                <button id="modal_contact" class="button-secondary" style="width: 100%;"><?php _e('Pošalji upit', 'molo'); ?></button>                    <?php endif; ?>     
                            </div>
                            

                        </div>
                    
                    </div>
                </div>
                
                
            </div>
        </div>
    </div>
</section>        

<section>
                <?php if( have_rows('basic_info') ): ?>
                    <?php 
                         $rentlioUnitID = NULL;
			 $isEnquiry = NULL;
                    ?>
                <?php while ( have_rows('basic_info') ) : the_row(); 
                    $rentlioUnitID = get_sub_field('rentlioUnitID');
		    $isEnquiry = get_sub_field('no_instant_booking');
                    ?>
                <?php endwhile; endif; ?>

                <?php if($rentlioUnitID): ?>
                    <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>
    		    <div class="container-sm">
                        <div class="row">
                            <div class="col-md-12">
		                <div class="app-info pt-md-4">
              		             <h3 class="h4 single-title text-center"  id="calendar"><?php _e('Kalendar raspoloživosti', 'molo'); ?></h3>
            	                 </div>
	                     </div>
                        </div>
                    </div>
                    
                    <div class="ml-engine-calendar" isEnquiry="<?php if($isEnquiry) echo 'true' ?>" unitTypeId="<?php echo $rentlioUnitID ?>" unitImgSrc="<?php echo $url ?>" language="<?php if (ICL_LANGUAGE_CODE == 'en'): ?>en<?php elseif (ICL_LANGUAGE_CODE == 'hr'): ?>hr<?php elseif (ICL_LANGUAGE_CODE == 'de'): ?>de<?php elseif (ICL_LANGUAGE_CODE == 'it'): ?>it<?php endif; ?>"></div>
                <?php endif; ?>
    <div class="container-sm">
        <div class="row">
            <div class="col-md-12">
<!--         <div class="app-info pt-4">
               <h3 class="single-title"><?php _e('Kalendar raspoloživosti', 'molo'); ?></h3>
            </div>
        <div class="app-calendar text-center py-5">
            <?php echo do_shortcode("[mphb_availability_calendar monthstoshow='4']"); ?> 
        </div> --> 
       </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="app-info pt-5 mb-5">
                    <h3 class="single-title h4"><?php _e('Dodatne informacije', 'molo'); ?></h3>
                </div>
            </div>
            <?php if( get_field('dodatni_opis') ): ?>
                <div class="col-md-8">
                    <div class="app-info">
                        <div class="add-info ">
                            <p class="app-desc">
                                <?php the_field('dodatni_opis');?>
                            </p>
                        </div>
                    </div>
                </div>
            <?php endif; ?>	
            
            <div class="col-md-12">
                <div class="app-info">
                    <div class="sidebar-list distance-meta">
                        <div class="app-meta distance-meta">
                            <?php if( have_rows('amenities') ): while ( have_rows('amenities') ) : the_row(); ?>
                            <ul>
                                <?php if( get_sub_field('to_beach') ): ?>
                                <li>
                                	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_beach.svg"/>
                                	<span class="d-flex justify-content-between w-100"><span><?php _e('Plaža', 'molo'); ?></span> <span> <?php the_sub_field('to_beach'); ?></span></span>
                                </li>
                                <?php endif; ?>   
                                
                                <?php if( get_sub_field('to_restaurant') ): ?>
                                <li>
                                	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-pribor.svg"/>
                                	<span class="d-flex justify-content-between w-100"><span><?php _e('Restoran', 'molo'); ?></span> <span> <?php the_sub_field('to_restaurant'); ?></span></span>
                                </li>
                                <?php endif; ?>   
                                
                                <?php if( get_sub_field('to_atm') ): ?>
                                <li>
                                	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_atm.svg"/>
                                	<span class="d-flex justify-content-between w-100"><span><?php _e('Bankomat', 'molo'); ?></span> <span> <?php the_sub_field('to_atm'); ?></span></span>
                                </li>
                                <?php endif; ?>   
                                
                                <?php if( get_sub_field('to_parking') ): ?>
                                <li>
                                	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_parking.svg"/>
                                	<span class="d-flex justify-content-between w-100"><span><?php _e('Parking', 'molo'); ?></span> <span><?php the_sub_field('to_parking'); ?></span></span>
                                </li>
                                <?php endif; ?>   
                                
                                <?php if( get_sub_field('to_airport') ): ?>
                                <li>
                                	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_airport.svg"/>
                                	<span class="d-flex justify-content-between w-100"><span><?php _e('Aerodrom', 'molo'); ?></span> <span> <?php the_sub_field('to_airport'); ?></span></span>
                                </li>
                                <?php endif; ?> 
                            </ul>
                            <?php endwhile; endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="map-section">
    <div id="dd_map"></div>
</section>
<section class="app-list pt-0">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-4">
                <h3 class="single-title h4"><?php _e('Slične smještajne jedinice', 'molo'); ?></h3>
            </div>
            <div class="swiper swiper-featured pt-5 mt-n5 pb-4">
                <div class="swiper-wrapper">
                <?php 
                $terms = get_the_terms( $post->ID , 'mphb_room_type_category', 'string');
                $term_ids = wp_list_pluck($terms,'term_id');
                $args = array('posts_per_page' => 5, 'orderby' => 'menu_order', 'order' => 'ASC','post_type' => 'mphb_room_type','tax_query' => array(array('taxonomy' => 'mphb_room_type_category', 'field' => 'id','terms' => $term_ids, 'operator'=> 'IN'))); $new = new WP_Query( $args ); if ( have_posts() ) while ($new->have_posts()) : $new->the_post(); ?>
                    <div class="swiper-slide">
                        <div class="col-md-12">
                            <?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>
                            <div class="app-box">
            				    <div class="app-image">
            				        <div class="swiper swiper-id" id="post-<?php the_ID(); ?>">
            				            <div class="swiper-wrapper">
            				                <?php $images = get_field('room_slider'); if ($images) { $counter = 1; ?>
                                                <?php foreach( $images as $image ) { ?>	
            				                        <div class="swiper-slide">
            				                            <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
            				                        </div>
        				                        <?php $counter++; if ($counter == 10) { break; }} ?>
                                            <?php } ?>
                                            
            				            </div>
            				            <div class="swiper-pagination"></div>
            				            <div class="swiper-button-prev"></div>
                                            <div class="swiper-button-next"></div>
            				        </div>
      
            				        <span class="app-stars app_stars<?php the_sub_field('stars'); ?>"></span>
            				       <!-- <p class="product-price-or"><?php the_sub_field('price'); ?> <?php _e('eur/noć', 'molo'); ?></p>-->
            				    </div>
            				    <div class="app-content">
            				        <a class="link-abs" href="<?php the_permalink() ?>"></a>
            				        <div class="app-title">
            				            <h4><?php the_title(); ?></h4>
            				            <!--<span class="app-price"><?php the_sub_field('price'); ?> <?php _e('eur/noć', 'molo'); ?></span>-->
            				        </div>
            				        <div class="app-icons">
            				            <ul class="small-icons">
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/pin-color.svg" alt="<?php _e('Lokacija', 'molo'); ?>"> <?php the_sub_field('city'); ?>	</li>
            				                <?php if( get_sub_field('basic_capacity') ): ?>
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico_guests-color.svg" alt="<?php _e('Kapacitet', 'molo'); ?>"> <?php the_sub_field('basic_capacity'); if( get_sub_field('spare_capacity') ): ?> + <?php the_sub_field('spare_capacity'); endif; ?></li>
            				                <?php endif; ?>  
            				                <?php if( get_sub_field('beds') ): ?>
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico_bed-color.svg" alt="<?php _e('Broj kreveta', 'molo'); ?>"> <?php the_sub_field('beds'); if( get_sub_field('spare_beds') ): ?> + <?php the_sub_field('spare_beds'); endif; ?> </li>
            				                <?php endif; ?>
            				                <?php if( get_sub_field('bathrooms') ): ?>
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/bath.svg" alt="<?php _e('Broj kupaona', 'molo'); ?>"> <?php the_sub_field('bathrooms'); ?> </li>
            				                <?php endif; ?>
            				            </ul>
            				        </div>
            				        <div class="app-short">
            				            <p><?php echo excerpt(22); ?></p>
            				        </div>
            				        <div class="app-button">
            				            <a href="<?php the_permalink() ?>" class="button-primary"><?php _e('Rezerviraj', 'molo'); ?></a>
            				        </div>
            				    </div>
            				    
            				</div>
            				<?php endwhile; endif; ?>
                        </div>
                    </div>
                <?php endwhile; wp_reset_query(); ?>
              </div>
            
              <!-- If we need navigation buttons -->
              <div class="swiper-button-prev pos-tr"></div>
              <div class="swiper-button-next pos-tr"></div>

            </div>
        </div>
    </div>
</section>

<div class="mobile-action">
    <a href="#calendar"><?php _e('Rezerviraj', 'molo'); ?></a>
</div>
<!-- The Modal -->
<div id="modal_content" class="modal_wrapper">

  <!-- Modal content -->
  <div class="modal_content">
    <span class="close">&times;</span>
    <h6><?php _e('Kontaktirajte nas', 'molo'); ?></h6>
    <p><?php _e('Imate li pitanje vezano uz rezervaciju apartmana ili njegovu dostupnost? Kontaktirajte nas i odgovorit ćemo vam u najkraćem mogućem roku', 'molo'); ?></p>
    <div class="app-form-content">
        <div class="img-app">
            <img src="<?php the_post_thumbnail_url('thumbnail'); ?>" alt="<?php the_title(); ?>" />
        </div>
        <div class="app-content">
            <?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>	
            <div class="modal_info">
                <span class="room_stars room_stars_left<?php the_sub_field('stars'); ?>"></span>
                <h4><?php the_title(); ?></h4>
                
                <span><?php the_sub_field('address'); ?>, <?php the_sub_field('city'); ?></span>
                
            </div><?php endwhile; endif; ?>
        </div>
    </div>
    <?php if(ICL_LANGUAGE_CODE=='hr'): ?>
    <?php echo do_shortcode('[contact-form-7 id="853796" title="Upit"]'); ?>
    <?php elseif(ICL_LANGUAGE_CODE=='en'): ?>
    <?php echo do_shortcode('[contact-form-7 id="856279" title="Upit en"]'); ?>
    <?php elseif(ICL_LANGUAGE_CODE=='de'): ?>
    <?php echo do_shortcode('[contact-form-7 id="856303" title="Upit de"]'); ?>
    
    <?php elseif(ICL_LANGUAGE_CODE=='it'): ?>
    <?php echo do_shortcode('[contact-form-7 id="856304" title="Upit it"]'); ?>
    <?php endif; ?>
    	
    
  </div>

</div>
<?php

/**

 * @hooked \MPHB\Views\SingleRoomTypeView::renderPageWrapperEnd - 10

 */

do_action( 'mphb_render_single_room_type_wrapper_end' );

?>
<script>
var modal = document.getElementById("modal_content");
var btn = document.getElementById("modal_contact");
var span = document.getElementsByClassName("close")[0];
btn.onclick = function() {
  modal.style.display = "flex";
}
span.onclick = function() {
  modal.style.display = "none";
}
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCxNs-xT0afx_k-D69SLU8dvmn5oPnKkFE"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/markerclusterer.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/sticky-sidebar.min.js"></script>
<script>
var sidebar = new StickySidebar('#sidebar', {
        containerSelector: '.app-content',
        innerWrapperSelector: '.sidebar__inner',
        topSpacing: 20,
        bottomSpacing: 20
    });
</script>

<script>
    function initMap(){
          // map options
          var options = {
            zoom:10,
            center:{<?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>lat:<?php the_sub_field('latitude'); ?>, lng:<?php the_sub_field('longitude'); ?><?php endwhile; endif; ?>},
			mapTypeId: "roadmap",
    		// disableDefaultUI: true,
			styles: [

				{
					featureType: "all",
					elementType: "all",
					stylers: [
					  { "saturation": -100 }
					]
				},
				{
					"featureType": "landscape",
					"elementType": "all",
					"stylers": [
						{ "saturation": -100 },
						{ "lightness": 65 },
						{ "visibility": "on" }
					]
				},				
				
				{
					"featureType": "water",
					"elementType": "geometry",
					"stylers": [
						{ "hue": "#ffff00" },
						{ "lightness": -10 },
						{ "saturation": -100 }
					]
				},
				{
					"featureType": "water",
					"elementType": "labels",
					"stylers": [
						{ "lightness": -25 },
						{ "saturation": -100 }
					]
				}
			]
          }
    var map = new google.maps.Map(document.getElementById('dd_map'),options);

          var markers = [
				<?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>	
						<?php if( get_sub_field('latitude') ): ?>{
						  coords:{lat:<?php the_sub_field('latitude'); ?>, lng:<?php the_sub_field('longitude'); ?>},
						  iconImage:'<?php echo esc_url(get_template_directory_uri()); ?>/images/map_pin.png',
						  content:
							'<p class="dd_map_title"><?php the_title(); ?></p>' +
							'<p class="dd_map_p small-icons"><span class="room_stars room_stars_left<?php the_sub_field('stars'); ?>"></span></p>' +
							'<p class="dd_map_p small-icons"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/pin-color.svg" /><?php the_sub_field('address'); ?>, <?php the_sub_field('city'); ?></p>' +
							'<p class="dd_map_p small-icons align-items-center d-flex">' +
							
							
							'<span class="dd_map_half"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico_guests-color.svg" /><?php the_sub_field('basic_capacity'); if( get_sub_field('spare_capacity') ): ?> &#43; <?php the_sub_field('spare_capacity'); endif; ?></span><span class="ml-2 dd_map_half"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico_bed-color.svg" /><?php the_sub_field('beds'); if( get_sub_field('spare_beds') ): ?> + <?php the_sub_field('spare_beds'); endif; ?></span>' +
							
							'</p>'+
							'<p class="dd_map_p">Cijena od: <strong><?php the_sub_field('price'); ?> € <small>za 3 noći</small></strong></p>' +
							'<div style="padding:0 18px 10px 18px"><a class="button dd_map_button dd_map_button2" target="_blank" href="<?php the_sub_field('google_map'); ?>" target="_blank">Pogledaj na karti</a></p>' 
							
						},<?php endif; ?>
				<?php endwhile; endif; ?>	
          ];
          // Loop through markers
          var gmarkers = [];
          for(var i = 0; i < markers.length; i++){
            gmarkers.push(addMarker(markers[i]));

          }

          //Add MArker function
          function addMarker(props){
            var marker = new google.maps.Marker({
              position:props.coords,
              map:map,

            });

            if(props.iconImage){
              marker.setIcon(props.iconImage);
            }

            //Check content
            if(props.content){
              var infoWindow = new google.maps.InfoWindow({
                content:props.content
              });
              marker.addListener('click',function(){
                infoWindow.open(map,marker);
              });
            }
            return marker;
          }
		  
        var markerCluster = new MarkerClusterer(map, gmarkers, 
          {
            // for default marker sizes (m1, m2, m3...)
			//imagePath: '<?php echo esc_url(get_template_directory_uri()); ?>/images/m'
			
			styles:[{
			
				url: "<?php echo esc_url(get_template_directory_uri()); ?>/images/m5.png",
					width: 90,
					height:90,
					fontFamily:"Nunito Sans",
					textSize:16,
					textColor:"#ffffff",
					//color: #00FF00,
			}]			
			
          });
		  
        }
    google.maps.event.addDomListener(window, 'load', initMap);
</script>
<script>
    jQuery(function($){
    function getLocation(){
        if(navigator.geolocation){
            navigator.geolocation.getCurrentPosition(getWeather);
        }else{
            alert("Geolocation not supported by this browser");
        }
    }
    function getWeather(position){
        let lat = '45.337620';
        let long = '14.305196';
        let API_KEY = '2398e46ce4a606d494453c5342cc0b97';
        let baseURL = `https://api.openweathermap.org/data/2.5/onecall?&lat=${lat}&lon=${long}&appid=${API_KEY}`;
        
        

        $.get(baseURL,function(res){
            let data = res.current;
            let temp = Math.floor(data.temp - 273);
            let condition = data.weather[0].description;

            $('#temp-main').html(`${temp}°`);
            $('#condition').html(condition);
        })
        
    }

    getLocation();
})
</script>
<script
      type="text/javascript"
      src="https://booking.mololongoaccommodation.com/build/engine.js"
      async
  ></script>
<?php get_footer(); ?>