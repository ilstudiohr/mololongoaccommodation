<?php



if ( !defined( 'ABSPATH' ) ) {

	exit;

}

?>
    <h3 class="h4 pb-5"><?php _e('Sadržaji', 'molo'); ?></h3>
    <?php if( have_rows('amenities') ): while ( have_rows('amenities') ) : the_row(); ?>
    <?php
        $counter = '0';
    ;?>
    <div class="app-meta grid-meta">
        <ul class="single-app">
            	
            <?php if( have_rows('opcenito') ): while ( have_rows('opcenito') ) : the_row(); ?>
            <?php if( get_sub_field('airco') ): ?>
            <li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-airco.svg"/>
            	<span><?php _e('Klima', 'molo'); ?></span>	
            </li>
            <?php endif; ?>   
            <?php if( get_sub_field('wi_fi') ): ?>
            <li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_wifi.svg"/>
                <span><?php _e('Wi-Fi', 'molo'); ?></span>
            </li>
            <?php endif; ?>
            <?php if( get_sub_field('sauna') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_sauna.svg"/>
                <span><?php _e('Sauna', 'molo'); ?></span>
            </li>
			<?php endif; ?>
			<?php if( get_sub_field('jacuzzi') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_jacuzy.svg"/>
                <span><?php _e('Jacuzzi', 'molo'); ?></span>
            </li>
			<?php endif; ?> 
			<?php if( get_sub_field('balkon') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-balcony.svg"/>
				<span><?php _e('Balkon', 'molo'); ?></span>
            </li>
			<?php endif; ?>   
            
            <?php if( get_sub_field('terasa') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-terrace.svg"/>
				<span><?php _e('Terasa', 'molo'); ?></span>
            </li>
			<?php endif; ?> 
			<?php if( get_sub_field('loggia') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-loggia.svg"/>
				<span><?php _e('Loggia', 'molo'); ?></span>
            </li>
			<?php endif; ?> 
			<?php if( get_sub_field('posteljina') ): ?>
            <li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-sheet.svg"/>
            	<span><?php _e('Posteljina', 'molo'); ?></span>
            </li>
            <?php endif; ?>	
            <?php if( get_sub_field('radni_stol') ): ?>
            <li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-desk.svg"/>
            	<span><?php _e('Radni stol', 'molo'); ?></span>
            </li>
            <?php endif; ?>	
            <?php if( get_sub_field('privatni_ulaz') ): ?>
            <li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-private-door.svg"/>
            	<span><?php _e('Privatni ulaz', 'molo'); ?></span>
            </li>
            <?php endif; ?>	
            <?php if( get_sub_field('iron') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-iron.svg"/>
                <span><?php _e('Glačalo', 'molo'); ?></span>
            </li>
			<?php endif; ?> 
			<?php if( get_sub_field('lift') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-elevator.svg"/>
                <span><?php _e('Lift', 'molo'); ?></span>
            </li>
			<?php endif; ?>
			<?php if( get_sub_field('ormar') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-closet.svg"/>
                <span><?php _e('Ormar', 'molo'); ?></span>
            </li>
			<?php endif; ?>
			<?php if( get_sub_field('sef') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-vault.svg"/>
                <span><?php _e('Sef', 'molo'); ?></span>
            </li>
			<?php endif; ?>
			<?php if( get_sub_field('kutija_prve_pomoci') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-aid.svg"/>
                <span><?php _e('Kutija prve pomoci', 'molo'); ?></span>
            </li>
			<?php endif; ?>
			<?php if( get_sub_field('mini_hladnjak') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-minibar.svg"/>
                <span><?php _e('Mini hladnjak', 'molo'); ?></span>
            </li>
			<?php endif; ?>
			<?php if( get_sub_field('vanjski_namjestaj') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-furniture.svg"/>
                <span><?php _e('Vanjski namještaj', 'molo'); ?></span>
            </li>
			<?php endif; ?>
			<?php if( get_sub_field('suncobran') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-parasol.svg"/>
                <span><?php _e('Suncobran', 'molo'); ?></span>
            </li>
			<?php endif; ?>
			<?php if( get_sub_field('djecji_krevetic') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-baby.svg"/>
                <span><?php _e('Dječji krevetić', 'molo'); ?></span>
            </li>
			<?php endif; ?>
			<?php if( get_sub_field('rostilj') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-barbercue.svg"/>
                <span><?php _e('Roštilj', 'molo'); ?></span>
            </li>
			<?php endif; ?>
			<?php if( get_sub_field('vanjska_kuhinja') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-outdoor-kitchen.svg"/>
                <span><?php _e('Vanjska kuhinja', 'molo'); ?></span>
            </li>
			<?php endif; ?>
			<?php if( get_sub_field('prostor_za_sjedenje') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-seating.svg"/>
                <span><?php _e('Prostor za sjedenje', 'molo'); ?></span>
            </li>
			<?php endif; ?>
			<?php if( get_sub_field('pool') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_pool.svg"/>
                <span><?php _e('Vanjski bazen', 'molo'); ?></span>
            </li>
			<?php endif; ?>
			<?php if( get_sub_field('indoor_pool') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_pool_inside.svg"/>
                <span><?php _e('Unutarnji bazen', 'molo'); ?></span>
            </li>
			<?php endif; ?>
			<?php if( get_sub_field('privatni_bazen') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_pool.svg"/>
                <span><?php _e('Privatni bazen', 'molo'); ?></span>
            </li>
			<?php endif; ?>
			<?php if( get_sub_field('zajednicki_bazen') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_pool.svg"/>
                <span><?php _e('Zajednički bazen', 'molo'); ?></span>
            </li>
			<?php endif; ?>
			<?php if( get_sub_field('grijani_bazen') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_pool_hot.svg"/>
                <span><?php _e('Grijani bazen', 'molo'); ?></span>
            </li>
			<?php endif; ?>
			<?php if( get_sub_field('lezaljke') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_sunbed.svg"/>
                <span><?php _e('Ležaljke', 'molo'); ?></span>
            </li>
			<?php endif; ?>
			<?php if( get_sub_field('stalak_za_susenje_odjece') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-clothes.svg"/>
                <span><?php _e('Stalak za sušenje odjeće', 'molo'); ?></span>
            </li>
			<?php endif; ?>
			<?php if( get_sub_field('ugradbeni_ormar') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-builtin.svg"/>
                <span><?php _e('Ugradbeni ormar', 'molo'); ?></span>
            </li>
			<?php endif; ?>
			
			<?php if( get_sub_field('kamin') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-fireplace.svg"/>
                <span><?php _e('Kamin', 'molo'); ?></span>
            </li>
			<?php endif; ?>
			<?php if( get_sub_field('sredstva_za_ciscenje') ): ?>
			<li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-cleaning.svg"/>
                <span><?php _e('Sredstva za čišćenje', 'molo'); ?></span>
            </li>
			<?php endif; ?>
			<?php if( get_sub_field('toaster') ): ?>
            <li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-toaster.svg"/>
            	<span><?php _e('Toster', 'molo'); ?></span>
            </li>
            <?php endif; ?> 
            <?php endwhile; endif; ?>
<!--
            <?php if( get_sub_field('double') ): ?>
                <li>
        	        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_bed.svg"/>
                    <span><?php _e('Bračni krevet', 'molo'); ?></span>
                </li>
            <?php endif; ?>
            
            <?php if( get_sub_field('double-2') ): ?>
                <li>
        	        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_bed.svg"/>
                    <span><?php _e('2 bračna kreveta', 'molo'); ?></span>
                </li>
            <?php endif; ?>
            
            <?php if( get_sub_field('double-3') ): ?>
                <li>
        	        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_bed.svg"/>
                    <span><?php _e('3 bračna kreveta', 'molo'); ?></span>
                </li>
            <?php endif; ?>
            
            <?php if( get_sub_field('single') ): ?>
                <li>
        	        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_bed.svg"/>
                    <span><?php _e('Jednostruki krevet', 'molo'); ?></span>
                </li>
            <?php endif; ?>
            
            <?php if( get_sub_field('single-2') ): ?>
                <li>
        	        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_bed.svg"/>
                    <span><?php _e('Dva jednostruka krevet', 'molo'); ?></span>
                </li>
            <?php endif; ?>
            
            <?php if( get_sub_field('single-3') ): ?>
                <li>
        	        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_bed.svg"/>
                    <span><?php _e('Tri jednostruka krevet', 'molo'); ?></span>
                </li>
            <?php endif; ?>
            
            <?php if( get_sub_field('bunk_bed') ): ?>
                <li>
        	        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-bunk-bed.svg"/>
                    <span><?php _e('Krevet na kat', 'molo'); ?></span>
                </li>
            <?php endif; ?>
            
            <?php if( get_sub_field('bunk_bed-2') ): ?>
                <li>
        	        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-bunk-bed.svg"/>
                    <span><?php _e('Dva kreveta na kat', 'molo'); ?></span>
                </li>
            <?php endif; ?>
            
            
            
            <?php if( get_sub_field('couch_extend-2') ): ?>
                <li>
        	        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-couch.svg"/>
                    <span><?php _e('Dva kauča na razvlačenje', 'molo'); ?></span>
                </li>
            <?php endif; ?>
            
            <?php if( get_sub_field('couch-2') ): ?>
            <li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-couch.svg"/>
                <span><?php _e('2 kauča', 'molo'); ?></span>
            </li>
            <?php endif; ?>     
            
            <?php if( get_sub_field('sofa-2') ): ?>
            <li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-sofa.svg"/>
            	<span><?php _e('2 sofe na razvlačenje', 'molo'); ?></span>
            </li>
            <?php endif; ?>     
            
            <?php if( get_sub_field('corner_set') ): ?>
            <li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-corner-set.svg"/>
            	<span><?php _e('Kutna garnitura', 'molo'); ?></span>
            </li>
            <?php endif; ?>

            <?php if( get_sub_field('stove') ): ?>
            <li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-stove.svg"/>
            	<span><?php _e('Električni štednjak', 'molo'); ?></span>
            </li>
            <?php endif; ?>     
            
            <?php if( get_sub_field('stove_gas') ): ?>
            <li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-stove-gas.svg"/>
            	<span><?php _e('Plinski štednjak', 'molo'); ?></span>
            </li>
            <?php endif; ?>     
            
            <?php if( get_sub_field('stove_gas_el') ): ?>
            <li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-stove-gas-el.svg"/>
            	<span><?php _e('Plinsko-električni štednjak', 'molo'); ?></span>
            </li>
            <?php endif; ?>  
            <?php if( get_sub_field('plates') ): ?>
            <li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-hotplate.svg"/>
            	<span><?php _e('Ploče za kuhanje', 'molo'); ?></span>
            </li>
            <?php endif; ?>     
            
            <?php if( get_sub_field('coffee') ): ?>
            <li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-coffee.svg"/>
            	<span><?php _e('Aparat za kavu', 'molo'); ?></span>
            </li>
            <?php endif; ?>     
            
            <?php if( get_sub_field('parking') ): ?><li>
            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_parking.svg"/>
                <span><?php _e('Privatni parking', 'molo'); ?></span>
            </li>
			<?php endif; ?>  -->
                    
        </ul>
        
        
    </div>
        <div class="my-4 text-center">
        <button class="button amenities modal-toggle"><?php _e('Prikaži sve', 'molo'); ?></button>
    </div>
    <div class="modal--or app-meta">
    <div class="modal-overlay modal-toggle"></div>
    <div class="modal-wrapper modal-transition">
        <div class="modal-header">
            <h2><?php _e('Sadržaj', 'molo'); ?></h2>
        </div>
        <div class="modal-body">
            <div class="modal-content">
                <div class="modal_list grid-meta">
                    <?php if( have_rows('kuhinja') ): while ( have_rows('kuhinja') ) : the_row(); ?>
                     <div class="sep-title h5"><?php _e('Kuhinja', 'molo'); ?></div>
                        <ul class="amenities_content single-app simple-list grid-three">
                            <?php if( get_sub_field('kuhinja_cajna_kuhinja') ): ?>
                                <li>
                                	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/kitchen.svg"/>
                                	<span><?php _e('Kuhinja / čajna kuhinja', 'molo'); ?></span>
                                </li>
                            <?php endif; ?>
                            <?php if( get_sub_field('pribor') ): ?>
                                <li>
                                	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/pribor.svg"/>
                                	<span><?php _e('Kuhinjski pribor', 'molo'); ?></span>
                                </li>
                            <?php endif; ?> 
                            <?php if( get_sub_field('owen') ): ?>
                                <li>
                                	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-oven.svg"/>
                                	<span><?php _e('Pećnica', 'molo'); ?></span>
                                </li>
                            <?php endif; ?> 
                            <?php if( get_sub_field('stednjak') ): ?>
                                <li>
                                	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/stednjak.svg"/>
                                	<span><?php _e('Štednjak', 'molo'); ?></span>
                                </li>
                            <?php endif; ?>
                            <?php if( get_sub_field('dishwasher') ): ?>
                                <li>
                                	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-dishwasher.svg"/>
                                	<span><?php _e('Perilica posuđa', 'molo'); ?></span>
                                </li>
                            <?php endif; ?>  
                            <?php if( get_sub_field('microwave') ): ?>
                                <li>
                                	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-microwave.svg"/>
                                	<span><?php _e('Mikrovalna pećnica', 'molo'); ?></span>
                                </li>
                            <?php endif; ?>
                            <?php if( get_sub_field('nespresso') ): ?>
                                <li>
                                	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/nespresso.svg"/>
                                	<span><?php _e('Nespresso', 'molo'); ?></span>
                                </li>
                            <?php endif; ?>
                            <?php if( get_sub_field('stol_za_blagovaonicu') ): ?>
                                <li>
                                	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/dining-table.svg"/>
                                	<span><?php _e('Stol za blagovaonicu', 'molo'); ?></span>
                                </li>
                            <?php endif; ?>
                            <?php if( get_sub_field('kettle') ): ?>
                                <li>
                                	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-kattle.svg"/>
                                	<span><?php _e('Kuhalo za vodu', 'molo'); ?></span>
                                </li>
                            <?php endif; ?> 
                            <?php if( get_sub_field('cooler') ): ?>
                                <li>
                                	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-fridge.svg"/>
                                	<span><?php _e('Hladnjak', 'molo'); ?></span>
                                </li>
                            <?php endif; ?>
                        </ul>
                    <?php endwhile; endif; ?>
                    <?php if( have_rows('kupaonica') ): while ( have_rows('kupaonica') ) : the_row(); ?>
                    <div class="sep-title h5"><?php _e('Kupaonica', 'molo'); ?></div>
                        <ul class="amenities_content single-app simple-list grid-three">
                            <?php if( get_sub_field('bath') ): ?>
                			<li>
                            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-bath.svg"/>
                                <span><?php _e('Kada', 'molo'); ?></span>
                            </li>
            			<?php endif; ?> 
            			<?php if( get_sub_field('rucnici') ): ?>
                			<li>
                            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-rucnici.svg"/>
                                <span><?php _e('Ručnici', 'molo'); ?></span>
                            </li>
            			<?php endif; ?>
            			<?php if( get_sub_field('hairdryer') ): ?>
            			<li>
                        	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-hairdrier.svg"/>
                            <span><?php _e('Sušilo za kosu', 'molo'); ?></span>
                        </li>
            			<?php endif; ?> 
            			<?php if( get_sub_field('vlastita_kupaonica') ): ?>
            			<li>
                        	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon-ownbathroom.svg"/>
                            <span><?php _e('Vlastita kupaonica', 'molo'); ?></span>
                        </li>
            			<?php endif; ?> 
            			<?php if( get_sub_field('shower') ): ?>
                        <li>
                        	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-shower.svg"/>
                            <span><?php _e('Tuš', 'molo'); ?></span>
                        </li>
            			<?php endif; ?>  
            			<?php if( get_sub_field('washing_machine') ): ?>
            			<li>
                        	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-washer.svg"/>
                            <span><?php _e('Perilica rublja', 'molo'); ?></span>
                        </li>
            			<?php endif; ?> 
            			<?php if( get_sub_field('dodatni_wc') ): ?>
            			<li>
                        	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/wc.svg"/>
                            <span><?php _e('Dodatni WC', 'molo'); ?></span>
                        </li>
            			<?php endif; ?>
                        </ul>
                    <?php endwhile; endif; ?>
                    
                    <?php if( have_rows('dnevni_boravak') ): while ( have_rows('dnevni_boravak') ) : the_row(); ?>
                    <div class="sep-title h5"><?php _e('Dnevna soba', 'molo'); ?></div>
                        <ul class="amenities_content single-app simple-list grid-three">
                            <?php if( get_sub_field('tv') ): ?>
                            <li>
                            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon-tv.svg"/>
                            	<span><?php _e('TV', 'molo'); ?></span>
                            </li>
                            <?php endif; ?>
                            <?php if( get_sub_field('couch_extend') ): ?>
                            <li>
                    	        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-couch.svg"/>
                                <span><?php _e('Kauč na razvlačenje', 'molo'); ?></span>
                            </li>
                            <?php endif; ?>
                            <?php if( get_sub_field('couch') ): ?>
                            <li>
                            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-couch2.svg"/>
                                <span><?php _e('Kauč', 'molo'); ?></span>
                            </li>
                            <?php endif; ?>
                            <?php if( get_sub_field('sofa') ): ?>
                            <li>
                            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-sofa.svg"/>
                                <span><?php _e('Sofa na razvlačenje', 'molo'); ?></span>
                            </li>
                            <?php endif; ?>
                            <?php if( get_sub_field('satelitska') ): ?>
                            <li>
                            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon-satelite.svg"/>
                            	<span><?php _e('Satelitska', 'molo'); ?></span>
                            </li>
                            <?php endif; ?>
                            <?php if( get_sub_field('netflix') ): ?>
                            <li>
                            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-netflix.svg"/>
                            	<span><?php _e('Netflix', 'molo'); ?></span>
                            </li>
                            <?php endif; ?> 
                        </ul>
                    <?php endwhile; endif; ?> 

                </div>
                <button class="button-primary search-filter-submit modal-toggle mt-4"><?php _e('Zatvori', 'molo'); ?></button>
            </div>
        </div>
    </div>
</div>
    <?php endwhile; endif; ?>

<script>
jQuery(function($){
  $('.modal-toggle').on('click', function(e) {
    e.preventDefault();
    $('.modal--or').toggleClass('is-visible');
    $('.modal-overlay').toggleClass('is-visible');
    });
    });
</script> 
    