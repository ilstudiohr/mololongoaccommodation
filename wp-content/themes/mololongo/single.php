<?php
/**
 * The template for displaying all single posts
 *
 * @package MoloLongo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'mololongo_container_type' );
?>
<?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>
<section class="small-hero" style="background: url('<?php echo $backgroundImg[0]; ?>') ;background-position:center center;background-repeat:no-repeat;background-size:cover;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="hero-title text-center">
                    <h1><?php the_title();?></h1>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="center-section">
    <div class="container-sm">
        <div class="row">
            <div class="col-md-12">
                <div class="meta-box mb-5">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="single-meta-box">
                                <span><?php _e('Datum', 'molo'); ?></span>
                                <span><?php the_date();?></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="single-meta-box">
                                <span><?php _e('Podijeli', 'molo'); ?></span>
                                <?php echo do_shortcode('[supsystic-social-sharing id="2"]') ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
				while ( have_posts() ) {
					the_post();
					the_content(); 
				}
				?>
            </div>
        </div>
    </div>
</section>
<section class="sep">
    <div class="container-sm">
        <div class="row">
            <div class="col-md-12">
                <div class="separator"></div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container-sm related-posts">
        <div class="row">
            <div class="col-md-12 text-center">
                <span class="over-title">MOLO LONGO</span>
                <h4><?php _e('Pročitajte još i ovo', 'molo'); ?></h4>
            </div>
            <?php
            $related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 3, 'post__not_in' => array($post->ID) ) );
            if( $related ) foreach( $related as $post ) {
            setup_postdata($post); ?>
            <div class="col-md-4">
            <div class="blog-post mb-3">
                <div class="post-img">
                    <?php the_post_thumbnail();?>
                </div>
                <div class="post-content">
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <p class="mb-4"><?php echo excerpt(20);?></p>
                    <a class="simple-read" href="<?php the_permalink(); ?>"><?php _e('Pročitaj', 'molo'); ?></a>
                </div>
                <div class="post-date">
                    <?php echo get_the_date(''); ?>
                </div>
            </div>
            </div>
            <?php }
            wp_reset_postdata(); ?>
        </div>
    </div>
</section>

<?php
get_footer();
