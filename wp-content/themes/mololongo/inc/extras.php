<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package MoloLongo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

add_filter( 'body_class', 'mololongo_body_classes' );

if ( ! function_exists( 'mololongo_body_classes' ) ) {
	/**
	 * Adds custom classes to the array of body classes.
	 *
	 * @param array $classes Classes for the body element.
	 *
	 * @return array
	 */
	function mololongo_body_classes( $classes ) {
		// Adds a class of group-blog to blogs with more than 1 published author.
		if ( is_multi_author() ) {
			$classes[] = 'group-blog';
		}
		// Adds a class of hfeed to non-singular pages.
		if ( ! is_singular() ) {
			$classes[] = 'hfeed';
		}

		return $classes;
	}
}

if ( function_exists( 'mololongo_adjust_body_class' ) ) {
	/*
	 * mololongo_adjust_body_class() deprecated in v0.9.4. We keep adding the
	 * filter for child themes which use their own mololongo_adjust_body_class.
	 */
	add_filter( 'body_class', 'mololongo_adjust_body_class' );
}

// Filter custom logo with correct classes.
add_filter( 'get_custom_logo', 'mololongo_change_logo_class' );

if ( ! function_exists( 'mololongo_change_logo_class' ) ) {
	/**
	 * Replaces logo CSS class.
	 *
	 * @param string $html Markup.
	 *
	 * @return string
	 */
	function mololongo_change_logo_class( $html ) {

		$html = str_replace( 'class="custom-logo"', 'class="img-fluid"', $html );
		$html = str_replace( 'class="custom-logo-link"', 'class="navbar-brand custom-logo-link"', $html );
		$html = str_replace( 'alt=""', 'title="Home" alt="logo"', $html );

		return $html;
	}
}

if ( ! function_exists( 'mololongo_post_nav' ) ) {
	/**
	 * Display navigation to next/previous post when applicable.
	 */
	function mololongo_post_nav() {
		// Don't print empty markup if there's nowhere to navigate.
		$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
		$next     = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous ) {
			return;
		}
		?>
		<nav class="container navigation post-navigation">
			<h2 class="sr-only"><?php esc_html_e( 'Post navigation', 'mololongo' ); ?></h2>
			<div class="row nav-links justify-content-between">
				<?php
				if ( get_previous_post_link() ) {
					previous_post_link( '<span class="nav-previous">%link</span>', _x( '<i class="fa fa-angle-left"></i>&nbsp;%title', 'Previous post link', 'mololongo' ) );
				}
				if ( get_next_post_link() ) {
					next_post_link( '<span class="nav-next">%link</span>', _x( '%title&nbsp;<i class="fa fa-angle-right"></i>', 'Next post link', 'mololongo' ) );
				}
				?>
			</div><!-- .nav-links -->
		</nav><!-- .navigation -->
		<?php
	}
}

if ( ! function_exists( 'mololongo_pingback' ) ) {
	/**
	 * Add a pingback url auto-discovery header for single posts of any post type.
	 */
	function mololongo_pingback() {
		if ( is_singular() && pings_open() ) {
			echo '<link rel="pingback" href="' . esc_url( get_bloginfo( 'pingback_url' ) ) . '">' . "\n";
		}
	}
}
add_action( 'wp_head', 'mololongo_pingback' );

if ( ! function_exists( 'mololongo_mobile_web_app_meta' ) ) {
	/**
	 * Add mobile-web-app meta.
	 */
	function mololongo_mobile_web_app_meta() {
		echo '<meta name="mobile-web-app-capable" content="yes">' . "\n";
		echo '<meta name="apple-mobile-web-app-capable" content="yes">' . "\n";
		echo '<meta name="apple-mobile-web-app-title" content="' . esc_attr( get_bloginfo( 'name' ) ) . ' - ' . esc_attr( get_bloginfo( 'description' ) ) . '">' . "\n";
	}
}
add_action( 'wp_head', 'mololongo_mobile_web_app_meta' );

if ( ! function_exists( 'mololongo_default_body_attributes' ) ) {
	/**
	 * Adds schema markup to the body element.
	 *
	 * @param array $atts An associative array of attributes.
	 * @return array
	 */
	function mololongo_default_body_attributes( $atts ) {
		$atts['itemscope'] = '';
		$atts['itemtype']  = 'http://schema.org/WebSite';
		return $atts;
	}
}
add_filter( 'mololongo_body_attributes', 'mololongo_default_body_attributes' );

// Escapes all occurances of 'the_archive_description'.
add_filter( 'get_the_archive_description', 'mololongo_escape_the_archive_description' );

if ( ! function_exists( 'mololongo_escape_the_archive_description' ) ) {
	/**
	 * Escapes the description for an author or post type archive.
	 *
	 * @param string $description Archive description.
	 * @return string Maybe escaped $description.
	 */
	function mololongo_escape_the_archive_description( $description ) {
		if ( is_author() || is_post_type_archive() ) {
			return wp_kses_post( $description );
		}

		/*
		 * All other descriptions are retrieved via term_description() which returns
		 * a sanitized description.
		 */
		return $description;
	}
} // End of if function_exists( 'mololongo_escape_the_archive_description' ).

// Escapes all occurances of 'the_title()' and 'get_the_title()'.
add_filter( 'the_title', 'mololongo_kses_title' );

// Escapes all occurances of 'the_archive_title' and 'get_the_archive_title()'.
add_filter( 'get_the_archive_title', 'mololongo_kses_title' );

if ( ! function_exists( 'mololongo_kses_title' ) ) {
	/**
	 * Sanitizes data for allowed HTML tags for post title.
	 *
	 * @param string $data Post title to filter.
	 * @return string Filtered post title with allowed HTML tags and attributes intact.
	 */
	function mololongo_kses_title( $data ) {
		// Tags not supported in HTML5 are not allowed.
		$allowed_tags = array(
			'abbr'             => array(),
			'aria-describedby' => true,
			'aria-details'     => true,
			'aria-label'       => true,
			'aria-labelledby'  => true,
			'aria-hidden'      => true,
			'b'                => array(),
			'bdo'              => array(
				'dir' => true,
			),
			'blockquote'       => array(
				'cite'     => true,
				'lang'     => true,
				'xml:lang' => true,
			),
			'cite'             => array(
				'dir'  => true,
				'lang' => true,
			),
			'dfn'              => array(),
			'em'               => array(),
			'i'                => array(
				'aria-describedby' => true,
				'aria-details'     => true,
				'aria-label'       => true,
				'aria-labelledby'  => true,
				'aria-hidden'      => true,
				'class'            => true,
			),
			'code'             => array(),
			'del'              => array(
				'datetime' => true,
			),
			'ins'              => array(
				'datetime' => true,
				'cite'     => true,
			),
			'kbd'              => array(),
			'mark'             => array(),
			'pre'              => array(
				'width' => true,
			),
			'q'                => array(
				'cite' => true,
			),
			's'                => array(),
			'samp'             => array(),
			'span'             => array(
				'dir'      => true,
				'align'    => true,
				'lang'     => true,
				'xml:lang' => true,
			),
			'small'            => array(),
			'strong'           => array(),
			'sub'              => array(),
			'sup'              => array(),
			'u'                => array(),
			'var'              => array(),
		);
		$allowed_tags = apply_filters( 'mololongo_kses_title', $allowed_tags );

		return wp_kses( $data, $allowed_tags );
	}
} // End of if function_exists( 'mololongo_kses_title' ).
// disable for posts
add_filter('use_block_editor_for_post', '__return_false', 10);

// disable for post types
add_filter('use_block_editor_for_post_type', '__return_false', 10);

function trim_custom_excerpt($excerpt) {
    if (has_excerpt()) {
        $excerpt = wp_trim_words(get_the_excerpt(), apply_filters("excerpt_length", 15));
    }

    return $excerpt;
}

function custom_excerpt($length = '') {
   global $post;

   $output = get_the_excerpt();
   $output = apply_filters('wptexturize', $output);
   $output = apply_filters('convert_chars', $output);
   $output = substr($output, 0, $length);
   $output = '<p>' . $output . '...</p>';
   return $output;
}
function excerpt($limit) {
      $excerpt = explode(' ', get_the_excerpt(), $limit);

      if (count($excerpt) >= $limit) {
          array_pop($excerpt);
          $excerpt = implode(" ", $excerpt) . '...';
      } else {
          $excerpt = implode(" ", $excerpt);
      }

      $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);

      return $excerpt;
}

function content($limit) {
    $content = explode(' ', get_the_content(), $limit);

    if (count($content) >= $limit) {
        array_pop($content);
        $content = implode(" ", $content) . '...';
    } else {
        $content = implode(" ", $content);
    }

    $content = preg_replace('/\[.+\]/','', $content);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);

    return $content;
}
if ( function_exists('register_sidebar') )
  register_sidebar(array(
    'name'          => __( 'Uredi', 'mololongo' ),
	'id'            => 'uredi',
	'description'   => __( 'Popis ureda', 'mololongo' ),
	'before_widget' => '<div class="footer-widget col-md-3 "><div clas="contact-box">',
	'after_widget'  => '</div></div><!-- .footer-widget -->',
	'before_title'  => '<div class="contact-title"><h6>',
	'after_title'   => '</h6></div>',
  )
);

function ja_global_enqueues() {

	wp_enqueue_style(
		'jquery-auto-complete',
		get_template_directory_uri() . '/css/jquery.auto-complete.css',
		array(),
		'1.0.7'
	);

	wp_enqueue_script(
		'jquery-auto-complete',
		get_template_directory_uri() . '/js/jquery.auto-complete.min.js',
		array( 'jquery' ),
		'1.0.7',
		true
	);

	wp_enqueue_script(
		'global',
		get_template_directory_uri() . '/js/global.js',
		array( 'jquery' ),
		rand(0,999),
		true
	);

	wp_localize_script(
		'global',
		'global',
		array(
			'ajax' => admin_url( 'admin-ajax.php' ),
		)
	);
}
add_action( 'wp_enqueue_scripts', 'ja_global_enqueues' );

/**
 * Live autocomplete search feature.
 *
 * @since 1.0.0
 */
function ja_ajax_search() {
    $term=$_GET["mphb_ra_grad"];

//	$cities_and_accomodations = get_transient( 'cities_and_accomodations_v1' );

//	if ( false === $cities_and_accomodations ) {
		$cities = get_terms( array(
			'taxonomy' => 'mphb_ra_grad',
			'hide_empty' => false,
		) );
		$cities_arr = [];
		foreach ($cities as $key => $city) {
			if (stripos($city->name, $_POST['search']) !== false) {
				$cities_arr[] = $city->name;
			}
		}
	
		$results = new WP_Query( array(
			'post_type'     =>  array( 'mphb_room_attribute', 'mphb_room_type' ) ,
			'post_status'   => 'publish',
			'nopaging'      => true,
			'posts_per_page'=> 3,
			's'             => '',
		) );
	
		$items = array();
	
		if ( !empty( $results->posts ) ) {
			foreach ( $results->posts as $result ) {
				$items[] = $result->post_title;
			}
		}
		// Merge cities array with accomodations
		$cities_and_accomodations = array_merge($cities_arr,$items);

//		set_transient( 'cities_and_accomodations_v1', $cities_and_accomodations, 24 * HOUR_IN_SECONDS );
//	}

	wp_send_json_success( $cities_and_accomodations );
}
add_action( 'wp_ajax_search_site',        'ja_ajax_search' );
add_action( 'wp_ajax_nopriv_search_site', 'ja_ajax_search' );

// If search submitted check type of search - If it is accomodation then redirect
function search_submit() {

	if (isset($_REQUEST['location'])) {
		$accomodation_title = $_REQUEST['location'];
		$accomodation_page = get_page_by_title($accomodation_title, OBJECT, 'mphb_room_type');
		if ($accomodation_page) {
			$accomodation_permalink = get_permalink($accomodation_page->ID);
			wp_redirect($accomodation_permalink);
			exit;
		}
	}
}
add_image_size( 'search-size', 715, 475 ); 