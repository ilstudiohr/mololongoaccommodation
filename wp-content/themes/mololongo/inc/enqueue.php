<?php
/**
 * MoloLongo enqueue scripts
 *
 * @package MoloLongo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'mololongo_scripts' ) ) {
	/**
	 * Load theme's JavaScript and CSS sources.
	 */
	function mololongo_scripts() {
		// Get the theme data.
		$the_theme     = wp_get_theme();
		$theme_version = $the_theme->get( 'Version' );

		$css_version = $theme_version . '.' . filemtime( get_template_directory() . '/css/theme.min.css' );
		wp_enqueue_style( 'mololongo-styles', get_template_directory_uri() . '/css/theme.min.css', array(), $css_version );
		wp_enqueue_style( 'mololongo-custom', get_template_directory_uri() . '/css/custom.css', array(), $css_version );
		wp_enqueue_style( 'mololongo-lightbox', get_template_directory_uri() . '/css/lightbox.css', array(), $css_version );
		wp_enqueue_style( 'mololongo-calendar', get_template_directory_uri() . '/css/mphb-datepicker-minimal.css', array(), $css_version );
		wp_enqueue_style( 't-datepicker', get_template_directory_uri() . '/css/t-datepicker/t-datepicker.min.css', array(), $css_version );
		wp_enqueue_style( 't-datepicker-blue', get_template_directory_uri() . '/css/t-datepicker/t-datepicker-blue.css', array(), $css_version );
		wp_enqueue_style( 'orioly-custom', get_template_directory_uri() . '/css/orioly-custom.css', array(), $css_version );

		wp_enqueue_script( 'jquery' );

		$js_version = $theme_version . '.' . filemtime( get_template_directory() . '/js/theme.min.js' );
		wp_enqueue_script( 'mololongo-scripts', get_template_directory_uri() . '/js/theme.min.js', array(), $js_version, true );
		wp_enqueue_script( 't-datepicker-script', get_template_directory_uri() . '/js/t-datepicker.js', array(), $js_version, true );
		wp_enqueue_script( 'mololongo-custom-scripts', get_template_directory_uri() . '/js/custom.js', array(), $js_version, true );
		if(is_single() || is_tax( 'mphb_ra_grad' )){
		    wp_enqueue_script( 'mololongo-scripts-lightbox', get_template_directory_uri() . '/js/lightbox.js', array(), $js_version, true );
		}
		
		wp_enqueue_script( 'mololongo-scripts-sticky', get_template_directory_uri() . '/js/sticky-sidebar.min.js', array(), $js_version, true );
		

		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
} // End of if function_exists( 'mololongo_scripts' ).

add_action( 'wp_enqueue_scripts', 'mololongo_scripts' );
