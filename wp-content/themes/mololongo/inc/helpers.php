<?php
//outputs a string with print_r() inside of <pre> tags
function epr( $str = null ) {
	echo "<pre>";
	print_r( $str );
	echo "</pre>";
} //end function epr()
?>
