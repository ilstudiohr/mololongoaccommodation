<?php
/*
Template Name: Napredna Booking pretraga
*/
?>

<?php get_header(); ?>	
<section class="map-padding">
    <div class="appartments-grid">
        <div class="app-container f-left">
            <ul class="app-list">
               	 <?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
                    
                    <li class="app-list_single">
                    <?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>
                    <div class="app-image">
                        <div class="ribbon-left">
                            <div><?php the_sub_field('price'); ?> <?php _e('eur/noć', 'molo'); ?></div>
                        </div>
                        <div class="swiper swiper_app-list">
                            <div class="swiper-wrapper">
                                <?php $images = get_field('room_slider'); if ($images) { $counter = 1; ?>
                                    <?php foreach( $images as $image ) { ?>	
                                    	<div class="swiper-slide">
                                            <a href="<?php echo $image['url']; ?>" rel="lightbox">
                                                 <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                                            </a>
                                        </div>
                                    <?php $counter++; if ($counter == 4) { break; }} ?>
                                <?php } ?>
                                
                            </div>
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>
                        
                    </div>
                    <div class="app-content">
                        <h3 class="app-title"><?php the_title(); ?></h3>
                        <div class="app-info"><?php echo custom_excerpt(100); ?></div>
                        <ul class="app-icons">
                            <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/pin-color.svg" alt="<?php _e('Lokacija', 'molo'); ?>"> <?php the_sub_field('city'); ?>	</li>
			                <?php if( get_sub_field('basic_capacity') ): ?>
			                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico_guests-color.svg" alt="<?php _e('Kapacitet', 'molo'); ?>"> <?php the_sub_field('basic_capacity'); if( get_sub_field('spare_capacity') ): ?> + <?php the_sub_field('spare_capacity'); endif; ?></li>
			                <?php endif; ?>  
			                <?php if( get_sub_field('beds') ): ?>
			                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico_bed-color.svg" alt="<?php _e('Broj kreveta', 'molo'); ?>"> <?php the_sub_field('beds'); if( get_sub_field('spare_beds') ): ?> + <?php the_sub_field('spare_beds'); endif; ?> </li>
			                <?php endif; ?>
			                <?php if( get_sub_field('bathrooms') ): ?>
			                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-bath.svg" alt="<?php _e('Broj kupaona', 'molo'); ?>"> <?php the_sub_field('bathrooms'); ?> </li>
			                <?php endif; ?>
		                </ul>
		                <div class="app-button">
				            <a href="<?php the_permalink() ?>" class="button-primary"><?php _e('Rezerviraj', 'molo'); ?></a>
				        </div>
		             </div>
		             
                    <?php endwhile; endif; ?>
                </li>
                    
                <?php endwhile; endif; ?>	
            </ul>
        </div>
        <div class="appartments-map f-right" id="sidebarmap">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d359953.9144909471!2d13.628855803337293!3d45.184263050513856!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47633452f5cb56a9%3A0x7e9a83b6b0dfb279!2sIstra!5e0!3m2!1shr!2shr!4v1632213240897!5m2!1shr!2shr" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        </div>
    </div>
</section>

<script>
jQuery(function($){
    $('.app-container, .appartments-map').css({ height: $(window).innerHeight() });
    $(window).resize(function(){
        $('.app-container, .appartments-map').css({ height: $(window).innerHeight() });
    });
});
</script>
<?php get_footer(); ?>	