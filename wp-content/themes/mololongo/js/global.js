/* globals global */
jQuery(function($){
	var searchRequest;
	$('#mphb_grad-mphb-search-form').autoComplete({
		minChars: 3,
		delay: 0,
		source: function(term, suggest){
			try { searchRequest.abort(); } catch(e){}
			searchRequest = $.post(global.ajax, { search: term, action: 'search_site' }, function(res) {
				var matches = [];
				for (i=0;i<res.data.length;i++) {
					if (~res.data[i].toLowerCase().indexOf(term.toLowerCase())) matches.push(res.data[i]);
				}
				suggest(matches);
			});
		},
	});

	$('#mphb_grad-mphb-search-form').change(function(){
		console.log('change');
	});
});
