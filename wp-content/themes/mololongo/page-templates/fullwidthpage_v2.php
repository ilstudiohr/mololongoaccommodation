<?php
/**
 * Template Name: Full Width Page V2
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package MoloLongo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'mololongo_container_type' );

if ( is_front_page() ) {
	get_template_part( 'global-templates/hero' );
}
?>
<?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>
<section class="home-hero subpage-hero" style="background: url('<?php echo $backgroundImg[0]; ?>') ;background-position: center;
    background-repeat: no-repeat;
    background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="text-center big-title"><?php the_field('intro_title'); ?></h1>
            </div>
            <div class="col-md-12 text-center px-12">
                <?php the_field('intro_txt'); ?>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <?php
					while ( have_posts() ) {
						the_post();
						the_content();
					}
					?>
            </div>
        </div>
    </div>
</section>
<?php if( have_rows('counter') ): while ( have_rows('counter') ) : the_row(); ?>
<section class="parallax-cta counter" style="background: url('<?php the_sub_field('pozadinska_slika'); ?>') ;    background-position: 0px -182px;
    background-repeat: no-repeat;
    background-size: cover;">
    <div class="parallax-overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="big text-white"><?php the_sub_field('naslov_brojaca'); ?></h3>
                <?php if(ICL_LANGUAGE_CODE=='hr'): ?>
                <?php echo do_shortcode('[anc_6310_number_counter id="2"]');?>
                <?php elseif(ICL_LANGUAGE_CODE=='en'): ?>
                <?php echo do_shortcode('[anc_6310_number_counter id="3"]');?>
                <?php elseif(ICL_LANGUAGE_CODE=='de'): ?>
                <?php echo do_shortcode('[anc_6310_number_counter id="4"]');?>
                <?php elseif(ICL_LANGUAGE_CODE=='it'): ?>
                <?php echo do_shortcode('[anc_6310_number_counter id="5"]');?>
                <?php endif; ?>
                
                
            </div>
        </div>
    </div>
</section>
<?php endwhile; endif; ?>
<?php if( have_rows('quote') ): while ( have_rows('quote') ) : the_row(); ?>
<section class="bg-gray big-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="big"><?php the_sub_field('naslov'); ?></h3>
                <p class="quote"><?php the_sub_field('kratki_tekst'); ?></p>
                <div class="owner-box text-center">
                    <p><?php the_sub_field('ime_i_prezime'); ?></p>
                    <span><?php the_sub_field('pozicija'); ?></span>
                </div>
            </div>
        </div>
    </div>
</section>
 <?php endwhile; endif; ?>

<section class="big-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center pb-5">
                <div class="above-title">
                    <?php _e('Sve na jednom mjestu', 'molo'); ?>
                </div>
                <h3 class="big"><?php _e('Full service agencija u turizmu', 'molo'); ?></h3>
            </div>
            <?php if( have_rows('full_service_1') ): while ( have_rows('full_service_1') ) : the_row(); ?>
            <div class="col-md-4 mb-5">
                <div class="image-sub">
                    <div class="image-round">
                        <img src="<?php the_sub_field('slika'); ?>">
                    </div>
                    <h4><?php the_sub_field('naslov'); ?></h4>
                    <p><?php the_sub_field('tekst'); ?></p>
                </div>
            </div>
            <?php endwhile; endif; ?>
             <?php if( have_rows('full_service_2') ): while ( have_rows('full_service_2') ) : the_row(); ?>
            <div class="col-md-4 mb-5">
                <div class="image-sub">
                    <div class="image-round">
                        <img src="<?php the_sub_field('slika'); ?>">
                    </div>
                    <h4><?php the_sub_field('naslov'); ?></h4>
                    <p><?php the_sub_field('tekst'); ?></p>
                </div>
            </div>
            <?php endwhile; endif; ?>
             <?php if( have_rows('full_service_3') ): while ( have_rows('full_service_3') ) : the_row(); ?>
            <div class="col-md-4 mb-5">
                <div class="image-sub">
                    <div class="image-round">
                        <img src="<?php the_sub_field('slika'); ?>">
                    </div>
                    <h4><?php the_sub_field('naslov'); ?></h4>
                    <p><?php the_sub_field('tekst'); ?></p>
                </div>
            </div>
            <?php endwhile; endif; ?>
            <?php if( have_rows('full_service_4') ): while ( have_rows('full_service_4') ) : the_row(); ?>
            <div class="col-md-4 mb-5">
                <div class="image-sub">
                    <div class="image-round">
                        <img src="<?php the_sub_field('slika'); ?>">
                    </div>
                    <h4><?php the_sub_field('naslov'); ?></h4>
                    <p><?php the_sub_field('tekst'); ?></p>
                </div>
            </div>
            <?php endwhile; endif; ?>
            <?php if( have_rows('full_service_5') ): while ( have_rows('full_service_5') ) : the_row(); ?>
            <div class="col-md-4 mb-5">
                <div class="image-sub">
                    <div class="image-round">
                        <img src="<?php the_sub_field('slika'); ?>">
                    </div>
                    <h4><?php the_sub_field('naslov'); ?></h4>
                    <p><?php the_sub_field('tekst'); ?></p>
                </div>
            </div>
            <?php endwhile; endif; ?>
            <?php if( have_rows('full_service_6') ): while ( have_rows('full_service_6') ) : the_row(); ?>
            <div class="col-md-4 mb-5">
                <div class="image-sub">
                    <div class="image-round">
                        <img src="<?php the_sub_field('slika'); ?>">
                    </div>
                    <h4><?php the_sub_field('naslov'); ?></h4>
                    <p><?php the_sub_field('tekst'); ?></p>
                </div>
            </div>
            <?php endwhile; endif; ?>
            <div class="col-md-12 text-center">
                <a href="#" class="button-primary"><?php _e('Saznaj više', 'molo'); ?></a>
            </div>
        </div>
    </div>
</section>
<?php if( get_field('pozadinska_slika')  ): ?>
<section class="parallax-cta v2" style="background: url('<?php the_field('pozadinska_slika'); ?>') ; background-repeat: no-repeat;background-size: cover;background-position: 0px 0px;">
    <div class="container">
        <div class="row align-center">
            <div class="col-md-6">
                <?php if( get_field('naslov_slike') ): ?>
                <span><?php _e('IZNAJMLJIVAČI NEKRETNINA', 'molo'); ?></span>
                	<h4 class="text-black"><?php the_field('naslov_slike'); ?></h4>
                <?php endif; ?>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-4">
                <a href="<?php _e('kontakt', 'molo'); ?>" class="button-color dark-color"><?php _e('Kontakt', 'molo'); ?></a>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<?php
get_footer();
