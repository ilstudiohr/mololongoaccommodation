<?php
/**
 * Template Name: Full Width Background
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package MoloLongo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'mololongo_container_type' );

if ( is_front_page() ) {
	get_template_part( 'global-templates/hero' );
}
?>
<?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>
<section class="home-hero big-hero" style="background: url('<?php echo $backgroundImg[0]; ?>') ;background-position: center;
    background-repeat: no-repeat;
    background-size: cover;">
    <div class="overlay"></div>
    <div class="container-sm">
        <div class="row">
            <div class="col-md-12 text-center text-white">
                <h1 class="text-white"><?php the_title();?></h1>
                <?php if( get_field('intro_text') ): ?>
                	<p class="small-desc mb-5 text-white"><?php the_field('intro_text'); ?></p>
                <?php endif; ?>
            </div>
            <div class="col-md-6">
                <div class="contact-box_contact">
                    <div class="contact-content">
                        <h4 class="text-white"><?php _e('Kontaktirajte nas', 'molonew'); ?></h4>
                        <ul class="contact-list mt-2">
                            <li><i class="fa fa-phone"></i>+385 (0)91 600 6646</li>
                            <li><i class="fa fa-envelope"></i>info@mololongo.com</li>
                        </ul>
                        <ul class="social-list_contact mt-5">
                            <li><a href="https://www.instagram.com/mololongoapartments/" target="_blank"><img width="20" src="<?php bloginfo('template_directory'); ?>/images/instagram.svg" class="img-fluid" alt="<?php the_title();?>"></a></li>
            		        <li><a href="https://www.facebook.com/mololongoapartments" target="_blank"><img width="20" src="<?php bloginfo('template_directory'); ?>/images/facebook.svg" class="img-fluid" alt="<?php the_title();?>"></a></li>
            		        <li><a href="https://www.linkedin.com/company/molo-longo" target="_blank"><img width="20" src="<?php bloginfo('template_directory'); ?>/images/linkedin.svg" class="img-fluid" alt="<?php the_title();?>"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <?php
					while ( have_posts() ) {
						the_post();
						the_content();
					}
					?>
            </div>
        </div>
    </div>
</section>
<?php
get_footer();
