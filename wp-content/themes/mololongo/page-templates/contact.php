<?php
/**
 * Template Name: Contact Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package MoloLongo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'mololongo_container_type' );

if ( is_front_page() ) {
	get_template_part( 'global-templates/hero' );
}
?>

<section>
    <div class="container">
        <div class="row d-md-flex align-items-md-center">
            <div class="col-md-6">
                <?php the_post_thumbnail('full', array('class' => 'rounded full')); ?>
            </div>
            <div class="col-md-6">
                <h1><?php the_title();?></h1>
                <?php if( get_field('intro_text') ): ?>
                	<p class="small-desc"><?php the_field('intro_text'); ?></p>
                <?php endif; ?>
            </div>
            <div class="col-md-12 big-contact mt-4">
                <?php the_content();?>
                                <hr>
                
            </div>
        </div>
    </div>
</section>

<?php
get_footer();
