<?php
/**
 * Template Name: Apartment Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package MoloLongo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'mololongo_container_type' );

?>
<style>
    p.app-desc {
        font-size: 18px;
        line-height: 1.8;
        font-weight: 300;
    }
    h2 {
        font-size: 40px;
    }
    .distance-meta img {
    height: 40px;
    width: auto;
    margin-right: 20px;
    min-width: 34px;
}
    .sidebar-list li {
    display: flex;
        align-items: center;
    padding-bottom: 10px;
    border-bottom: 1px solid #eaeef3;
    margin-bottom: 10px;
}
.sidebar-list span, .sidebar-list li > a {
    font-weight: bold;
}
.sidebar-button_group {
    display: flex;
    margin-bottom: 25px;
    justify-content: space-between;
    text-align: center;
    text-transform: uppercase;
}
h3 {
    font-size: 30px;
}
.grid-meta ul {
    display: grid;
    grid-template-columns: repeat(5, 1fr);
    gap: 15px;
}
.grid-meta ul li span {
    padding-left: 0;
    padding-top: 5px;
    font-weight: bold;
    color: #363636;
    margin-top: 5px;
}
section.dark-bg {
    background: #363636;
    color: #fff;
}
section.dark-bg p, section.dark-bg span{
    color: #fff!important;
}
.small-gutters .col-md-8{
    padding-right:5px
}
.small-gutters .col-md-4{
    padding-left:5px
}
.info-box_single {

    border-right: 1px solid #eaeef3;
}
.app_feat-img {
    margin-bottom: 0;
}
.info-box {
    background: #fff;
    color: #363636;
    box-shadow: 0px 0px 5px 0px rgb(0 0 0 / 10%);
}
.info-box_single.box_button {
    flex-basis: 93%;
}
.info-box_single.price_box {
    flex-basis: 107%;
}
.app-gallery {
    grid-gap: 10px;
}
.gallery-side{
    height:270px;
}
.info-box_single img {
    max-width: 45px;
}
.info-box_single span {
    margin-top: 5px;
    font-size: 14px;
}

.info-box_v2 {
    display: flex;
    align-items: center;
    justify-content: space-evenly;
    flex-basis: 100%;
    width: 100%;
    flex: 1;
box-shadow: 0px 0px 5px 0px rgb(0 0 0 / 10%);
}

.info-box_v2 img {
    width: 30px;
}
.info-box_v2 .info-box_single {
    flex-basis: 100%;
    display: flex;
    align-items: center;
    padding: 30px 20px;
    height: 90px;
}
.info_content {
    display: flex;
    flex-direction: column;
    margin-left: 15px;
}
.info_content span {
    margin: 0;
}
.info_content span:nth-child(1) {
    font-weight: bold;
}
.info-box_v2 .box_button a{
    font-size:16px;
}
div#dd_map {
    height: 70vh;
}
</style>
<section class="app-top pb-0">
    <div class="container-fluid">
        <div class="row mobile-order small-gutters">
            
            
            <div class="col-md-8 mobile-one">
                <div class="app_feat-img">
                                            <div class="gallery">   
                                                          <a href="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3660.jpg" rel="lightbox" data-lightbox="gallery">
                                                                    <img width="1920" height="1280" src="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3667.jpg" class="attachment-full size-full wp-post-image" alt="" loading="lazy" srcset="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3667.jpg 1920w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3667-900x600.jpg 900w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3667-1200x800.jpg 1200w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3667-600x400.jpg 600w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3667-768x512.jpg 768w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3667-1536x1024.jpg 1536w" sizes="(max-width: 1920px) 100vw, 1920px">                                                              </a>
                                                          <a href="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3667.jpg" rel="lightbox" data-lightbox="gallery">
                                                              </a>
                                                          <a href="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3687.jpg" rel="lightbox" data-lightbox="gallery">
                                                              </a>
                                                          <a href="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3678.jpg" rel="lightbox" data-lightbox="gallery">
                                                              </a>
                                                          <a href="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3692.jpg" rel="lightbox" data-lightbox="gallery">
                                                              </a>
                                                          <a href="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3700.jpg" rel="lightbox" data-lightbox="gallery">
                                                              </a>
                                                          <a href="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3716.jpg" rel="lightbox" data-lightbox="gallery">
                                                              </a>
                                                          <a href="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3653-scaled.jpg" rel="lightbox" data-lightbox="gallery">
                                                              </a>
                                                          <a href="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3657-scaled.jpg" rel="lightbox" data-lightbox="gallery">
                                                              </a>
                                                          <a href="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3730.jpg" rel="lightbox" data-lightbox="gallery">
                                                              </a>
                                                          <a href="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3494.jpg" rel="lightbox" data-lightbox="gallery">
                                                              </a>
                                                          <a href="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3503.jpg" rel="lightbox" data-lightbox="gallery">
                                                              </a>
                                                          <a href="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3485-scaled.jpg" rel="lightbox" data-lightbox="gallery">
                                                              </a>
                                                          <a href="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_1989-scaled.jpg" rel="lightbox" data-lightbox="gallery">
                                                              </a>
                                                          <a href="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_1981.jpg" rel="lightbox" data-lightbox="gallery">
                                                              </a>
                                                          <a href="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_1979.jpg" rel="lightbox" data-lightbox="gallery">
                                                              </a>
                                                    </div>
                                        
                </div>
            </div>
            <div class="col-md-4 mobile-hidden">
                <div class="app-gallery">
                                            	
                        	<div class="gallery-side">
                                <a href="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3660.jpg" rel="lightbox" data-lightbox="gallery">
                                     <img src="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3660-600x400.jpg" alt="">
                                </a>
                            </div>
                        	
                        	<div class="gallery-side">
                                <a href="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3667.jpg" rel="lightbox" data-lightbox="gallery">
                                     <img src="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3667-600x400.jpg" alt="">
                                </a>
                            </div>
                        	
                        	<div class="gallery-side">
                                <a href="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3687.jpg" rel="lightbox" data-lightbox="gallery">
                                     <img src="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3687-600x400.jpg" alt="">
                                </a>
                            </div>
                        	
                        	<div class="gallery-side">
                                <a href="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3678.jpg" rel="lightbox" data-lightbox="gallery">
                                     <img src="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/IMG_3678-600x400.jpg" alt="">
                                </a>
                            </div>
                                                            </div>
            </div>
        </div>
        </div>
        </section>
        <section class="pt-0">
            <div class="container-sm">
        <div class="row">
            <div class="col-md-12">
                <div class="info-box mt-n5">
                    	
                    <div class="info-box_single">
                                                    <img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/icon_guests.svg" alt="Kapacitet">
                            <span>2 + 1 gosti</span>
                         
                    </div>
                    <div class="info-box_single">
                                                    <img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/icon_bed.svg" alt="beds">
                            <span>1 + 1 kreveti</span>
                         
                    </div>
                    <div class="info-box_single">
                                                    <img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/icon_bathroom.svg" alt="Kupaonica">
                            <span>1 kupaonica</span>
                                            </div>
                    <div class="info-box_single">
                                                    <img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/icon_villa_size.svg" alt="Soba">
                            <span>27m<sup>2</sup></span>
                          
                    </div>
                                        <div class="info-box_single price_box">
                                                    <span>Cijena od</span>45 €/noć                                          </div>
                    <div class="info-box_single box_button">
                        <a href="#">Rezerviraj</a>
                    </div>
                </div>
                
            </div>
            
        </div>
        
        <div class="row app-content mt-2">
            
            <div class="col-md-8">
                <div class="app-info pt-4">
                    <div class="mobile-two pt-xs-0">
                <div class="apartment-title d-flex align-center">
                    <h1>Harbour View #2</h1>
                    	
                    <span class="ml-4 room_stars room_stars_left4"></span>
                                    </div>
                
            </div>
                    <p class="app-desc">
                        Apartman Harbour View 2 nalazi se na trećem katu stambene zgrade u Trpimirovoj ulici 6. Apartman je predviđen za boravak tri osobe, a prostire se na 27 metara kvadratnih. Nalazi se u neposrednoj blizini Pomorskog i povijesnog muzeja Hrvatskog primorja te Hrvatskog narodnog kazališta Ivana pl. Zajca. Apartman odiše svjetlošću i prozračnošću te je moderno uređen s jednostavnim linijama namještaja i pažljivo kombiniranim bojama. Ovaj studio apartman ima kuhinju sa svim priborom potrebnim za kuhanje, bračni krevet, kutnu garnituru na razvlačenje te kupaonicu s tušem. Od dodatne opreme možemo istaknuti klima-uređaj, besplatni bežični internet i televizor s kabelskim programima. Osim toga, kvalitetu boravka upotpunjuje i oprema poput sušila za kosu i glačala sa stalkom za glačanje. Naši brojni gosti posebno ističu da ih se dojmio prekrasan pogled na luku, kao i izvrsna lokacija apartmana. Uvjerite se i sami i rezervirajte svoj boravak u apartmanu Harbour View.</p>
                    </p>
                </div>
            </div>
            <div class="col-md-4">
                <div id="sidebar" style="
    padding-top: 100px!important;
">
                    <div class="sidebar__inner" style="position: relative;">
                        <div class="sidebar-list">
                            
                            <ul>
                                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/icon_pin.svg" alt=""><span>Best price guarantee!</span></li>
                                                                                                            <li>
                                            <img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/icon_pin.svg" alt="room">
                                            <a href="#dd_map">Trpimirova ulica 6, Rijeka 
                                            , 3 kat                                                                                                                                			 (Ima lift)                                    			    
                                                                                        </a>
                                            
                                        </li>
                                     
                                                                                                                                            <li>
                                        	<img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/icon_parking.svg">
                                            <span>Privatni parking</span>
                                        </li>
                        			  
                    			                    			                                                                            <li>
                                            <img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/icon_clock.svg">
                                            <span>Check-in: 16:00 Check-out: 10:00</span>
                                        </li>
                                    	
                                                                <li>
                                	<img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/zabava.svg">
                                    <span>Dopuštena organizacija zabava</span>
                                </li>
                                <li>
                                	<img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ljubimci.svg">
                                    <span>Dopušteni ljubimci</span>
                                </li>
                                <li>
                                	<img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/pusenje.svg">
                                    <span>Dopušteno pušenje</span>
                                </li>
                            </ul>
                            <div class="sidebar-button_group">
                                <a href="#" class="button-primary" style="
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;width: 100%;
">Rezerviraj</a>
                                <a href="#" class="button-secondary" style="
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;width: 100%;
">Pošalji upit</a>
                            </div>
                        </div>
                    
                    </div>
                </div>
            </div>
            

        </div>
    </div>
</section>
<section class="">
    <div class="container-sm">
        <div class="row">
            <div class="col-md-12">
                <div class="app-info">
                    <h3>Sadržaji</h3>
            <div class="app-meta grid-meta py-5">
        <ul>
                            <li>
        	        <img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/icon_bed.svg">
                    <span>Bračni krevet</span>
                </li>
                        
                        
                        
                        
                        
                        
                        
                        
                            <li>
        	        <img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico-couch.svg">
                    <span>Kauč na razvlačenje</span>
                </li>
                        
                        
                 
            
                 
            
                 
            
                 
            
                        
                        <li>
            	<img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/icon_wifi.svg">
                <span>Wi-Fi</span>
            </li>
            	
                        <li>
            	<img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico-posteljina.svg">
            	<span>Posteljina i ručnici</span>
            </li>
            	
                        <li>
            	<img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico-tv.svg">
            	<span>Kabelska TV</span>
            </li>
                 
             
                        <li>
            	<img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico-airco.svg">
            	<span>Klima</span>	
            </li>
                 
        
                        <li>
            	<img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico-fridge.svg">
            	<span>Hladnjak</span>
            </li>
                 
            
                        <li>
            	<img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico-stove.svg">
            	<span>Električni štednjak</span>
            </li>
                 
            
                 
            
              
                 
            
                 
            
                 
            
                 
            
                        <li>
            	<img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico-pribor.svg">
            	<span>Kuhinjski pribor</span>
            </li>
                 
            
                 
            
                        <li>
            	<img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico-kattle.svg">
            	<span>Kuhalo za vodu</span>
            </li>
                 
            
                 
            
                        <li>
            	<img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico-shower.svg">
                <span>Tuš</span>
            </li>
			     
            
                 
            
                 
            
            			<li>
            	<img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico-iron.svg">
                <span>Glačalo</span>
            </li>
			     
            
            			<li>
            	<img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico-hairdrier.svg">
                <span>Sušilo za kosu</span>
            </li>
			     
            
                 
            
                 
            
               
            
               
            
               
            
            <li>
            	<img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/icon_parking.svg">
                <span>Privatni parking</span>
            </li>
			   
                    
        </ul>
            </div>
                </div>
                
        </div>
    </div>
    
        
        
    </div>
</section>
<section>
    <div class="container-sm">
        <div class="row">
            <div class="col-md-12">
                <div class="app-info pt-4">
                        
                                        
                    <h3>Kalendar raspoloživosti</h3>
                    <div class="app-calendar text-center py-5">
                        <div class="mphb_sc_availability_calendar-wrapper">	<div class="mphb-calendar mphb-datepick inlinePicker is-datepick" id="mphb-calendar-63088" data-monthstoshow="4" style=""><div class="datepick datepick-multi mphb-datepicker-grayscale" style="width: 867px;"><div class="datepick-nav"><a href="javascript:void(0)" title="Prikaži prethodni mjesec" class="datepick-cmd datepick-cmd-prev  datepick-disabled">&lt;</a><a href="javascript:void(0)" title="Današnji datum" class="datepick-cmd datepick-cmd-today ">Danas</a><a href="javascript:void(0)" title="Prikaži slijedeći mjesec" class="datepick-cmd datepick-cmd-next ">&gt;</a></div><div class="datepick-month-row"><div class="datepick-month first"><div class="datepick-month-header"><select class="datepick-month-year" title="Prikaži mjesece"><option value="12/2021" selected="selected">Prosinac</option></select> <select class="datepick-month-year" title="Prikaži godine"><option value="12/2021" selected="selected">2021</option><option value="12/2022">2022</option><option value="12/2023">2023</option><option value="12/2024">2024</option><option value="12/2025">2025</option><option value="12/2026">2026</option><option value="12/2027">2027</option><option value="12/2028">2028</option><option value="12/2029">2029</option><option value="12/2030">2030</option><option value="12/2031">2031</option><option value="12/2036">&nbsp;&nbsp;▼</option></select></div><table><thead><tr><th><span class="datepick-dow-1" title="Ponedjeljak">Po</span></th><th><span class="datepick-dow-2" title="Utorak">Ut</span></th><th><span class="datepick-dow-3" title="Srijeda">Sr</span></th><th><span class="datepick-dow-4" title="Četvrtak">Če</span></th><th><span class="datepick-dow-5" title="Petak">Pe</span></th><th><span class="datepick-dow-6" title="Subota">Su</span></th><th><span class="datepick-dow-0" title="Nedjelja">Ne</span></th></tr></thead><tbody><tr><td><span class="dp1638183600000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1638270000000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1638356400000 mphb-date-cell mphb-past-date" title="Dan u prošlosti">1</span></td><td><span class="dp1638442800000 mphb-date-cell mphb-past-date" title="Dan u prošlosti">2</span></td><td><span class="dp1638529200000 mphb-date-cell mphb-past-date" title="Dan u prošlosti">3</span></td><td><span class="dp1638615600000 mphb-date-cell mphb-past-date datepick-weekend" title="Dan u prošlosti">4</span></td><td><span class="dp1638702000000 mphb-date-cell mphb-past-date datepick-weekend" title="Dan u prošlosti">5</span></td></tr><tr><td><span class="dp1638788400000 mphb-date-cell mphb-past-date" title="Dan u prošlosti">6</span></td><td><span class="dp1638874800000 mphb-date-cell mphb-past-date" title="Dan u prošlosti">7</span></td><td><span class="dp1638961200000 mphb-date-cell mphb-past-date" title="Dan u prošlosti">8</span></td><td><span class="dp1639047600000 mphb-date-cell mphb-past-date" title="Dan u prošlosti">9</span></td><td><span class="dp1639134000000 mphb-date-cell mphb-past-date" title="Dan u prošlosti">10</span></td><td><span class="dp1639220400000 mphb-date-cell mphb-past-date datepick-weekend" title="Dan u prošlosti">11</span></td><td><span class="dp1639306800000 mphb-date-cell mphb-past-date datepick-weekend" title="Dan u prošlosti">12</span></td></tr><tr><td><span class="dp1639393200000 mphb-date-cell mphb-past-date" title="Dan u prošlosti">13</span></td><td><span class="dp1639479600000 mphb-date-cell mphb-past-date" title="Dan u prošlosti">14</span></td><td><span class="dp1639566000000 mphb-date-cell mphb-past-date" title="Dan u prošlosti">15</span></td><td><span class="dp1639652400000 mphb-date-cell mphb-past-date" title="Dan u prošlosti">16</span></td><td><span class="dp1639738800000 mphb-date-cell mphb-past-date" title="Dan u prošlosti">17</span></td><td><span class="dp1639825200000 mphb-date-cell mphb-past-date datepick-weekend" title="Dan u prošlosti">18</span></td><td><span class="dp1639911600000 mphb-date-cell mphb-past-date datepick-weekend" title="Dan u prošlosti">19</span></td></tr><tr><td><span class="dp1639998000000 mphb-date-cell mphb-past-date" title="Dan u prošlosti">20</span></td><td><span class="dp1640084400000 mphb-date-cell mphb-past-date" title="Dan u prošlosti">21</span></td><td><span class="dp1640170800000 mphb-date-cell mphb-past-date" title="Dan u prošlosti">22</span></td><td><span class="dp1640257200000 mphb-date-cell mphb-past-date" title="Dan u prošlosti">23</span></td><td><span class="dp1640343600000 mphb-date-cell mphb-past-date" title="Dan u prošlosti">24</span></td><td><span class="dp1640430000000 mphb-date-cell mphb-past-date datepick-weekend" title="Dan u prošlosti">25</span></td><td><span class="dp1640516400000 mphb-date-cell mphb-past-date datepick-weekend" title="Dan u prošlosti">26</span></td></tr><tr><td><span class="dp1640602800000 mphb-date-cell mphb-past-date" title="Dan u prošlosti">27</span></td><td><span class="dp1640689200000 mphb-date-cell mphb-past-date" title="Dan u prošlosti">28</span></td><td><span class="dp1640775600000 mphb-date-cell mphb-available-date datepick-today datepick-highlight" title="Dostupno(1)">29</span></td><td><span class="dp1640862000000 mphb-date-cell mphb-available-date" title="Dostupno(1)">30</span></td><td><span class="dp1640948400000 mphb-date-cell mphb-available-date" title="Dostupno(1)">31</span></td><td><span class="dp1641034800000 mphb-date-cell mphb-extra-date datepick-weekend datepick-other-month">&nbsp;</span></td><td><span class="dp1641121200000 mphb-date-cell mphb-extra-date datepick-weekend datepick-other-month">&nbsp;</span></td></tr><tr><td><span class="dp1641207600000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1641294000000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1641380400000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1641466800000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1641553200000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1641639600000 mphb-date-cell mphb-extra-date datepick-weekend datepick-other-month">&nbsp;</span></td><td><span class="dp1641726000000 mphb-date-cell mphb-extra-date datepick-weekend datepick-other-month">&nbsp;</span></td></tr></tbody></table></div><div class="datepick-month"><div class="datepick-month-header">Siječanj 2022</div><table><thead><tr><th><span class="datepick-dow-1" title="Ponedjeljak">Po</span></th><th><span class="datepick-dow-2" title="Utorak">Ut</span></th><th><span class="datepick-dow-3" title="Srijeda">Sr</span></th><th><span class="datepick-dow-4" title="Četvrtak">Če</span></th><th><span class="datepick-dow-5" title="Petak">Pe</span></th><th><span class="datepick-dow-6" title="Subota">Su</span></th><th><span class="datepick-dow-0" title="Nedjelja">Ne</span></th></tr></thead><tbody><tr><td><span class="dp1640602800000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1640689200000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1640775600000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1640862000000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1640948400000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1641034800000 mphb-date-cell mphb-available-date datepick-weekend" title="Dostupno(1)">1</span></td><td><span class="dp1641121200000 mphb-date-cell mphb-available-date datepick-weekend" title="Dostupno(1)">2</span></td></tr><tr><td><span class="dp1641207600000 mphb-date-cell mphb-available-date" title="Dostupno(1)">3</span></td><td><span class="dp1641294000000 mphb-date-cell mphb-available-date" title="Dostupno(1)">4</span></td><td><span class="dp1641380400000 mphb-date-cell mphb-available-date" title="Dostupno(1)">5</span></td><td><span class="dp1641466800000 mphb-date-cell mphb-available-date" title="Dostupno(1)">6</span></td><td><span class="dp1641553200000 mphb-date-cell mphb-available-date" title="Dostupno(1)">7</span></td><td><span class="dp1641639600000 mphb-date-cell mphb-available-date datepick-weekend" title="Dostupno(1)">8</span></td><td><span class="dp1641726000000 mphb-date-cell mphb-available-date datepick-weekend" title="Dostupno(1)">9</span></td></tr><tr><td><span class="dp1641812400000 mphb-date-cell mphb-available-date" title="Dostupno(1)">10</span></td><td><span class="dp1641898800000 mphb-date-cell mphb-available-date" title="Dostupno(1)">11</span></td><td><span class="dp1641985200000 mphb-date-cell mphb-available-date" title="Dostupno(1)">12</span></td><td><span class="dp1642071600000 mphb-date-cell mphb-available-date" title="Dostupno(1)">13</span></td><td><span class="dp1642158000000 mphb-date-cell mphb-available-date" title="Dostupno(1)">14</span></td><td><span class="dp1642244400000 mphb-date-cell mphb-available-date datepick-weekend" title="Dostupno(1)">15</span></td><td><span class="dp1642330800000 mphb-date-cell mphb-available-date datepick-weekend" title="Dostupno(1)">16</span></td></tr><tr><td><span class="dp1642417200000 mphb-date-cell mphb-available-date" title="Dostupno(1)">17</span></td><td><span class="dp1642503600000 mphb-date-cell mphb-available-date" title="Dostupno(1)">18</span></td><td><span class="dp1642590000000 mphb-date-cell mphb-available-date" title="Dostupno(1)">19</span></td><td><span class="dp1642676400000 mphb-date-cell mphb-available-date" title="Dostupno(1)">20</span></td><td><span class="dp1642762800000 mphb-date-cell mphb-available-date" title="Dostupno(1)">21</span></td><td><span class="dp1642849200000 mphb-date-cell mphb-available-date datepick-weekend" title="Dostupno(1)">22</span></td><td><span class="dp1642935600000 mphb-date-cell mphb-available-date datepick-weekend" title="Dostupno(1)">23</span></td></tr><tr><td><span class="dp1643022000000 mphb-date-cell mphb-available-date" title="Dostupno(1)">24</span></td><td><span class="dp1643108400000 mphb-date-cell mphb-available-date" title="Dostupno(1)">25</span></td><td><span class="dp1643194800000 mphb-date-cell mphb-available-date" title="Dostupno(1)">26</span></td><td><span class="dp1643281200000 mphb-date-cell mphb-available-date" title="Dostupno(1)">27</span></td><td><span class="dp1643367600000 mphb-date-cell mphb-available-date" title="Dostupno(1)">28</span></td><td><span class="dp1643454000000 mphb-date-cell mphb-available-date datepick-weekend" title="Dostupno(1)">29</span></td><td><span class="dp1643540400000 mphb-date-cell mphb-available-date datepick-weekend" title="Dostupno(1)">30</span></td></tr><tr><td><span class="dp1643626800000 mphb-date-cell mphb-available-date" title="Dostupno(1)">31</span></td><td><span class="dp1643713200000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1643799600000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1643886000000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1643972400000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1644058800000 mphb-date-cell mphb-extra-date datepick-weekend datepick-other-month">&nbsp;</span></td><td><span class="dp1644145200000 mphb-date-cell mphb-extra-date datepick-weekend datepick-other-month">&nbsp;</span></td></tr></tbody></table></div><div class="datepick-month"><div class="datepick-month-header">Veljača 2022</div><table><thead><tr><th><span class="datepick-dow-1" title="Ponedjeljak">Po</span></th><th><span class="datepick-dow-2" title="Utorak">Ut</span></th><th><span class="datepick-dow-3" title="Srijeda">Sr</span></th><th><span class="datepick-dow-4" title="Četvrtak">Če</span></th><th><span class="datepick-dow-5" title="Petak">Pe</span></th><th><span class="datepick-dow-6" title="Subota">Su</span></th><th><span class="datepick-dow-0" title="Nedjelja">Ne</span></th></tr></thead><tbody><tr><td><span class="dp1643626800000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1643713200000 mphb-date-cell mphb-available-date" title="Dostupno(1)">1</span></td><td><span class="dp1643799600000 mphb-date-cell mphb-available-date" title="Dostupno(1)">2</span></td><td><span class="dp1643886000000 mphb-date-cell mphb-available-date" title="Dostupno(1)">3</span></td><td><span class="dp1643972400000 mphb-date-cell mphb-available-date" title="Dostupno(1)">4</span></td><td><span class="dp1644058800000 mphb-date-cell mphb-available-date datepick-weekend" title="Dostupno(1)">5</span></td><td><span class="dp1644145200000 mphb-date-cell mphb-available-date datepick-weekend" title="Dostupno(1)">6</span></td></tr><tr><td><span class="dp1644231600000 mphb-date-cell mphb-available-date" title="Dostupno(1)">7</span></td><td><span class="dp1644318000000 mphb-date-cell mphb-available-date" title="Dostupno(1)">8</span></td><td><span class="dp1644404400000 mphb-date-cell mphb-available-date" title="Dostupno(1)">9</span></td><td><span class="dp1644490800000 mphb-date-cell mphb-available-date" title="Dostupno(1)">10</span></td><td><span class="dp1644577200000 mphb-date-cell mphb-available-date" title="Dostupno(1)">11</span></td><td><span class="dp1644663600000 mphb-date-cell mphb-available-date datepick-weekend" title="Dostupno(1)">12</span></td><td><span class="dp1644750000000 mphb-date-cell mphb-available-date datepick-weekend" title="Dostupno(1)">13</span></td></tr><tr><td><span class="dp1644836400000 mphb-date-cell mphb-available-date" title="Dostupno(1)">14</span></td><td><span class="dp1644922800000 mphb-date-cell mphb-available-date" title="Dostupno(1)">15</span></td><td><span class="dp1645009200000 mphb-date-cell mphb-available-date" title="Dostupno(1)">16</span></td><td><span class="dp1645095600000 mphb-date-cell mphb-available-date" title="Dostupno(1)">17</span></td><td><span class="dp1645182000000 mphb-date-cell mphb-available-date" title="Dostupno(1)">18</span></td><td><span class="dp1645268400000 mphb-date-cell mphb-available-date datepick-weekend" title="Dostupno(1)">19</span></td><td><span class="dp1645354800000 mphb-date-cell mphb-available-date datepick-weekend" title="Dostupno(1)">20</span></td></tr><tr><td><span class="dp1645441200000 mphb-date-cell mphb-available-date" title="Dostupno(1)">21</span></td><td><span class="dp1645527600000 mphb-date-cell mphb-available-date" title="Dostupno(1)">22</span></td><td><span class="dp1645614000000 mphb-date-cell mphb-available-date" title="Dostupno(1)">23</span></td><td><span class="dp1645700400000 mphb-date-cell mphb-available-date" title="Dostupno(1)">24</span></td><td><span class="dp1645786800000 mphb-date-cell mphb-available-date" title="Dostupno(1)">25</span></td><td><span class="dp1645873200000 mphb-date-cell mphb-available-date datepick-weekend" title="Dostupno(1)">26</span></td><td><span class="dp1645959600000 mphb-date-cell mphb-available-date datepick-weekend" title="Dostupno(1)">27</span></td></tr><tr><td><span class="dp1646046000000 mphb-date-cell mphb-available-date" title="Dostupno(1)">28</span></td><td><span class="dp1646132400000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1646218800000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1646305200000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1646391600000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1646478000000 mphb-date-cell mphb-extra-date datepick-weekend datepick-other-month">&nbsp;</span></td><td><span class="dp1646564400000 mphb-date-cell mphb-extra-date datepick-weekend datepick-other-month">&nbsp;</span></td></tr><tr><td><span class="dp1646650800000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1646737200000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1646823600000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1646910000000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1646996400000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1647082800000 mphb-date-cell mphb-extra-date datepick-weekend datepick-other-month">&nbsp;</span></td><td><span class="dp1647169200000 mphb-date-cell mphb-extra-date datepick-weekend datepick-other-month">&nbsp;</span></td></tr></tbody></table></div><div class="datepick-month last"><div class="datepick-month-header">Ožujak 2022</div><table><thead><tr><th><span class="datepick-dow-1" title="Ponedjeljak">Po</span></th><th><span class="datepick-dow-2" title="Utorak">Ut</span></th><th><span class="datepick-dow-3" title="Srijeda">Sr</span></th><th><span class="datepick-dow-4" title="Četvrtak">Če</span></th><th><span class="datepick-dow-5" title="Petak">Pe</span></th><th><span class="datepick-dow-6" title="Subota">Su</span></th><th><span class="datepick-dow-0" title="Nedjelja">Ne</span></th></tr></thead><tbody><tr><td><span class="dp1646046000000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1646132400000 mphb-date-cell mphb-available-date" title="Dostupno(1)">1</span></td><td><span class="dp1646218800000 mphb-date-cell mphb-available-date" title="Dostupno(1)">2</span></td><td><span class="dp1646305200000 mphb-date-cell mphb-available-date" title="Dostupno(1)">3</span></td><td><span class="dp1646391600000 mphb-date-cell mphb-available-date" title="Dostupno(1)">4</span></td><td><span class="dp1646478000000 mphb-date-cell mphb-available-date datepick-weekend" title="Dostupno(1)">5</span></td><td><span class="dp1646564400000 mphb-date-cell mphb-available-date datepick-weekend" title="Dostupno(1)">6</span></td></tr><tr><td><span class="dp1646650800000 mphb-date-cell mphb-available-date" title="Dostupno(1)">7</span></td><td><span class="dp1646737200000 mphb-date-cell mphb-available-date" title="Dostupno(1)">8</span></td><td><span class="dp1646823600000 mphb-date-cell mphb-available-date" title="Dostupno(1)">9</span></td><td><span class="dp1646910000000 mphb-date-cell mphb-available-date" title="Dostupno(1)">10</span></td><td><span class="dp1646996400000 mphb-date-cell mphb-available-date" title="Dostupno(1)">11</span></td><td><span class="dp1647082800000 mphb-date-cell mphb-available-date datepick-weekend" title="Dostupno(1)">12</span></td><td><span class="dp1647169200000 mphb-date-cell mphb-available-date datepick-weekend" title="Dostupno(1)">13</span></td></tr><tr><td><span class="dp1647255600000 mphb-date-cell mphb-available-date" title="Dostupno(1)">14</span></td><td><span class="dp1647342000000 mphb-date-cell mphb-available-date" title="Dostupno(1)">15</span></td><td><span class="dp1647428400000 mphb-date-cell mphb-available-date" title="Dostupno(1)">16</span></td><td><span class="dp1647514800000 mphb-date-cell mphb-available-date" title="Dostupno(1)">17</span></td><td><span class="dp1647601200000 mphb-date-cell mphb-available-date" title="Dostupno(1)">18</span></td><td><span class="dp1647687600000 mphb-date-cell mphb-available-date datepick-weekend" title="Dostupno(1)">19</span></td><td><span class="dp1647774000000 mphb-date-cell mphb-available-date datepick-weekend" title="Dostupno(1)">20</span></td></tr><tr><td><span class="dp1647860400000 mphb-date-cell mphb-available-date" title="Dostupno(1)">21</span></td><td><span class="dp1647946800000 mphb-date-cell mphb-available-date" title="Dostupno(1)">22</span></td><td><span class="dp1648033200000 mphb-date-cell mphb-available-date" title="Dostupno(1)">23</span></td><td><span class="dp1648119600000 mphb-date-cell mphb-available-date" title="Dostupno(1)">24</span></td><td><span class="dp1648206000000 mphb-date-cell mphb-available-date" title="Dostupno(1)">25</span></td><td><span class="dp1648292400000 mphb-date-cell mphb-available-date datepick-weekend" title="Dostupno(1)">26</span></td><td><span class="dp1648375200000 mphb-date-cell mphb-available-date datepick-weekend" title="Dostupno(1)">27</span></td></tr><tr><td><span class="dp1648461600000 mphb-date-cell mphb-available-date" title="Dostupno(1)">28</span></td><td><span class="dp1648548000000 mphb-date-cell mphb-available-date" title="Dostupno(1)">29</span></td><td><span class="dp1648634400000 mphb-date-cell mphb-available-date" title="Dostupno(1)">30</span></td><td><span class="dp1648720800000 mphb-date-cell mphb-available-date" title="Dostupno(1)">31</span></td><td><span class="dp1648807200000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1648893600000 mphb-date-cell mphb-extra-date datepick-weekend datepick-other-month">&nbsp;</span></td><td><span class="dp1648980000000 mphb-date-cell mphb-extra-date datepick-weekend datepick-other-month">&nbsp;</span></td></tr><tr><td><span class="dp1649066400000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1649152800000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1649239200000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1649325600000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1649412000000 mphb-date-cell mphb-extra-date datepick-other-month">&nbsp;</span></td><td><span class="dp1649498400000 mphb-date-cell mphb-extra-date datepick-weekend datepick-other-month">&nbsp;</span></td><td><span class="dp1649584800000 mphb-date-cell mphb-extra-date datepick-weekend datepick-other-month">&nbsp;</span></td></tr></tbody></table></div></div><div class="datepick-clear-fix"></div></div></div>
	</div> 
                    </div>
                    
                    <h3>Dodatne informacije</h3>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="app-info">
                            <div class="add-info ">
                                <p class="app-desc">Hosts speak English, Italian and Croatian, and do not live in this property.
Apartment Emerald is a part of the condominium consisting of 3 luxurious accommodation units on the same floor. The whole condominium hosting up to 16 guests is also available for rental. If you have a custom inquiry regarding this option, please contact us. We will be happy to assist you.

We kindly recommend you to check your Booking &amp; Cancellation policy before making your booking.</p>
                            </div>
                           
                            
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="app-info">
                            <div class="sidebar-list distance-meta">
                                                                <ul>
                                                                        <li>
                                    	<img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/icon_beach.svg">
                                    	<span class="d-flex justify-content-between w-100"><span>Plaža</span> <span> 2.9 km</span></span>
                                    </li>
                                       
                                    
                                                                        <li>
                                    	<img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico-pribor.svg">
                                    	<span class="d-flex justify-content-between w-100"><span>Restoran</span> <span> 250 m</span></span>
                                    </li>
                                       
                                    
                                                                        <li>
                                    	<img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/icon_atm.svg">
                                    	<span class="d-flex justify-content-between w-100"><span>Bankomat</span> <span> 110 m</span></span>
                                    </li>
                                       
                                    
                                                                        <li>
                                    	<img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/icon_parking.svg">
                                    	<span class="d-flex justify-content-between w-100"><span>Parking</span> <span>350 m</span></span>
                                    </li>
                                       
                                    
                                                                        <li>
                                    	<img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/icon_airport.svg">
                                    	<span class="d-flex justify-content-between w-100"><span>Aerodrom</span> <span> 25.2 km</span></span>
                                    </li>
                                     
                                </ul>
                                                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="map-section">
    <div id="dd_map"></div>
</section>
<section class="app-list pt-0">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-4">
                <h3>Slične smještajne jedinice</h3>
            </div>
            <div class="swiper swiper-featured pt-5 mt-n5 pb-4 swiper-initialized swiper-horizontal swiper-pointer-events">
                <div class="swiper-wrapper" id="swiper-wrapper-fddeb31f11dd4d7e" aria-live="polite" style="transition-duration: 0ms; transform: translate3d(-1640px, 0px, 0px);"><div class="swiper-slide swiper-slide-duplicate" data-swiper-slide-index="2" role="group" aria-label="3 / 5" style="width: 506.667px; margin-right: 40px;">
                        <div class="col-md-12">
                                                        <div class="app-box">
            				    <div class="app-image">
            				        <img width="900" height="590" src="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/LuxFlat20-11-900x590.jpg" class="attachment-medium size-medium wp-post-image" alt="" loading="lazy" srcset="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/LuxFlat20-11-900x590.jpg 900w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/LuxFlat20-11-1200x787.jpg 1200w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/LuxFlat20-11-768x504.jpg 768w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/LuxFlat20-11-1536x1007.jpg 1536w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/LuxFlat20-11.jpg 1920w" sizes="(max-width: 900px) 100vw, 900px">            				        <span class="app-stars app_stars4"></span>
            				    </div>
            				    <div class="app-content">
            				        <div class="app-title">
            				            <h4>LuxFlat20</h4>
            				            <span class="app-price">45 eur/noć</span>
            				        </div>
            				        <div class="app-icons">
            				            <ul class="small-icons">
            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/pin-color.svg" alt="Lokacija"> Rijeka	</li>
            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico_guests-color.svg" alt="Kapacitet"> 2 + 1</li>
            				                  
            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico_bed-color.svg" alt="Broj kreveta"> 1 + 1 </li>
            				                            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico-bath.svg" alt="Broj kupaona"> 1 </li>
            				                            				            </ul>
            				        </div>
            				        <div class="app-short">
            				            <p>Apartman LUX FLAT 20 nalazi se u Ul.Moše Albaharija 7, na 4. katu neposredno centra i svih važnijih sadržaja grada. Apartman površine 30 m2 je jednosoban sa kuhinjom koja...</p>
            				        </div>
            				        <div class="app-button">
            				            <a href="https://stage.mololongoaccommodation.com/accommodation/luxflat20/" class="button-primary">Rezerviraj</a>
            				        </div>
            				    </div>
            				    
            				</div>
            				                        </div>
                    </div><div class="swiper-slide swiper-slide-duplicate" data-swiper-slide-index="3" role="group" aria-label="4 / 5" style="width: 506.667px; margin-right: 40px;">
                        <div class="col-md-12">
                                                        <div class="app-box">
            				    <div class="app-image">
            				        <img width="900" height="600" src="https://stage.mololongoaccommodation.com/wp-content/uploads/2021/03/IMG_2135-900x600.jpg" class="attachment-medium size-medium wp-post-image" alt="" loading="lazy" srcset="https://stage.mololongoaccommodation.com/wp-content/uploads/2021/03/IMG_2135-900x600.jpg 900w, https://stage.mololongoaccommodation.com/wp-content/uploads/2021/03/IMG_2135-1200x800.jpg 1200w, https://stage.mololongoaccommodation.com/wp-content/uploads/2021/03/IMG_2135-600x400.jpg 600w, https://stage.mololongoaccommodation.com/wp-content/uploads/2021/03/IMG_2135-768x512.jpg 768w, https://stage.mololongoaccommodation.com/wp-content/uploads/2021/03/IMG_2135-1536x1024.jpg 1536w, https://stage.mololongoaccommodation.com/wp-content/uploads/2021/03/IMG_2135.jpg 1920w" sizes="(max-width: 900px) 100vw, 900px">            				        <span class="app-stars app_stars4"></span>
            				    </div>
            				    <div class="app-content">
            				        <div class="app-title">
            				            <h4>Adeona #1</h4>
            				            <span class="app-price">50 eur/noć</span>
            				        </div>
            				        <div class="app-icons">
            				            <ul class="small-icons">
            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/pin-color.svg" alt="Lokacija"> Rijeka	</li>
            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico_guests-color.svg" alt="Kapacitet"> 2 + 2</li>
            				                  
            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico_bed-color.svg" alt="Broj kreveta"> 1 + 1 </li>
            				                            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico-bath.svg" alt="Broj kupaona"> 1 </li>
            				                            				            </ul>
            				        </div>
            				        <div class="app-short">
            				            <p>Novouređeni apartman ADEONA 1 nalazi se u centru Rijeke, u Križanićevoj ulici br. 7, na prvom katu zgrade u pasažu s galerijama. Apartman je površine 32 m2 i pruža...</p>
            				        </div>
            				        <div class="app-button">
            				            <a href="https://stage.mololongoaccommodation.com/accommodation/adeona-1/" class="button-primary">Rezerviraj</a>
            				        </div>
            				    </div>
            				    
            				</div>
            				                        </div>
                    </div><div class="swiper-slide swiper-slide-duplicate swiper-slide-prev" data-swiper-slide-index="4" role="group" aria-label="5 / 5" style="width: 506.667px; margin-right: 40px;">
                        <div class="col-md-12">
                                                        <div class="app-box">
            				    <div class="app-image">
            				        <img width="900" height="506" src="https://stage.mololongoaccommodation.com/wp-content/uploads/2021/06/villabveastwing_6-900x506.jpg" class="attachment-medium size-medium wp-post-image" alt="" loading="lazy" srcset="https://stage.mololongoaccommodation.com/wp-content/uploads/2021/06/villabveastwing_6-900x506.jpg 900w, https://stage.mololongoaccommodation.com/wp-content/uploads/2021/06/villabveastwing_6-1200x675.jpg 1200w, https://stage.mololongoaccommodation.com/wp-content/uploads/2021/06/villabveastwing_6-768x432.jpg 768w, https://stage.mololongoaccommodation.com/wp-content/uploads/2021/06/villabveastwing_6-1536x864.jpg 1536w, https://stage.mololongoaccommodation.com/wp-content/uploads/2021/06/villabveastwing_6.jpg 1920w" sizes="(max-width: 900px) 100vw, 900px">            				        <span class="app-stars app_stars4"></span>
            				    </div>
            				    <div class="app-content">
            				        <div class="app-title">
            				            <h4>Studio East Wing</h4>
            				            <span class="app-price">99 eur/noć</span>
            				        </div>
            				        <div class="app-icons">
            				            <ul class="small-icons">
            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/pin-color.svg" alt="Lokacija"> Opatija	</li>
            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico_guests-color.svg" alt="Kapacitet"> 2</li>
            				                  
            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico_bed-color.svg" alt="Broj kreveta"> 1 </li>
            				                            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico-bath.svg" alt="Broj kupaona"> 1 </li>
            				                            				            </ul>
            				        </div>
            				        <div class="app-short">
            				            <p>Četiri luksuzno opremljena apartmana čine ovu kuću za odmor vrhunskim izborom svakog hedonista. Prema ocjenama gostiju, ova kuća za odmor ima sve što je potrebno za istinski odmor duše...</p>
            				        </div>
            				        <div class="app-button">
            				            <a href="https://stage.mololongoaccommodation.com/accommodation/studio-east-wing/" class="button-primary">Rezerviraj</a>
            				        </div>
            				    </div>
            				    
            				</div>
            				                        </div>
                    </div>
                                    <div class="swiper-slide swiper-slide-active" data-swiper-slide-index="0" role="group" aria-label="1 / 5" style="width: 506.667px; margin-right: 40px;">
                        <div class="col-md-12">
                                                        <div class="app-box">
            				    <div class="app-image">
            				        <img width="900" height="600" src="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_7483-900x600.jpg" class="attachment-medium size-medium wp-post-image" alt="" loading="lazy" srcset="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_7483-900x600.jpg 900w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_7483-1200x800.jpg 1200w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_7483-600x400.jpg 600w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_7483-768x512.jpg 768w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_7483-1536x1024.jpg 1536w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_7483.jpg 1995w" sizes="(max-width: 900px) 100vw, 900px">            				        <span class="app-stars app_stars4"></span>
            				    </div>
            				    <div class="app-content">
            				        <div class="app-title">
            				            <h4>Srnec Ciottina S2</h4>
            				            <span class="app-price">45 eur/noć</span>
            				        </div>
            				        <div class="app-icons">
            				            <ul class="small-icons">
            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/pin-color.svg" alt="Lokacija"> Rijeka	</li>
            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico_guests-color.svg" alt="Kapacitet"> 2 + 1</li>
            				                  
            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico_bed-color.svg" alt="Broj kreveta"> 1 + 1 </li>
            				                            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico-bath.svg" alt="Broj kupaona"> 1 </li>
            				                            				            </ul>
            				        </div>
            				        <div class="app-short">
            				            <p>Jednosobni apartman Srnec Ciottina S2 nalazi se u centru grada u Ciottinoj ulici 30, na prvome katu stambene zgrade. Ovaj moderno opremljen stan smješten je na 30 metara kvadratnih,...</p>
            				        </div>
            				        <div class="app-button">
            				            <a href="https://stage.mololongoaccommodation.com/accommodation/srnec-ciottina-s2/" class="button-primary">Rezerviraj</a>
            				        </div>
            				    </div>
            				    
            				</div>
            				                        </div>
                    </div>
                                    <div class="swiper-slide swiper-slide-next" data-swiper-slide-index="1" role="group" aria-label="2 / 5" style="width: 506.667px; margin-right: 40px;">
                        <div class="col-md-12">
                                                        <div class="app-box">
            				    <div class="app-image">
            				        <img width="900" height="600" src="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_1044-Edit-900x600.jpg" class="attachment-medium size-medium wp-post-image" alt="" loading="lazy" srcset="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_1044-Edit-900x600.jpg 900w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_1044-Edit-1200x800.jpg 1200w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_1044-Edit-600x400.jpg 600w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_1044-Edit-768x512.jpg 768w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_1044-Edit-1536x1024.jpg 1536w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_1044-Edit.jpg 1995w" sizes="(max-width: 900px) 100vw, 900px">            				        <span class="app-stars app_stars3"></span>
            				    </div>
            				    <div class="app-content">
            				        <div class="app-title">
            				            <h4>Apartman &amp; sobe Forever – Soba #2</h4>
            				            <span class="app-price">35 eur/noć</span>
            				        </div>
            				        <div class="app-icons">
            				            <ul class="small-icons">
            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/pin-color.svg" alt="Lokacija"> Rijeka	</li>
            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico_guests-color.svg" alt="Kapacitet"> 2</li>
            				                  
            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico_bed-color.svg" alt="Broj kreveta"> 1 </li>
            				                            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico-bath.svg" alt="Broj kupaona"> 1 </li>
            				                            				            </ul>
            				        </div>
            				        <div class="app-short">
            				            <p>Forever Room #2 nalazi se u samome centru grada Rijeka, na adresi Matije Gupca 20 na drugome katu stambene zgrade. Smještena je na 16 metara kvadratnih, a sadrži veliki...</p>
            				        </div>
            				        <div class="app-button">
            				            <a href="https://stage.mololongoaccommodation.com/accommodation/apartments-rooms-forever-room-2/" class="button-primary">Rezerviraj</a>
            				        </div>
            				    </div>
            				    
            				</div>
            				                        </div>
                    </div>
                                    <div class="swiper-slide" data-swiper-slide-index="2" role="group" aria-label="3 / 5" style="width: 506.667px; margin-right: 40px;">
                        <div class="col-md-12">
                                                        <div class="app-box">
            				    <div class="app-image">
            				        <img width="900" height="590" src="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/LuxFlat20-11-900x590.jpg" class="attachment-medium size-medium wp-post-image" alt="" loading="lazy" srcset="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/LuxFlat20-11-900x590.jpg 900w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/LuxFlat20-11-1200x787.jpg 1200w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/LuxFlat20-11-768x504.jpg 768w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/LuxFlat20-11-1536x1007.jpg 1536w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/LuxFlat20-11.jpg 1920w" sizes="(max-width: 900px) 100vw, 900px">            				        <span class="app-stars app_stars4"></span>
            				    </div>
            				    <div class="app-content">
            				        <div class="app-title">
            				            <h4>LuxFlat20</h4>
            				            <span class="app-price">45 eur/noć</span>
            				        </div>
            				        <div class="app-icons">
            				            <ul class="small-icons">
            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/pin-color.svg" alt="Lokacija"> Rijeka	</li>
            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico_guests-color.svg" alt="Kapacitet"> 2 + 1</li>
            				                  
            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico_bed-color.svg" alt="Broj kreveta"> 1 + 1 </li>
            				                            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico-bath.svg" alt="Broj kupaona"> 1 </li>
            				                            				            </ul>
            				        </div>
            				        <div class="app-short">
            				            <p>Apartman LUX FLAT 20 nalazi se u Ul.Moše Albaharija 7, na 4. katu neposredno centra i svih važnijih sadržaja grada. Apartman površine 30 m2 je jednosoban sa kuhinjom koja...</p>
            				        </div>
            				        <div class="app-button">
            				            <a href="https://stage.mololongoaccommodation.com/accommodation/luxflat20/" class="button-primary">Rezerviraj</a>
            				        </div>
            				    </div>
            				    
            				</div>
            				                        </div>
                    </div>
                                    <div class="swiper-slide" data-swiper-slide-index="3" role="group" aria-label="4 / 5" style="width: 506.667px; margin-right: 40px;">
                        <div class="col-md-12">
                                                        <div class="app-box">
            				    <div class="app-image">
            				        <img width="900" height="600" src="https://stage.mololongoaccommodation.com/wp-content/uploads/2021/03/IMG_2135-900x600.jpg" class="attachment-medium size-medium wp-post-image" alt="" loading="lazy" srcset="https://stage.mololongoaccommodation.com/wp-content/uploads/2021/03/IMG_2135-900x600.jpg 900w, https://stage.mololongoaccommodation.com/wp-content/uploads/2021/03/IMG_2135-1200x800.jpg 1200w, https://stage.mololongoaccommodation.com/wp-content/uploads/2021/03/IMG_2135-600x400.jpg 600w, https://stage.mololongoaccommodation.com/wp-content/uploads/2021/03/IMG_2135-768x512.jpg 768w, https://stage.mololongoaccommodation.com/wp-content/uploads/2021/03/IMG_2135-1536x1024.jpg 1536w, https://stage.mololongoaccommodation.com/wp-content/uploads/2021/03/IMG_2135.jpg 1920w" sizes="(max-width: 900px) 100vw, 900px">            				        <span class="app-stars app_stars4"></span>
            				    </div>
            				    <div class="app-content">
            				        <div class="app-title">
            				            <h4>Adeona #1</h4>
            				            <span class="app-price">50 eur/noć</span>
            				        </div>
            				        <div class="app-icons">
            				            <ul class="small-icons">
            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/pin-color.svg" alt="Lokacija"> Rijeka	</li>
            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico_guests-color.svg" alt="Kapacitet"> 2 + 2</li>
            				                  
            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico_bed-color.svg" alt="Broj kreveta"> 1 + 1 </li>
            				                            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico-bath.svg" alt="Broj kupaona"> 1 </li>
            				                            				            </ul>
            				        </div>
            				        <div class="app-short">
            				            <p>Novouređeni apartman ADEONA 1 nalazi se u centru Rijeke, u Križanićevoj ulici br. 7, na prvom katu zgrade u pasažu s galerijama. Apartman je površine 32 m2 i pruža...</p>
            				        </div>
            				        <div class="app-button">
            				            <a href="https://stage.mololongoaccommodation.com/accommodation/adeona-1/" class="button-primary">Rezerviraj</a>
            				        </div>
            				    </div>
            				    
            				</div>
            				                        </div>
                    </div>
                                    <div class="swiper-slide swiper-slide-duplicate-prev" data-swiper-slide-index="4" role="group" aria-label="5 / 5" style="width: 506.667px; margin-right: 40px;">
                        <div class="col-md-12">
                                                        <div class="app-box">
            				    <div class="app-image">
            				        <img width="900" height="506" src="https://stage.mololongoaccommodation.com/wp-content/uploads/2021/06/villabveastwing_6-900x506.jpg" class="attachment-medium size-medium wp-post-image" alt="" loading="lazy" srcset="https://stage.mololongoaccommodation.com/wp-content/uploads/2021/06/villabveastwing_6-900x506.jpg 900w, https://stage.mololongoaccommodation.com/wp-content/uploads/2021/06/villabveastwing_6-1200x675.jpg 1200w, https://stage.mololongoaccommodation.com/wp-content/uploads/2021/06/villabveastwing_6-768x432.jpg 768w, https://stage.mololongoaccommodation.com/wp-content/uploads/2021/06/villabveastwing_6-1536x864.jpg 1536w, https://stage.mololongoaccommodation.com/wp-content/uploads/2021/06/villabveastwing_6.jpg 1920w" sizes="(max-width: 900px) 100vw, 900px">            				        <span class="app-stars app_stars4"></span>
            				    </div>
            				    <div class="app-content">
            				        <div class="app-title">
            				            <h4>Studio East Wing</h4>
            				            <span class="app-price">99 eur/noć</span>
            				        </div>
            				        <div class="app-icons">
            				            <ul class="small-icons">
            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/pin-color.svg" alt="Lokacija"> Opatija	</li>
            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico_guests-color.svg" alt="Kapacitet"> 2</li>
            				                  
            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico_bed-color.svg" alt="Broj kreveta"> 1 </li>
            				                            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico-bath.svg" alt="Broj kupaona"> 1 </li>
            				                            				            </ul>
            				        </div>
            				        <div class="app-short">
            				            <p>Četiri luksuzno opremljena apartmana čine ovu kuću za odmor vrhunskim izborom svakog hedonista. Prema ocjenama gostiju, ova kuća za odmor ima sve što je potrebno za istinski odmor duše...</p>
            				        </div>
            				        <div class="app-button">
            				            <a href="https://stage.mololongoaccommodation.com/accommodation/studio-east-wing/" class="button-primary">Rezerviraj</a>
            				        </div>
            				    </div>
            				    
            				</div>
            				                        </div>
                    </div>
                              <div class="swiper-slide swiper-slide-duplicate swiper-slide-duplicate-active" data-swiper-slide-index="0" role="group" aria-label="1 / 5" style="width: 506.667px; margin-right: 40px;">
                        <div class="col-md-12">
                                                        <div class="app-box">
            				    <div class="app-image">
            				        <img width="900" height="600" src="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_7483-900x600.jpg" class="attachment-medium size-medium wp-post-image" alt="" loading="lazy" srcset="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_7483-900x600.jpg 900w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_7483-1200x800.jpg 1200w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_7483-600x400.jpg 600w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_7483-768x512.jpg 768w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_7483-1536x1024.jpg 1536w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_7483.jpg 1995w" sizes="(max-width: 900px) 100vw, 900px">            				        <span class="app-stars app_stars4"></span>
            				    </div>
            				    <div class="app-content">
            				        <div class="app-title">
            				            <h4>Srnec Ciottina S2</h4>
            				            <span class="app-price">45 eur/noć</span>
            				        </div>
            				        <div class="app-icons">
            				            <ul class="small-icons">
            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/pin-color.svg" alt="Lokacija"> Rijeka	</li>
            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico_guests-color.svg" alt="Kapacitet"> 2 + 1</li>
            				                  
            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico_bed-color.svg" alt="Broj kreveta"> 1 + 1 </li>
            				                            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico-bath.svg" alt="Broj kupaona"> 1 </li>
            				                            				            </ul>
            				        </div>
            				        <div class="app-short">
            				            <p>Jednosobni apartman Srnec Ciottina S2 nalazi se u centru grada u Ciottinoj ulici 30, na prvome katu stambene zgrade. Ovaj moderno opremljen stan smješten je na 30 metara kvadratnih,...</p>
            				        </div>
            				        <div class="app-button">
            				            <a href="https://stage.mololongoaccommodation.com/accommodation/srnec-ciottina-s2/" class="button-primary">Rezerviraj</a>
            				        </div>
            				    </div>
            				    
            				</div>
            				                        </div>
                    </div><div class="swiper-slide swiper-slide-duplicate swiper-slide-duplicate-next" data-swiper-slide-index="1" role="group" aria-label="2 / 5" style="width: 506.667px; margin-right: 40px;">
                        <div class="col-md-12">
                                                        <div class="app-box">
            				    <div class="app-image">
            				        <img width="900" height="600" src="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_1044-Edit-900x600.jpg" class="attachment-medium size-medium wp-post-image" alt="" loading="lazy" srcset="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_1044-Edit-900x600.jpg 900w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_1044-Edit-1200x800.jpg 1200w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_1044-Edit-600x400.jpg 600w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_1044-Edit-768x512.jpg 768w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_1044-Edit-1536x1024.jpg 1536w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/07/IMG_1044-Edit.jpg 1995w" sizes="(max-width: 900px) 100vw, 900px">            				        <span class="app-stars app_stars3"></span>
            				    </div>
            				    <div class="app-content">
            				        <div class="app-title">
            				            <h4>Apartman &amp; sobe Forever – Soba #2</h4>
            				            <span class="app-price">35 eur/noć</span>
            				        </div>
            				        <div class="app-icons">
            				            <ul class="small-icons">
            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/pin-color.svg" alt="Lokacija"> Rijeka	</li>
            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico_guests-color.svg" alt="Kapacitet"> 2</li>
            				                  
            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico_bed-color.svg" alt="Broj kreveta"> 1 </li>
            				                            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico-bath.svg" alt="Broj kupaona"> 1 </li>
            				                            				            </ul>
            				        </div>
            				        <div class="app-short">
            				            <p>Forever Room #2 nalazi se u samome centru grada Rijeka, na adresi Matije Gupca 20 na drugome katu stambene zgrade. Smještena je na 16 metara kvadratnih, a sadrži veliki...</p>
            				        </div>
            				        <div class="app-button">
            				            <a href="https://stage.mololongoaccommodation.com/accommodation/apartments-rooms-forever-room-2/" class="button-primary">Rezerviraj</a>
            				        </div>
            				    </div>
            				    
            				</div>
            				                        </div>
                    </div><div class="swiper-slide swiper-slide-duplicate" data-swiper-slide-index="2" role="group" aria-label="3 / 5" style="width: 506.667px; margin-right: 40px;">
                        <div class="col-md-12">
                                                        <div class="app-box">
            				    <div class="app-image">
            				        <img width="900" height="590" src="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/LuxFlat20-11-900x590.jpg" class="attachment-medium size-medium wp-post-image" alt="" loading="lazy" srcset="https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/LuxFlat20-11-900x590.jpg 900w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/LuxFlat20-11-1200x787.jpg 1200w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/LuxFlat20-11-768x504.jpg 768w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/LuxFlat20-11-1536x1007.jpg 1536w, https://stage.mololongoaccommodation.com/wp-content/uploads/2020/12/LuxFlat20-11.jpg 1920w" sizes="(max-width: 900px) 100vw, 900px">            				        <span class="app-stars app_stars4"></span>
            				    </div>
            				    <div class="app-content">
            				        <div class="app-title">
            				            <h4>LuxFlat20</h4>
            				            <span class="app-price">45 eur/noć</span>
            				        </div>
            				        <div class="app-icons">
            				            <ul class="small-icons">
            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/pin-color.svg" alt="Lokacija"> Rijeka	</li>
            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico_guests-color.svg" alt="Kapacitet"> 2 + 1</li>
            				                  
            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico_bed-color.svg" alt="Broj kreveta"> 1 + 1 </li>
            				                            				                            				                <li><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/ico-bath.svg" alt="Broj kupaona"> 1 </li>
            				                            				            </ul>
            				        </div>
            				        <div class="app-short">
            				            <p>Apartman LUX FLAT 20 nalazi se u Ul.Moše Albaharija 7, na 4. katu neposredno centra i svih važnijih sadržaja grada. Apartman površine 30 m2 je jednosoban sa kuhinjom koja...</p>
            				        </div>
            				        <div class="app-button">
            				            <a href="https://stage.mololongoaccommodation.com/accommodation/luxflat20/" class="button-primary">Rezerviraj</a>
            				        </div>
            				    </div>
            				    
            				</div>
            				                        </div>
                    </div></div>
            
              <!-- If we need navigation buttons -->
              <div class="swiper-button-prev pos-tr" tabindex="0" role="button" aria-label="Previous slide" aria-controls="swiper-wrapper-fddeb31f11dd4d7e"></div>
              <div class="swiper-button-next pos-tr" tabindex="0" role="button" aria-label="Next slide" aria-controls="swiper-wrapper-fddeb31f11dd4d7e"></div>

            <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
        </div>
    </div>
</section>

<div class="mobile-action">
    <a href="#sidebar-form">Rezerviraj</a>
</div>

<?php

/**

 * @hooked \MPHB\Views\SingleRoomTypeView::renderPageWrapperEnd - 10

 */

do_action( 'mphb_render_single_room_type_wrapper_end' );

?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCAPbxPreRwwIHsYRYFuC2jJZcRC660tXQ&libraries=places"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/markerclusterer.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/sticky-sidebar.min.js"></script>

<script>
    function initMap(){
          // map options
          var options = {
            zoom:10,
            center:{lat:45.328070893324586, lng:14.437667978788184},
			mapTypeId: "roadmap",
    		// disableDefaultUI: true,
			styles: [

				{
					featureType: "all",
					elementType: "all",
					stylers: [
					  { "saturation": -100 }
					]
				},
				{
					"featureType": "landscape",
					"elementType": "all",
					"stylers": [
						{ "saturation": -100 },
						{ "lightness": 65 },
						{ "visibility": "on" }
					]
				},				
				
				{
					"featureType": "water",
					"elementType": "geometry",
					"stylers": [
						{ "hue": "#ffff00" },
						{ "lightness": -10 },
						{ "saturation": -100 }
					]
				},
				{
					"featureType": "water",
					"elementType": "labels",
					"stylers": [
						{ "lightness": -25 },
						{ "saturation": -100 }
					]
				}
			]
          }
    var map = new google.maps.Map(document.getElementById('dd_map'),options);

          var markers = [
					
						{
						  coords:{lat:45.328070893324586, lng:14.437667978788184},
						  iconImage:'https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/map_pin.png',
						  content:
							'<p class="dd_map_title">Harbour View #2</p>' +
							'<p class="dd_map_p"><span class="room_stars room_stars_left4"></span></p>' +
							
							'<p class="dd_map_p"><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/pin.png" />Trpimirova ulica 6, Rijeka</p>' +
							
							'<p class="dd_map_p"><span class="dd_map_half"><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/map_guests.png" />2 &#43; 1</span><span class="dd_map_half"><img src="https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/map_beds.png" />1 + 1</span></p>' +
							'<p class="dd_map_p">Cijena od: <strong>45 € <small>za 3 noći</small></strong></p>' +
							'<a class="button dd_map_button dd_map_button2" target="_blank" href="https://goo.gl/maps/19q5rGGcVznRrmyMA" target="_blank">Pogledaj na karti</a>'
						},					
          ];
          // Loop through markers
          var gmarkers = [];
          for(var i = 0; i < markers.length; i++){
            gmarkers.push(addMarker(markers[i]));

          }

          //Add MArker function
          function addMarker(props){
            var marker = new google.maps.Marker({
              position:props.coords,
              map:map,

            });

            if(props.iconImage){
              marker.setIcon(props.iconImage);
            }

            //Check content
            if(props.content){
              var infoWindow = new google.maps.InfoWindow({
                content:props.content
              });
              marker.addListener('click',function(){
                infoWindow.open(map,marker);
              });
            }
            return marker;
          }
		  
        var markerCluster = new MarkerClusterer(map, gmarkers, 
          {
            // for default marker sizes (m1, m2, m3...)
			//imagePath: 'https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/m'
			
			styles:[{
			
				url: "https://stage.mololongoaccommodation.com/wp-content/themes/mololongo/images/m5.png",
					width: 90,
					height:90,
					fontFamily:"Nunito Sans",
					textSize:16,
					textColor:"#ffffff",
					//color: #00FF00,
			}]			
			
          });
		  
        }
    google.maps.event.addDomListener(window, 'load', initMap);
</script>
<?php get_footer('single2'); ?>