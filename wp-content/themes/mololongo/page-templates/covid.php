<?php
/**
 * Template Name: Full Width Covid Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package MoloLongo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'mololongo_container_type' );

if ( is_front_page() ) {
	get_template_part( 'global-templates/hero' );
}
?>
<?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>
<section class="small-hero" style="background: url('<?php echo $backgroundImg[0]; ?>') ;background-position: center;
    background-repeat: no-repeat;
    background-size: cover;">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center intro-width">
                <h1><?php the_title();?></h1>
                <?php if( get_field('intro_text') ): ?>
                	<p class="small-desc"><?php the_field('intro_text'); ?></p>
                <?php endif; ?>
                
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12 intro-width">
                <?php
					while ( have_posts() ) {
						the_post();
						the_content();
					}
					?>
            </div>
        </div>
    </div>
</section>
<?php if( get_field('slika_lijevo') || get_field('tekst_desno') ): ?>
<section>
    <div class="container">
        <div class="row intro-width">
            <div class="col-md-6">
                <?php 
                $image = get_field('slika_lijevo');
                if( !empty( $image ) ): ?>
                    <div class="round-img h350">
                        <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                    </div>
                <?php endif; ?>
                
            </div>
            <div class="col-md-6">
                <?php if( get_field('tekst_desno') ): ?>
                	<p class="small-desc"><?php the_field('tekst_desno'); ?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<?php if( get_field('slika_desno') || get_field('tekst_lijevo_2') ): ?>
<section>
    <div class="container">
        <div class="row intro-width">
            <div class="col-md-6">
                <?php if( get_field('tekst_lijevo_2') ): ?>
                	<p class="small-desc"><?php the_field('tekst_lijevo_2'); ?></p>
                <?php endif; ?>
            </div>
            <div class="col-md-6">
                <?php 
                $image = get_field('slika_desno');
                if( !empty( $image ) ): ?>
                    <div class="round-img h350">
                        <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                    </div>
                <?php endif; ?>
                
            </div>
            
        </div>
    </div>
</section>
<?php endif; ?>
<?php if( get_field('tekst_lijevo') ||  get_field('iframe_desno') ): ?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="info-box text-right">
                    <?php if( get_field('tekst_lijevo') ): ?>
                    	<p class="small-desc"><?php the_field('tekst_lijevo'); ?></p>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="map-position">
                    <?php if( get_field('iframe_desno') ): ?>
                    	<p class="small-desc"><?php the_field('iframe_desno'); ?></p>
                    <?php endif; ?>
                </div>
            </div>
            
        </div>
    </div>
</section>
<?php endif; ?>
<?php if( have_rows('slika_1')): ?>
<section class="image-boxes">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?php if( have_rows('slika_1') ): while ( have_rows('slika_1') ) : the_row(); ?>
                <div class="single-image-box">
                    <div class="single-image-box_bg" style="background: url('<?php the_sub_field('pozadinska_slika_1'); ?>') ;background-position: center;background-repeat: no-repeat;background-size: cover;"><div class="overlay"></div></div>
                    <div class="single-image-box_content">
                        <h5><?php the_sub_field('naslov_slike_1'); ?></h5>
                        <p><?php the_sub_field('tekst_slike_1'); ?></p>
                    </div>
                    
                </div>
                <?php endwhile; endif; ?>
            </div>
            <div class="col-md-3">
                <?php if( have_rows('slika_2') ): while ( have_rows('slika_2') ) : the_row(); ?>
                <div class="single-image-box">
                    <div class="single-image-box_bg" style="background: url('<?php the_sub_field('pozadinska_slika_2'); ?>') ;background-position: center;background-repeat: no-repeat;background-size: cover;"><div class="overlay"></div></div>
                    <div class="single-image-box_content">
                        <h5><?php the_sub_field('naslov_slike_2'); ?></h5>
                        <p><?php the_sub_field('tekst_slike_2'); ?></p>
                    </div>
                    
                </div>
                <?php endwhile; endif; ?>
            </div>
            <div class="col-md-3">
                <?php if( have_rows('slika_3') ): while ( have_rows('slika_3') ) : the_row(); ?>
                <div class="single-image-box">
                    <div class="single-image-box_bg" style="background: url('<?php the_sub_field('pozadinska_slika_3'); ?>') ;background-position: center;background-repeat: no-repeat;background-size: cover;"><div class="overlay"></div></div>
                    <div class="single-image-box_content">
                        <h5><?php the_sub_field('naslov_slike_3'); ?></h5>
                        <p><?php the_sub_field('tekst_slike_3'); ?></p>
                    </div>
                    
                </div>
                <?php endwhile; endif; ?>
            </div>
            <div class="col-md-3">
                <?php if( have_rows('slika_4') ): while ( have_rows('slika_4') ) : the_row(); ?>
                <div class="single-image-box">
                    <div class="single-image-box_bg" style="background: url('<?php the_sub_field('pozadinska_slika_4'); ?>') ;background-position: center;background-repeat: no-repeat;background-size: cover;"><div class="overlay"></div></div>
                    <div class="single-image-box_content">
                        <h5><?php the_sub_field('naslov_slike_4'); ?></h5>
                        <p><?php the_sub_field('tekst_slike_4'); ?></p>
                    </div>
                    
                </div>
                <?php endwhile; endif; ?>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<?php if( get_field('naslov_sekcije')  ): ?>
<section class="help-box">
    <div class="container">
        <div class="row  intro-width">
            <div class="col-md-12">
                <?php if( get_field('naslov_sekcije') ): ?>
                	<h3 class="text-center"><?php the_field('naslov_sekcije'); ?></h3>
                <?php endif; ?>
            </div>
            <div class="col-md-4">
                <?php if( have_rows('box_1') ): while ( have_rows('box_1') ) : the_row(); ?>
                <div class="help_sinlge-box">
                    <div class="help_sinlge-box-icon">
                        <?php if( get_sub_field('ikona_1') ): ?>
                            <img src="<?php the_sub_field('ikona_1'); ?>" />
                        <?php endif; ?>
                    </div>
                    <div class="help_sinlge-box-title"><a href="<?php the_sub_field('link_polja'); ?>"><h4><?php the_sub_field('naslov_kucice_1'); ?></h4></a></div>
                </div>
                <?php endwhile; endif; ?>
            </div>
            <div class="col-md-4">
                <?php if( have_rows('box_2') ): while ( have_rows('box_2') ) : the_row(); ?>
                <div class="help_sinlge-box">
                    <div class="help_sinlge-box-icon">
                        <?php if( get_sub_field('ikona_2') ): ?>
                            <img src="<?php the_sub_field('ikona_2'); ?>" />
                        <?php endif; ?>
                    </div>
                    <div class="help_sinlge-box-title"><a href="<?php the_sub_field('link_polja_2'); ?>"><h4><?php the_sub_field('naslov_kucice_2'); ?></h4></a></div>
                </div>
                <?php endwhile; endif; ?>
            </div>
            <div class="col-md-4">
                <?php if( have_rows('box_3') ): while ( have_rows('box_3') ) : the_row(); ?>
                <div class="help_sinlge-box invert-box">
                    <div class="help_sinlge-box-icon">
                        <?php if( get_sub_field('ikona_3') ): ?>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/envelope_w.svg" />
                        <?php endif; ?>
                    </div>
                    <div class="help_sinlge-box-title"><a href="<?php the_sub_field('link_polja_3'); ?>"><h4><?php the_sub_field('naslov_kucice_3'); ?></h4></a></div>
                </div>
                <?php endwhile; endif; ?>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<?php if( get_field('pozadinska_slika')  ): ?>
<section class="parallax-cta" style="background: url('<?php the_field('pozadinska_slika'); ?>') ; background-repeat: no-repeat;background-size: cover;background-attachment:fixed">
    <div class="parallax-overlay"></div>
    <div class="container">
        <div class="row align-center">
            <div class="col-md-8 offset-md-2">
                <?php if( get_field('naslov_slike') ): ?>
                	<h4><?php the_field('naslov_slike'); ?></h4>
                <?php endif; ?>
            </div>
            <div class="col-md-2">
                <a href="/kontakt" class="button-color"><?php _e('Kontakt', 'molo'); ?></a>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<?php
get_footer();
