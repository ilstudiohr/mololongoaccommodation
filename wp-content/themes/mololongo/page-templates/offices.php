<section class="contact-office">
    <div class="container-sm">
        <div class="row">
            <div class="col-md-12">
                <h5><?php _e('Naši uredi', 'molonew'); ?></h5>
            </div>
            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Uredi") ) : ?>
            <?php endif;?>

            <div class="col-md-4">
                <div class="contact-box">
                    <div class="contact-title">
                        <h6><?php _e('Rijeka', 'molonew'); ?></h6>
                    </div>
                    <ul class="contact-list">
                        <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/pin.svg">Trpimirova 1A</li>
                        <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/phone.svg">+385 51 452 883</li>
                        <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/envelope.svg">rijeka@mololongo.com</li>
                        <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clock.svg">10:00 – 23:00</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="contact-box">
                    <div class="contact-title">
                        <h6><?php _e('Opatija', 'molonew'); ?></h6>
                    </div>
                    <ul class="contact-list">
                        <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/pin.svg">Maršala Tita 39</li>
                        <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/phone.svg">+385 51 452 882</li>
                        <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/envelope.svg">opatija@mololongo.com</li>
                        <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clock.svg">Promjenjivo</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="contact-box">
                    <div class="contact-title">
                        <h6><?php _e('Crikvenica', 'molonew'); ?></h6>
                    </div>
                    <ul class="contact-list">
                        <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/pin.svg">Kralja Tomislava 109</li>
                        <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/phone.svg">+385 51 452 884</li>
                        <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/envelope.svg">crikvenica@mololongo.com</li>
                        <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clock.svg">Promjenjivo</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
