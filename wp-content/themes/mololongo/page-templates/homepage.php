<?php
/**
 * Template Name: Home Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package MoloLongo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'mololongo_container_type' );
search_submit();

?>

<?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>
<section class="home-hero">
    <video width="100%" loop autoplay muted>
        <source src="<?php echo get_template_directory_uri(); ?>/images/summer-in-croatia.mp4" type="video/mp4">
        Your browser does not support HTML video.
    </video>
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="hero-title">
                    <div class="filter-box filter-box--or--homepage">
			<button class="mobile--modal"><span><img src="<?php bloginfo('template_directory');?>/images/search_magnifier.svg" alt="search" /></span><?php _e('Lokacija', 'molonew'); ?></button>
                        <div class="filter-wrapper mobile--form">
			    <button style="display: none;" class="mobile--modal-close"><img src="<?php bloginfo('template_directory');?>/images/closemolo.svg" alt="close"></button>
                            <form method="GET" class="mphb_sc_search-form" action="<?php echo get_site_url(); ?><?php _e('/rezultati-pretrage/', 'molonew'); ?>">
                            <!--<p class="mphb_sc_search-location ui-widget border-tr mphb_sc_search-location-homepage--or dropdown-option mphb_sc_search-grad">
                                <select id="mphb_grad-mphb-search-form" name="mphb_ra_grad" autocomplete="off" class="left-radius" placeholder="<?php _e('Lokacija', 'molonew'); ?>">
                                    <option value=""><?php _e('Lokacija', 'molonew'); ?></option>
                                    <option value="rijeka"><?php _e('Rijeka', 'molonew'); ?></option>
                                    <option value="opatija"><?php _e('Opatija', 'molonew'); ?></option>
                                    <option value="crikvenica"><?php _e('Crikvenica', 'molonew'); ?></option>
                                    <option value="krk"><?php _e('Krk', 'molonew'); ?></option>
                                </select>
                            </p>  -->
                            <p class="mphb_sc_search-location ui-widget border-tr mphb_sc_search-location-homepage--or">
                                <input id="mphb_grad-mphb-search-form" class="left-radius" name="q" autocomplete="off" placeholder="<?php _e('Lokacija', 'molonew'); ?>">
                            </p> 
                            <!--<p class="mphb_sc_search-location ui-widget border-tr mphb_sc_search-location-homepage--or">
                                <input id="mphb_grad-mphb-search-form" name="q" autocomplete="off" class="left-radius" placeholder="<?php _e('Lokacija', 'molonew'); ?>">
                            </p> -->
                            <div class="t-datepicker">
                                <p class="mphb_sc_search-check-in-date t-check-in"></p>
                                <p class="mphb_sc_search-check-out-date t-check-out"></p>
                            </div>
                            <input type="hidden" id="mphb_adults-mphb-search-form" name="mphb_adults" value="1" />
                            <input type="hidden" id="mphb_children-mphb-search-form" name="mphb_children" value="0" />

                            <div class="modal-toggle-range--wrapp modal-toggle-range--wrapp--homepage modal-guests modal-toggle-range-no-icon">
                               <button class="advanced-filters--or modal-toggle-range " type="button" id="dropdownMenuButton">
                                 <?php _e('Gosti', 'molonew'); ?> <span id="guestsOr"></span> <input type="text" id="Sum" value="" disabled/><i class="fa fa-angle-down guest-icon"></i>
                               </button>
                               <script>
                                function reSum()
                                {
                                    var num1 = parseInt(document.getElementById("adults").value);
                                    var num2 = parseInt(document.getElementById("children").value);
                                    document.getElementById("Sum").value = num1 + num2;
                        
                                }
                              </script>
                                <div class="dropdown-menu dropdown-menu--or" >
                                    <ul class="block detalji_smjestaja accommodation-details--or accommodation-details--or--homepage">
                                        <li class="sf-field-post-meta-basic_info_beds">
                                           <p><?php _e('Odrasli', 'molonew'); ?></p>
                                           <label>
                                            <div class="quantity">
                                              <input class="quantity__capacity" id="adults" type="number" onchange="reSum();" name="adults" min="1" step="1" max="20" value="1">
                                            </div>
                                          </label>
                                        </li>
                                        <li class="sf-field-post-meta-basic_info_beds">
                                           <p class="modal-guests__info">
                                            <span><?php _e('Djeca', 'molonew'); ?></span>
                                            <span><?php _e('ispod 18 godina', 'molonew'); ?></span>
                                        </p>
                                           <label>
                                            <div class="quantity">
                                              <input class="quantity__capacity" id="children" type="number" name="children" onchange="reSum();" min="0" step="1" max="20" value="0">
                                            </div>
                                          </label>
                                        </li>
                                        
                                        <li>
                                            <input type="hidden" id="capacity" type="number" min="1" max="40" value="" name="mphb_attributes[kapacitet]">
                                        </li>
                                        <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">
                                          <div class="checkbox--or">
                                            <input  class="sf-input-checkbox" type="checkbox" value="pet_friendly" name="_sfm_search_amenities[]" id="sf-input-4922c9d1395f7191f2e7f935350a656d">
                                            <label for="sf-input-4922c9d1395f7191f2e7f935350a656d"><?php _e('Pet friendly', 'molonew'); ?></label>
                                          </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>     
                            <input id="mphb_check_in_date-mphb-search-form-hidden" value="" type="hidden" name="mphb_check_in_date" />
                            <input id="mphb_check_out_date-mphb-search-form-hidden" value="" type="hidden" name="mphb_check_out_date" />
                            <p class="mphb_sc_search-submit-button-wrapper mphb_sc_search-submit-button-wrapper--homepage">
                            <input type="submit" class="button" value="<?php _e('Pretraži', 'molonew'); ?>"/>
                            </p>
				<?php get_template_part( 'search-res-templates/adv', 'search-homepage' ); ?>
                        </form>
                    	   
                        </div>
                    </div>
                    <p class="desc"><?php _e('Doživi. Istraži. Osjeti.', 'molonew'); ?></p>

                </div>


            </div>
        </div>
    </div>
</section>
<section class="app-list">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="h1"><?php _e('Za vas izdvajamo', 'molonew'); ?></h2>
            </div>
            <div class="swiper swiper-featured pt-5 mt-n5 pb-4">
                <div class="swiper-wrapper">
                <?php $args = array('posts_per_page' => 5, 'orderby' => 'menu_order', 'order' => 'ASC','post_type' => 'mphb_room_type','tax_query' => array(array('taxonomy' => 'mphb_room_type_category', 'field' => 'slug', 'terms' => array('izdvojeno')))); $new = new WP_Query( $args ); if ( have_posts() ) while ($new->have_posts()) : $new->the_post(); ?>
                    <div class="swiper-slide">
                        <div class="col-md-12">
                            <?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>
                            <div class="app-box">
            				    <div class="app-image">
            				        <div class="swiper swiper-id" id="post-<?php the_ID(); ?>">
            				            <div class="swiper-wrapper">
            				                <?php $images = get_field('room_slider'); if ($images) { $counter = 1; ?>
                                                <?php foreach( $images as $image ) { ?>	
            				                        <div class="swiper-slide">
            				                            <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
            				                        </div>
        				                        <?php $counter++; if ($counter == 10) { break; }} ?>
                                            <?php } ?>
                                            
            				            </div>
            				            <div class="swiper-pagination"></div>
            				            <div class="swiper-button-prev"></div>
                                            <div class="swiper-button-next"></div>
            				        </div>
      
            				        <span class="app-stars app_stars<?php the_sub_field('stars'); ?>"></span>
            				        <!--<p class="product-price-or"><?php the_sub_field('price'); ?> <?php _e('eur/noć', 'molonew'); ?></p>-->
            				    </div>
            				    <div class="app-content">
            				        <a class="link-abs" href="<?php the_permalink() ?>"></a>
            				        <div class="app-title">
            				            <h4><?php the_title(); ?></h4>
            				            <!-- <span class="app-price"><?php the_sub_field('price'); ?> <?php _e('eur/noć', 'molonew'); ?></span> -->
            				        </div>
            				        <div class="app-icons">
            				            <ul class="small-icons">
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/pin-color.svg" alt="<?php _e('Lokacija', 'molonew'); ?>"> <?php the_sub_field('city'); ?>	</li>
            				                <?php if( get_sub_field('basic_capacity') ): ?>
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico_guests-color.svg" alt="<?php _e('Kapacitet', 'molonew'); ?>"> <?php the_sub_field('basic_capacity'); if( get_sub_field('spare_capacity') ): ?> + <?php the_sub_field('spare_capacity'); endif; ?></li>
            				                <?php endif; ?>
            				                <?php if( get_sub_field('beds') ): ?>
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico_bed-color.svg" alt="<?php _e('Broj kreveta', 'molonew'); ?>"> <?php the_sub_field('beds'); if( get_sub_field('spare_beds') ): ?> + <?php the_sub_field('spare_beds'); endif; ?> </li>
            				                <?php endif; ?>
            				                <?php if( get_sub_field('bathrooms') ): ?>
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/bath.svg" alt="<?php _e('Broj kupaona', 'molonew'); ?>"> <?php the_sub_field('bathrooms'); ?> </li>
            				                <?php endif; ?>
            				            </ul>
            				        </div>
            				        <div class="app-short">
            				            <p><?php echo excerpt(22); ?></p>
            				        </div>
            				        <div class="app-button">
            				            <a href="<?php the_permalink() ?>" class="button-primary"><?php _e('Rezerviraj', 'molonew'); ?></a>
            				        </div>
            				    </div>

            				</div>
            				<?php endwhile; endif; ?>
                        </div>
                    </div>
                <?php endwhile; wp_reset_query(); ?>
              </div>

              <!-- If we need navigation buttons -->
              <div class="swiper-button-prev pos-tr"></div>
              <div class="swiper-button-next pos-tr"></div>

            </div>
        </div>
    </div>
</section>
<section class="sep">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="separator"></div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row mobile-order">
            <div class="col-md-6 mobile-two">
                <div class="row">
                    <?php if( have_rows('box_1') ): while ( have_rows('box_1') ) : the_row(); ?>
                    <div class="col-md-6">
                        <div class="shadow-box">
                            <div class="box-icon">
                                <img src="<?php the_sub_field('ikona'); ?>" alt="<?php the_title();?>" style="width:50px">
                            </div>
                            <div class="box-title">
                                <?php the_sub_field('naslov'); ?>
                            </div>
                            <div class="box-content">
                                <?php the_sub_field('tekst'); ?>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; endif; ?>
                    <?php if( have_rows('box_2') ): while ( have_rows('box_2') ) : the_row(); ?>
                    <div class="col-md-6 pt-5">
                        <div class="shadow-box">
                            <div class="box-icon">
                                <img src="<?php the_sub_field('ikona'); ?>" alt="<?php the_title();?>" style="width:50px">
                            </div>
                            <div class="box-title">
                                <?php the_sub_field('naslov'); ?>
                            </div>
                            <div class="box-content">
                                <?php the_sub_field('tekst'); ?>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; endif; ?>
                </div>
                <div class="row mt-md-n5">
                    <?php if( have_rows('box_3') ): while ( have_rows('box_3') ) : the_row(); ?>
                    <div class="col-md-6 pt-xs-5">
                        <div class="shadow-box">
                            <div class="box-icon">
                                <img src="<?php the_sub_field('ikona'); ?>" alt="<?php the_title();?>" style="width:50px">
                            </div>
                            <div class="box-title">
                                <?php the_sub_field('naslov'); ?>
                            </div>
                            <div class="box-content">
                                <?php the_sub_field('tekst'); ?>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; endif; ?>
                    <?php if( have_rows('box_4') ): while ( have_rows('box_4') ) : the_row(); ?>
                    <div class="col-md-6 pt-5">
                        <div class="shadow-box">
                            <div class="box-icon">
                                <img src="<?php the_sub_field('ikona'); ?>" alt="<?php the_title();?>" style="width:50px">
                            </div>
                            <div class="box-title">
                                <?php the_sub_field('naslov'); ?>
                            </div>
                            <div class="box-content">
                                <?php the_sub_field('tekst'); ?>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; endif; ?>
                </div>
            </div>
            
            <div class="col-md-6 mobile-one">
                <div class="pl-md-5 pt-10">
                    <h3 class="h1"><?php the_field('naslov_teksta_desno'); ?></h3>
                    <?php the_field('tekst_desno_2'); ?>
                    <div class="image-box_social">
                        <img class="rounded mt-5 small" src="<?php bloginfo('template_directory'); ?>/images/osjecaj.jpg">
                        <div class="image-share">
                            <div class="share-title"><?php _e('Pratite nas na', 'molonew'); ?></div>
                            <ul class="social-list_contact social-footer">
                                <li><a href="https://www.instagram.com/mololongoapartments/" target="_blank"><img width="20" src="<?php bloginfo('template_directory'); ?>/images/instagram.svg" class="img-fluid" alt="<?php the_title();?>"></a></li>
        		        <li><a href="https://www.facebook.com/mololongoapartments" target="_blank"><img width="20" src="<?php bloginfo('template_directory'); ?>/images/facebook.svg" class="img-fluid" alt="<?php the_title();?>"></a></li>
        		        <li><a href="https://www.linkedin.com/company/molo-longo" target="_blank"><img width="20" src="<?php bloginfo('template_directory'); ?>/images/linkedin.svg" class="img-fluid" alt="<?php the_title();?>"></a></li>
                            </ul>
                        </div>
                    </div>

                </div>

            </div>
            
        </div>
    </div>
</section>
<section class="sep">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="separator"></div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12 intro-text">
                <h4 class="text-center h2"><?php the_field('naslov_destinacija'); ?></h4>
                <div class="text-center"><?php the_field('tekst_destinacija'); ?></div>
            </div>
        </div>
        <div class="row small_gutter">
            <div class="col-md-4">
                
                <div class="image-title">
                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/dalmacija.jpg">
                    <div class="image-content">
                        <h5 class="h2"><?php the_field('naslov_destinacija_1'); ?></h5>
                        <p><?php the_field('tekst_destinacija_1'); ?></p>
                    </div>
                </div>
                
                <div class="image-title">
                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/dubrovnik.jpg">
                    <div class="image-content">
                         <h5 class="h2"><?php the_field('naslov_destinacija_2'); ?></h5>
                        <p><?php the_field('tekst_destinacija_2'); ?></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="image-title full-height">
                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/istra.jpg">
                    <div class="image-content">
                         <h5 class="h2"><?php the_field('naslov_destinacija_3'); ?></h5>
                        <p><?php the_field('tekst_destinacija_3'); ?></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="image-title">
                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/kvarner.jpg">
                    <div class="image-content">
                        <h5 class="h2"><?php the_field('naslov_destinacija_4'); ?></h5>
                        <p><?php the_field('tekst_destinacija_4'); ?></p>
                    </div>
                </div>
                <div class="image-title">
                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/hrvatska.jpg">
                    <div class="image-content">
                         <h5 class="h2"><?php the_field('naslov_destinacija_5'); ?></h5>
                        <p><?php the_field('tekst_destinacija_5'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="sep">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="separator"></div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="cta"><?php _e('Tu smo da vama osiguramo', 'molonew'); ?> <span><?php _e('savršeno iskustvo odmora.', 'molonew'); ?><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 150" preserveAspectRatio="none"><path d="M3,146.1c17.1-8.8,33.5-17.8,51.4-17.8c15.6,0,17.1,18.1,30.2,18.1c22.9,0,36-18.6,53.9-18.6 c17.1,0,21.3,18.5,37.5,18.5c21.3,0,31.8-18.6,49-18.6c22.1,0,18.8,18.8,36.8,18.8c18.8,0,37.5-18.6,49-18.6c20.4,0,17.1,19,36.8,19 c22.9,0,36.8-20.6,54.7-18.6c17.7,1.4,7.1,19.5,33.5,18.8c17.1,0,47.2-6.5,61.1-15.6"></path></svg></span>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="parallax" style="background: url('https://dev.mololongo.com/wp-content/uploads/2021/06/Travex-11.jpg') ;background-size: cover;    background-attachment: fixed;">
    <div class="parallax-overlay"></div>
    <div class="shape-top">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
        	<path class="elementor-shape-fill" d="M421.9,6.5c22.6-2.5,51.5,0.4,75.5,5.3c23.6,4.9,70.9,23.5,100.5,35.7c75.8,32.2,133.7,44.5,192.6,49.7
        	c23.6,2.1,48.7,3.5,103.4-2.5c54.7-6,106.2-25.6,106.2-25.6V0H0v30.3c0,0,72,32.6,158.4,30.5c39.2-0.7,92.8-6.7,134-22.4
        	c21.2-8.1,52.2-18.2,79.7-24.2C399.3,7.9,411.6,7.5,421.9,6.5z"></path>
        </svg>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="icon_text">
                    <div class="round-icon">
                        <i class="fa fa-bed"></i>
                    </div>
                    <h4><?php the_field('naslov_standardi_1'); ?></h4>
                    <?php the_field('tekst_standardi_1'); ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="icon_text">
                    <div class="round-icon">
                        <i class="fa fa-pencil"></i>
                    </div>
                    <h4><?php the_field('naslov_standardi_3'); ?></h4>
                    <?php the_field('tekst_standardi_4'); ?>
                </div>
            </div>

            <div class="col-md-4">
                <div class="icon_text">
                    <div class="round-icon">
                        <i class="fa fa-shield"></i>
                    </div>
                    <h4><?php the_field('naslov_standardi_5'); ?></h4>
                    <?php the_field('tekst_standardi_6'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="small-space">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="text-center h1"><?php _e('Jeste li već izabrali svoj smještaj?', 'molonew'); ?></h4>
            </div>
        </div>
    </div>
</section>
<section class="app-list pt-0">
    <div class="container">
        <div class="row">
            <div class="swiper swiper-list pt-5 mt-n5 pb-2">
                <div class="swiper-wrapper">
                    <?php $args = array('posts_per_page' => 5, 'orderby' => 'menu_order', 'order' => 'ASC','post_type' => 'mphb_room_type'); $new = new WP_Query( $args ); if ( have_posts() ) while ($new->have_posts()) : $new->the_post(); ?>
                    <div class="swiper-slide">
                        <div class="col-md-12">
                            <?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>
                            <div class="app-box">
            				    <div class="app-image">
            				        <div class="swiper swiper-id" id="post-<?php the_ID(); ?>">
            				            <div class="swiper-wrapper">
            				                <?php $images = get_field('room_slider'); if ($images) { $counter = 1; ?>
                                                <?php foreach( $images as $image ) { ?>	
            				                        <div class="swiper-slide">
            				                            <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
            				                        </div>
        				                        <?php $counter++; if ($counter == 10) { break; }} ?>
                                            <?php } ?>
                                            
            				            </div>
            				            <div class="swiper-pagination"></div>
            				            <div class="swiper-button-prev"></div>
                                            <div class="swiper-button-next"></div>
            				        </div>
            				        <span class="app-stars app_stars<?php the_sub_field('stars'); ?>"></span>
            				    </div>
            				    <div class="app-content">
            				        <a class="link-abs" href="<?php the_permalink() ?>"></a>
            				        <div class="app-title">
            				            <h4><?php the_title(); ?></h4>
            				           <!-- <p class="product-price-or"><?php the_sub_field('price'); ?> <?php _e('eur/noć', 'molonew'); ?></p>-->
            				        </div>
            				        <div class="app-icons">
            				            <ul class="small-icons">
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/pin-color.svg" alt="<?php _e('Lokacija', 'molonew'); ?>"> <?php the_sub_field('city'); ?>	</li>
            				                <?php if( get_sub_field('basic_capacity') ): ?>
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico_guests-color.svg" alt="<?php _e('Kapacitet', 'molonew'); ?>"> <?php the_sub_field('basic_capacity'); if( get_sub_field('spare_capacity') ): ?> + <?php the_sub_field('spare_capacity'); endif; ?></li>
            				                <?php endif; ?>
            				                <?php if( get_sub_field('beds') ): ?>
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico_bed-color.svg" alt="<?php _e('Broj kreveta', 'molonew'); ?>"> <?php the_sub_field('beds'); if( get_sub_field('spare_beds') ): ?> + <?php the_sub_field('spare_beds'); endif; ?> </li>
            				                <?php endif; ?>
            				                <?php if( get_sub_field('bathrooms') ): ?>
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/bath.svg" alt="<?php _e('Broj kupaona', 'molonew'); ?>"> <?php the_sub_field('bathrooms'); ?> </li>
            				                <?php endif; ?>
            				            </ul>
            				        </div>
            				        <div class="app-short">
            				            <p><?php echo excerpt(22); ?></p>
            				        </div>
            				        <div class="app-button">
            				            <a href="<?php the_permalink() ?>" class="button-primary"><?php _e('Rezerviraj', 'molonew'); ?></a>
            				        </div>
            				    </div>

            				</div>
            				<?php endwhile; endif; ?>
                        </div>
                    </div>
                    <?php endwhile; wp_reset_query(); ?>

                </div>

              <!-- If we need navigation buttons -->
              <div class="swiper-button-prev pos-tr"></div>
              <div class="swiper-button-next pos-tr"></div>

            </div>
        </div>
    </div>
</section>
<?php if(ICL_LANGUAGE_CODE!='de' && ICL_LANGUAGE_CODE!='it'): ?>
<section class="small-space latest-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-content intro-text" >
                    <h4 class="text-center h2"><?php _e('Novosti iz područja putovanja i turizma', 'molonew'); ?></h4>
                </div>
            </div>
            <?php
            $args = array(
                'posts_per_page' => 3, /* how many post you need to display */
                'offset' => 0,
                'orderby' => 'post_date',
                'order' => 'DESC',
                'post_type' => 'post', /* your post type name */
                'post_status' => 'publish'
            );
            $query = new WP_Query($args);
                if ($query->have_posts()) :
                    $i = 1;
                    while ($query->have_posts()) : $query->the_post();
                ?>
                <div class="<?php if ($i == 1){echo 'col-md-6';}elseif ($i == 2){echo 'col-md-4';}else{echo 'col-md-2';};?>">

                    <div class="image-title">
                        <div class="overlay"></div>
                        <?php the_post_thumbnail('medium'); ?>
                        <div class="image-content">
                            <h5><?php the_title();?></h5>
                            <p><?php echo excerpt(15); ?></p>
                            <div class="image-button">
                                <a href="<?php the_permalink();?>" class="button"><?php _e('Pročitaj', 'molonew'); ?></a>
                            </div>

                        </div>
                    </div>
                </div>
                <?php
                    $i++;
                endwhile;
            endif;
            ?>
        </div>
    </div>
</section>
<?php endif; ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php if( get_field('pozadinska_slika')  ): ?>
<section class="parallax-cta" style="background: url('<?php the_field('pozadinska_slika'); ?>') ; background-repeat: no-repeat;background-size: cover;background-attachment:fixed">
    <div class="parallax-overlay"></div>
    <div class="container">
        <div class="row align-center">
            <div class="col-md-8">
                <?php if( get_field('naslov_slike') ): ?>
                	<h4><?php the_field('naslov_slike'); ?></h4>
                <?php endif; ?>
            </div>
            <div class="col-md-4 text-center">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?><?php _e('iznajmite-nekretninu', 'molonew'); ?>" class="button-color big-button"><?php _e('Kontakt', 'molonew'); ?></a>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<?php endwhile;endif; ?>

</head>


<?php
get_footer();
