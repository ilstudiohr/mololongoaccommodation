<?php
/*
Template Name: Group
*/
?>

<?php get_header(); ?>	
<?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>
<section class="small-hero" style="background: url('<?php echo $backgroundImg[0]; ?>') ;background-position: center;
    background-repeat: no-repeat;
    background-size: cover;">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1><?php the_field('intro_title'); ?></h1>
                <?php if( get_field('intro_text') ): ?>
                	<p class="small-desc"><?php the_field('intro_text'); ?></p>
                <?php endif; ?>
                
            </div>
        </div>
    </div>
</section>	
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <?php if( have_rows('full_service_1') ): while ( have_rows('full_service_1') ) : the_row(); ?>
                <div class="shadow-box h-auto">
                    <div class="box-icon">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_booking.svg" alt="<?php _e('Booking', 'molonew'); ?>" />
                    </div>
                    <div class="box-title">
                        <h4 class="h5"><?php the_sub_field('naslov'); ?></h4>
                    </div>
                    <div class="box-content">
                        <?php the_sub_field('tekst');?>  
                    </div>
                </div>
                <?php endwhile; endif; ?>	
            </div>
            <div class="col-md-4">
                <?php if( have_rows('full_service_2') ): while ( have_rows('full_service_2') ) : the_row(); ?>
                <div class="shadow-box h-auto">
                    <div class="box-icon">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_monitor.svg" alt="<?php _e('Monitor', 'molonew'); ?>" />
                    </div>
                    <div class="box-title">
                        <h4 class="h5"><?php the_sub_field('naslov'); ?></h4>
                    </div>
                    <div class="box-content">
                        <?php the_sub_field('tekst');?>  
                    </div>
                </div>
                <?php endwhile; endif; ?>	
            </div>
            <div class="col-md-4">
                <?php if( have_rows('full_service_3') ): while ( have_rows('full_service_3') ) : the_row(); ?>
                <div class="shadow-box h-auto">
                    <div class="box-icon">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_pillow.svg" alt="<?php _e('Jastuk', 'molonew'); ?>" />
                    </div>
                    <div class="box-title">
                        <h4 class="h5"><?php the_sub_field('naslov'); ?></h4>
                    </div>
                    <div class="box-content">
                        <?php the_sub_field('tekst');?>  
                    </div>
                </div>
                <?php endwhile; endif; ?>	
            </div>
        </div>
    </div>
</section>
<section class="small-space" style="background: url('<?php echo esc_url(get_template_directory_uri()); ?>/images/Dots6.png') ;background-position: center left;background-repeat: no-repeat;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="page-content">
                    <span class="over-title"><?php the_field('podnaslov'); ?></span>
                    <h2><?php the_field('naslov_sekcije'); ?></h2>
                    <?php the_content(); ?>
                </div>
            </div>
            <div class="col-md-5 offset-md-1">
                <div class="page-content img-shape_lt">
                    <?php 
                    $image = get_field('slika');
                    if( !empty( $image ) ): ?>
                        <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="small-space">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2 text-center page-content image-boxes">
                <span class="over-title"><?php _e('Sve na jednom mjestu', 'molonew'); ?></span>
                <h3 class="h2 mb-5"><?php _e('Full service agencija u turizmu', 'molonew'); ?></h3>
                <div class="row">
                    <div class="col-md-6 mb-5">
                <div class="image-sub">
                    <div class="image-round mb-3">
                        <a href="https://mololongoaccommodation.com/" target="_blank"><img src="https://mololongoaccommodation.com/wp-content/uploads/2021/10/apartmani.jpg"></a>
                    </div>
                    <h4 class="mb-1 primary-color"><?php _e('Najam apartmana', 'molonew'); ?></h4>
                    <p><?php _e('Samo najbolje od hrvatske turističke ponude.', 'molonew'); ?></p>
                </div>
            </div>
            <div class="col-md-6 mb-5">
                <div class="image-sub">
                    <div class="image-round mb-3">
                        <a href="https://mololongoavillas.com/" target="_blank"><img src="https://mololongoaccommodation.com/wp-content/uploads/2021/10/vile.jpg"></a>
                    </div>
                    <h4 class="mb-1"><?php _e('Najam vila', 'molonew'); ?></h4>
                    <p><?php _e('Osjetite istinski luksuz.', 'molonew'); ?></p>
                </div>
            </div>
            

            <div class="col-md-6 mb-5">
                <div class="image-sub">
                    <div class="image-round mb-3">
                        <a href="https://mololongorealestate.com/" target="_blank"><img src="https://mololongoaccommodation.com/wp-content/themes/mololongo/images/molo.jpg"></a>
                    </div>
                    <h4 class="mb-1"><?php _e('Prodaja nekretnina', 'molonew'); ?></h4>
                    <p><?php _e('Uz vas smo od prve pretrage do preuzimanja ključeva.', 'molonew'); ?></p>
                </div>
            </div>

            <div class="col-md-6 mb-5">
                <div class="image-sub">
                    <div class="image-round mb-3">
                        <a href="https://mololongointeriors.com/" target="_blank"><img src="https://mololongoaccommodation.com/wp-content/uploads/2021/10/dizajn-interijera.jpg"></a>
                    </div>
                    <h4 class="mb-1"><?php _e('Dizajn interijera', 'molonew'); ?></h4>
                    <p><?php _e('Iznova definiramo lijepo.', 'molonew'); ?></p>
                </div>
            </div>
                </div>
            </div>
            


        </div>
    </div>
</section>
<section class="parallax-cta small-cta" style="background: url('/wp-content/themes/mololongo/images/villafiglica_8.jpg') ;;
    background-repeat: no-repeat;
    background-size: cover;
    background-attachment:fixed">
    <div class="parallax-overlay"></div>
    <div class="container">
        <div class="row align-center">
            <div class="col-md-8">
               <h4 class="mb-3"><?php _e('Što grupacija Molo Longo može učiniti za vas?', 'molonew'); ?></h4>
            </div>
            <div class="col-md-4 text-center">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?><?php _e('kontakt', 'molonew'); ?>" class="button-color big-button"><?php _e('Kontakt', 'molonew'); ?></a>
            </div>
        </div>
        
    </div>
</section>
<?php get_footer(); ?>	