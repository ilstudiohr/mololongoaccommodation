<?php
/**
 * Template Name: Blog Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package MoloLongo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'mololongo_container_type' );

?>
<?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>
<section class="small-hero" style="background: url('<?php echo $backgroundImg[0]; ?>') ;background-position: -50px 0px;background-repeat:no-repeat;">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="hero-title text-left">
                    <span class="over-title d-block second-font text-white"><?php _e('MOLO LONGO BLOG', 'molonew'); ?></span>
                    <?php _e('Donosimo vam novosti i savjete iz svijeta putovanja.', 'molonew'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="small-space">
    <div class="container">
        <div class="row">
            <?php 
            // the query
            $wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>-1)); ?>
             
            <?php if ( $wpb_all_query->have_posts() ) : ?>
             <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
            <div class="col-md-4">
                <div class="blog-post mb-3">
                    <div class="post-img">
                        <?php if(has_post_thumbnail())
                            { 
                            ?>
                                <img src="<?php the_post_thumbnail_url(); ?>" alt="" />
                            
                        <?php } ?>
                    </div>
                    <div class="post-content">
                        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                        <p class="mb-4"><?php echo excerpt(20);?></p>
                        <a class="simple-read" href="<?php the_permalink(); ?>"><?php _e('Pročitaj', 'molonew'); ?></a>
                    </div>
                    <div class="post-date">
                        <?php echo get_the_date(''); ?>
                    </div>
                </div>
                
            </div>
             <?php endwhile; ?>

                <?php wp_reset_postdata(); ?>
             
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>

            <div class="col-md-6">
                <div class="page-content">
                    <?php if( have_rows('intro_sekcija') ): while ( have_rows('intro_sekcija') ) : the_row(); ?>
                        <span class="over-title"><?php the_sub_field('podnaslov'); ?></span>
                        <h2><?php the_sub_field('naslov'); ?></h2>
                        <p><?php the_sub_field('tekst'); ?></p>
                        <a href="#" class="button-primary mt-3"><?php _e('Pretraži vile', 'molonew'); ?></a>
                    <?php endwhile; endif; ?>
                    
                </div>
            </div>
            <div class="col-md-6">
                <div class="page-content img-shape">
                    <?php if( have_rows('intro_sekcija') ): while ( have_rows('intro_sekcija') ) : the_row(); ?>
                        <?php 
                        $image = get_sub_field('slika');
                        if( !empty( $image ) ): ?>
                            <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                        <?php endif; ?>
                    <?php endwhile; endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();
