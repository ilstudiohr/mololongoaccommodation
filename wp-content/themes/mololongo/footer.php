<?php

/**

 * The template for displaying the footer

 *

 * Contains the closing of the #content div and all content after

 *

 * @package MoloLongo

 */



// Exit if accessed directly.

defined( 'ABSPATH' ) || exit;



$container = get_theme_mod( 'mololongo_container_type' );

?>

</main>

<footer class="pb-0">
    <div class="container">
        <div class="row py-3">
            <div class="col-md-2">
                <div class="inner-widget mb-md-5 mb-3 text-center">
                    <div class="mb-3">
                        <img src="<?php bloginfo('template_directory'); ?>/images/mololongoaccommodation_bijeli.png">
                    </div>
                    <!--<p><?php _e('Molo Longo je full service agencija koja u 
                    portfelju osim prodaje, najma i uređenja 
                    nekretnina, nudi i visokokvalitetnu podršku 
                    upravljanju nekretninama i unaprjeđenju 
                    operativnosti', 'molonew'); ?></p>-->
                    <ul class="social-list_contact social-footer d-inline-flex">
                        <li><a href="https://www.instagram.com/mololongoapartments/" target="_blank"><img width="20" src="<?php bloginfo('template_directory'); ?>/images/instagram.svg" class="img-fluid" alt="<?php the_title();?>"></a></li>
        		        <li><a href="https://www.facebook.com/mololongoapartments" target="_blank"><img width="20" src="<?php bloginfo('template_directory'); ?>/images/facebook.svg" class="img-fluid" alt="<?php the_title();?>"></a></li>
        		        <li><a href="https://www.linkedin.com/company/molo-longo" target="_blank"><img width="20" src="<?php bloginfo('template_directory'); ?>/images/linkedin.svg" class="img-fluid" alt="<?php the_title();?>"></a></li>
                    </ul>
                </div>
                
            </div>
            <div class="col-md-10 mt-md-0 mt-5">
                <div class="row">
                    <div class="col-md-4">
                        <div class="inner-widget">
                            <div class="wp-block-group mb-4">
                                <div class="wp-block-group__inner-container">
                                    <p class="widget-title"><?php _e('Grupacija Molo Longo', 'molonew'); ?></p>
                                        <ul class="menu-list">
                                            <li><a href="https://mololongo.com/" target="_blank"><?php _e('O grupaciji', 'molonew'); ?></a></li>
                                            <li><a href="https://mololongovillas.com/" target="_blank"><?php _e('Molo Longo vile', 'molonew'); ?></a></li>
                                            <li><a href="https://mololongorealestate.com/" target="_blank"><?php _e('Molo Longo nekretnine', 'molonew'); ?></a></li>
                                            <li><a href="https://mololongointeriors.com/" target="_blank"><?php _e('Molo Longo dizajn interijera', 'molonew'); ?></a></li>
                                        </ul>
                                </div>
                            </div>
                            <a href="https://rentl.io/en/sign-in" target="_blank" class="button-primary mt-4"><?php _e('Prijava za partnere iznajmljivače', 'molonew'); ?></a>
                        </div>
                    </div>
                    <div class="col-md-8">
        			    <div class="d-flex flex-row flex-wrap footer-sponsors justify-content-end mb-md-5 mb-1">
        			        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/g20.png">
    
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/g85.png">
    
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/g21.png">
        			    </div>
        			    <div class="mt-md-5 mt-2">
    			        <ul class="credit-cards">
    						<li>
    							<a href="https://www.wspay.info/mastercard-securecode.html" target="_blank" title="Mastercard Identity Check">
    								<img alt="Mastercard Identity Check" border="0" src="https://www.wspay.info/payment-info/mastercard-identity-check.png"> 
    							</a> 
    						</li>
    						<li>
    							<a href="https://www.wspay.info/verified-by-visa.html" target="_blank" title="Visa Secure"> 
    								<img alt="Visa Secure" border="0" src="https://www.wspay.info/payment-info/visa-secure.png"> 
    							</a> 
    						</li>
    						<li>
    							<a href="http://www.diners.com.hr" target="_blank" title="Diners Club International"> 
    								<img alt="Diners Club International" border="0" src="https://www.wspay.info/payment-info/diners_50.gif"> 
    							</a>
    						</li>
    						<li>
    							<a href="http://www.visa.com.hr/" target="_blank" title="Visa"> 
    								<img alt="Visa" border="0" src="https://www.wspay.info/payment-info/Visa50.gif"> 
    							</a>
    						</li>
    						<li>
    							<a href="https://www.mastercard.com/hr/" target="_blank" title="MasterCard"><img alt="MasterCard" border="0" src="https://www.wspay.info/payment-info/MasterCard50.gif"> </a> 							
    						</li>
    						<li>
    							<a href="https://www.mastercard.com/hr/" target="_blank" title="Maestro"> <img alt="Maestro" border="0" src="https://www.wspay.info/payment-info/maestro50.gif"> </a> 
    						</li>
        						
        						<li><a href="http://www.wspay.info" title="WSpay - Web Secure Payment Gateway" target="_blank"><img alt="WSpay - Web Secure Payment Gateway" src="https://www.wspay.info/payment-info/wsPayWebSecureLogo-118x50-transparent.png" border="0"></a>
                            </li>
    					</ul>
    			    </div>
                    </div>
                    <div class="col-md-12 mt-md-5 border-top mt-4 pt-4">
                        <div class="inner-widget">
                            <p class="widget-title"><?php _e('Naši uredi', 'molonew'); ?></p>
                            <div class="d-md-flex d-block justify-content-between">
                                <div class="single-location mb-md-0 mb-3">
                                    <p class="mb-1 pl-4"><?php _e('Rijeka', 'molonew'); ?></p>
                                        <ul class="contact-list">
                                        <li><img class="mr-1" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/pin.svg"><a href="https://goo.gl/maps/GXDDzmzZaCaTA1ap6" target="_blank">Trpimirova 1A</a></li>
                                        <li><img class="mr-1" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/phone.svg">+385 51 452 883</li>
                                        <li><img class="mr-1" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/envelope.svg"><a href="mailto:rijeka@mololongo.com">rijeka@mololongo.com</a></li>
                                        <li><img class="mr-1" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clock.svg">09:00 - 22:00</li>
                                    </ul>
                                </div>
                                <div class="single-location mb-md-0 mb-3">
                                    <p class="mb-1 pl-4"><?php _e('Opatija', 'molonew'); ?></p>
                                        <ul class="contact-list">
                                        <li><img class="mr-1" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/pin.svg"><a href="https://goo.gl/maps/tvFNazMdwy4rnZhv6" target="_blank">Maršala Tita 39</a></li>
                                        <li><img class="mr-1" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/phone.svg">+385 51 452 882</li>
                                        <li><img class="mr-1" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/envelope.svg"><a href="mailto:opatija@mololongo.com">opatija@mololongo.com</a></li>
                                        <li><img class="mr-1" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clock.svg">09:00 - 22:00</li>
                                    </ul>
                                </div>
                                <div class="single-location">
                                    <p class="mb-1 pl-4"><?php _e('Crikvenica', 'molonew'); ?></p>
                                        <ul class="contact-list">
                                            <li><img class="mr-1" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/pin.svg"><a href="https://goo.gl/maps/R74ikQFz3ZZD6GeG6" target="_blank">Kralja Tomislava 109</a> </li>
                                            <li><img class="mr-1" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/phone.svg">+385 51 452 884</li>
                                            <li><img class="mr-1" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/envelope.svg"><a href="mailto:crikvenica@mololongo.com">crikvenica@mololongo.com</a></li>
                                           <li><img class="mr-1" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clock.svg">09:00 - 22:00</li>
                                    </ul>
                                </div>
                                <div class="single-location">
                                    <p class="mb-1 pl-4"><?php _e('Poreč', 'molonew'); ?></p>
                                        <ul class="contact-list">
                                            <li><img class="mr-1" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/pin.svg"><a href="https://goo.gl/maps/P5JePcWGF8j893yRA" target="_blank">Bernarda Parentina 19</a> </li>
                                            <li><img class="mr-1" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/phone.svg">/</li>
                                            <li><img class="mr-1" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/envelope.svg"><a href="mailto:porec@mololongo.com">porec@mololongo.com</a></li>
                                           <li><img class="mr-1" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/clock.svg">09:00 - 22:00</li>
                                    </ul>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
    		<div class="row align-items-end">
    			<div class="col-md-7">
    				<div class="site-info">
    					<p>Virtus upravljanje d.o.o., Trpimirova 1a, 51000 Rijeka, OIB: 46096970345</p>
    					<p>© 2023 Molo Longo Accommodation. <?php _e('Sva prava pridržana.', 'molonew'); ?></p>
    
    				</div><!-- .site-info -->
    			</div><!--col end -->
    			<div class="col-md-5">
    				<div class="footer-bottom-content">
                        <ul class="bottom-menu">

                            <li><a href="/<?php _e('opci-uvjeti/', 'molonew'); ?>"><?php _e('Opći uvjeti poslovanja', 'molonew'); ?></a></li>
    
                            <li><a href="/<?php _e('pravila-privatnosti/', 'molonew'); ?>"><?php _e('Pravila privatnosti', 'molonew'); ?></a></li>
    
                            <li><a href="/<?php _e('metode-placanja/', 'molonew'); ?>"><?php _e('Metode plaćanja', 'molonew'); ?></a></li>
    
                        </ul>
    				</div>
    			</div>
    		</div>
    	</div>
	</div>
<!--	<div class="footer-bottom dark-color">
	    <div class="container">
    		<div class="row align-items-end">
    			<div class="col-md-6">
    			    <div class="d-flex flex-row flex-wrap footer-sponsors justify-content-start">
    			        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/g20.png">

                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/g85.png">

                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/g21.png">
    			    </div>
    			</div>
    			<div class="col-md-6">
    			    <div class="d-flex justify-content-end">
    			        <ul class="credit-cards">
    						<li>
    							<a href="https://www.wspay.info/mastercard-securecode.html" target="_blank" title="Mastercard Identity Check">
    								<img alt="Mastercard Identity Check" border="0" src="https://www.wspay.info/payment-info/mastercard-identity-check.png"> 
    							</a> 
    						</li>
    						<li>
    							<a href="https://www.wspay.info/verified-by-visa.html" target="_blank" title="Visa Secure"> 
    								<img alt="Visa Secure" border="0" src="https://www.wspay.info/payment-info/visa-secure.png"> 
    							</a> 
    						</li>
    						<li>
    							<a href="http://www.diners.com.hr" target="_blank" title="Diners Club International"> 
    								<img alt="Diners Club International" border="0" src="https://www.wspay.info/payment-info/diners_50.gif"> 
    							</a>
    						</li>
    						<li>
    							<a href="http://www.visa.com.hr/" target="_blank" title="Visa"> 
    								<img alt="Visa" border="0" src="https://www.wspay.info/payment-info/Visa50.gif"> 
    							</a>
    						</li>
    						<li>
    							<a href="https://www.mastercard.com/hr/" target="_blank" title="MasterCard"><img alt="MasterCard" border="0" src="https://www.wspay.info/payment-info/MasterCard50.gif"> </a> 							
    						</li>
    						<li>
    							<a href="https://www.mastercard.com/hr/" target="_blank" title="Maestro"> <img alt="Maestro" border="0" src="https://www.wspay.info/payment-info/maestro50.gif"> </a> 
    						</li>
        						
        						<li><a href="http://www.wspay.info" title="WSpay - Web Secure Payment Gateway" target="_blank"><img alt="WSpay - Web Secure Payment Gateway" src="https://www.wspay.info/payment-info/wsPayWebSecureLogo-118x50-transparent.png" border="0"></a>
                            </li>
    					</ul>
    			    </div>
    			</div>
    		</div>
    	</div>
    </div>-->
</footer>


    <script>

        const swiper1 = new Swiper('.swiper-featured', {

            loop: true,

            slidesPerView: 1,

              breakpoints: {

                // when window width is >= 320px

                480: {

                  slidesPerView: 1,

                  spaceBetween: 20

                },

                768: {

                  slidesPerView: 3,

                  spaceBetween: 40

                }

              },
              autoplay: {
                   delay: 3000,
                   pauseOnMouseEnter: true,
                   disableOnInteraction	:false
                 },
            // Navigation arrows

            navigation: {

            nextEl: '.swiper-button-next.pos-tr',

            prevEl: '.swiper-button-prev.pos-tr',

            },

        });

            

        const swiper2 = new Swiper('.swiper-list', {

            loop: true,

            slidesPerView: 1,

              breakpoints: {

                // when window width is >= 320px

                480: {

                  slidesPerView: 1,

                  spaceBetween: 20

                },

                768: {

                  slidesPerView: 3,

                  spaceBetween: 40

                }

              },

            // Navigation arrows

            navigation: {

            nextEl: '.swiper-button-next.pos-tr',

            prevEl: '.swiper-button-prev.pos-tr',

            },

        });

        

        const swiper3 = new Swiper('.swiper_app-list', {

            loop: true,

            // Navigation arrows

            navigation: {

            nextEl: '.swiper-button-next',

            prevEl: '.swiper-button-prev',

            },

        });

       // jQuery(document).ready(function($){

       //     $(window).scroll(function(){

       //         if ($(this).scrollTop() > 50) {

       //            $('.header-transparent').addClass('sticky');

       //         } else {

       //            $('.header-transparent').removeClass('sticky');

       //         }

       //     });

        // });

    </script>

    <script>

    jQuery(document).ready(function($) {

        $(function(){

            var nav = $('.auto-height'),

                animateTime = 500,

                navLink = $('.more-button');

                navLink.click(function(){

                    if(nav.height() === 330){

                        autoHeightAnimate(nav, animateTime);

                    } else {

                        nav.stop().animate({ height: '330' }, animateTime);

                    }

              });

            })

            

            /* Function to animate height: auto */

            function autoHeightAnimate(element, time){

              	var curHeight = element.height(), // Get Default Height

                    autoHeight = element.css('height', 'auto').height(); // Get Auto Height

                        element.height(curHeight); // Reset to Default Height

                        element.stop().animate({ height: autoHeight }, time); // Animate to Auto Height

            }

            $(function() {                       //run when the DOM is ready

              $(".more-button").click(function() {  //use a class, since your ID gets mangled

                $(this).addClass("hidden");      //add the class to the clicked element

              });

            });

            const myCustomSlider = document.querySelectorAll('.swiper-id');

            for( i=0; i< myCustomSlider.length; i++ ) {

              

              myCustomSlider[i].classList.add('swiper-id-' + i);

            

              var slider = new Swiper('.swiper-id-' + i, {

                loop: true,
                pagination: {
                    el: '.swiper-pagination',
                    type: 'bullets',
                    clickable: true
                  },
                navigation: {

                      nextEl: ".swiper-button-next",

                      prevEl: ".swiper-button-prev",

                    }

              });

            

            }

                });



    </script>



<?php if ( is_home() || is_front_page()):?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>
<!--
<script>

var textWrapper1 = document.querySelector('.letters-1');

var textWrapper2 = document.querySelector('.letters-2');

var textWrapper3 = document.querySelector('.letters-3');

textWrapper1.innerHTML = textWrapper1.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

textWrapper2.innerHTML = textWrapper2.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

textWrapper3.innerHTML = textWrapper3.textContent.replace(/\S/g, "<span class='letter'>$&</span>");







var ml4 = {};

ml4.opacityIn = [0,1];

ml4.scaleIn = [0.2, 1];

ml4.scaleOut = 1;

ml4.durationIn = 800;

ml4.durationOut = 600;

ml4.delay = 500;



anime.timeline({loop: true})

  .add({

    targets: '.ml4 .letters-1',

    opacity: ml4.opacityIn,

    scale: ml4.scaleIn,

    duration: ml4.durationIn,

  })

  .add({

    targets: '.letters-1 .letter',

    scale: [4,1],

    opacity: [0,1],

    translateZ: 0,

    easing: "easeOutExpo",

    duration: 950,

    delay: (el, i) => 70*i

  }).add({

    targets: '.ml4 .letters-1',

    opacity: 0,

    scale: ml4.scaleOut,

    duration: ml4.durationOut,

    easing: "easeInExpo",

    delay: ml4.delay

  }).add({

    targets: '.ml4 .letters-2',

    opacity: ml4.opacityIn,

    scale: ml4.scaleIn,

    duration: ml4.durationIn

  }).add({

    targets: '.letters-2 .letter',

    scale: [4,1],

    opacity: [0,1],

    translateZ: 0,

    easing: "easeOutExpo",

    duration: 950,

    delay: (el, i) => 70*i

  }).add({

    targets: '.ml4 .letters-2',

    opacity: 0,

    scale: ml4.scaleOut,

    duration: ml4.durationOut,

    easing: "easeInExpo",

    delay: ml4.delay

  }).add({

    targets: '.ml4 .letters-3',

    opacity: ml4.opacityIn,

    scale: ml4.scaleIn,

    duration: ml4.durationIn

  })

  .add({

    targets: '.letters-3 .letter',

    scale: [4,1],

    opacity: [0,1],

    translateZ: 0,

    easing: "easeOutExpo",

    duration: 950,

    delay: (el, i) => 70*i

  }).add({

    targets: '.ml4 .letters-3',

    opacity: 0,

    scale: ml4.scaleOut,

    duration: ml4.durationOut,

    easing: "easeInExpo",

    delay: ml4.delay

  }).add({

    targets: '.ml4',

    opacity: 0,

    duration: 500,

    delay: 500

  });

  </script>-->
  <?php endif;?>

<?php wp_footer(); ?>



</body>



</html>



