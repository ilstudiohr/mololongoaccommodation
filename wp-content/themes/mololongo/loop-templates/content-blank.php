<?php
/**
 * Blank content partial template
 *
 * @package MoloLongo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

the_content();
