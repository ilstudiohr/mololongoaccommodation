<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpQWVRYhOxoLfk3IAsZjdt3107Iqpus-I&libraries=&v=weekly"></script>

<script>

  jQuery(function($){



       // Call global the function

       $('.t-datepicker').tDatePicker({

          iconDate: '',

          formatDate: 'yyyy-mm-dd',

          toDayShowTitle: false,

          titleDateRanges: '',

          dateCheckIn: '<?php echo $_GET['t-start'] ?>',

          dateCheckOut: '<?php echo $_GET['t-end'] ?>',



          <?php 

          if(get_locale() == 'hr') {

               echo "titleMonths: ['Siječanj','Veljača','Ožujak','Travanj','Svibanj', 'Lipanj','Srpanj','Kolovoz','Rujan','Listopad','Studeni','Prosinac'],";

           } elseif (get_locale() == 'de') {

               echo "titleMonths: ['Januar','Februar','März','April','Mai', 'Juni','Juli','August','September','Oktober','November','Dezember'],";

           } elseif (get_locale() == 'it') {

               echo "titleMonths: ['Gennaio','Febbraio','Marzo','Aprile','Maggio', 'Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre'],";

           }

           

           ?>

      }).on('onChangeCO',function(e, changeDateCO) {

        setTimeout(function() {

          var date = $('.t-input-check-in').val();
          var dateShort = date.slice(-2);
          var mounthShort = date.slice(-5, -3);
          var fullDate = dateShort+"."+mounthShort;
          $('#mobileSearchCheckIn').html(fullDate);

          var date = $('.t-input-check-out').val();
          var dateShort = date.slice(-2);
          var mounthShort = date.slice(-5, -3);
          var fullDate = dateShort+"."+mounthShort;
          $('#mobileSearchCheckOut').html(fullDate);

        }, 500);

      });

       // }).on('onChangeCO',function(e, changeDateCO) {

       //   setTimeout(function() {

       //     $('#ajax_form').submit();

       //   }, 500);

       // });

      var now = new Date();

      var day = ("0" + now.getDate()).slice(-2);

      var month = ("0" + (now.getMonth() + 1)).slice(-2);

      var today = now.getFullYear()+"-"+(month)+"-"+(day);

      var tomorrow = new Date();



      tomorrow.setDate(now.getDate() + 1);

      var dayTomorrow = ("0" + tomorrow.getDate()).slice(-2);

      var monthTomorrow = ("0" + (tomorrow.getMonth() + 1)).slice(-2);

      var tomorrowFull = tomorrow.getFullYear()+"-"+(monthTomorrow)+"-"+(dayTomorrow);



      $('.t-datepicker--cities').tDatePicker({

        iconDate: '',

        toDayShowTitle: false,

        titleDateRanges: '',

        dateCheckIn  : today,

        dateCheckOut  : tomorrowFull,

        <?php 

        if(get_locale() == 'hr') {

            echo "titleMonths: ['Siječanj','Veljača','Ožujak','Travanj','Svibanj', 'Lipanj','Srpanj','Kolovoz','Rujan','Listopad','Studeni','Prosinac'],";

        } elseif (get_locale() == 'de') {

            echo "titleMonths: ['Januar','Februar','März','April','Mai', 'Juni','Juli','August','September','Oktober','November','Dezember'],";

        } elseif (get_locale() == 'it') {

            echo "titleMonths: ['Gennaio','Febbraio','Marzo','Aprile','Maggio', 'Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre'],";

        }        

        ?> 

    });

      /* Mobile date info */
      var date = $('.t-input-check-in').val();
      var dateShort = date.slice(-2);
      var mounthShort = date.slice(-5, -3);
      var fullDate = dateShort+"."+mounthShort;
      $('#mobileSearchCheckIn').html(fullDate);

      var date = $('.t-input-check-out').val();
      var dateShort = date.slice(-2);
      var mounthShort = date.slice(-5, -3);
      var fullDate = dateShort+"."+mounthShort;
      $('#mobileSearchCheckOut').html(fullDate);




    var accommodations = [];

    var markers = [];

    var gmarkers = [];

    var bounds = new google.maps.LatLngBounds();

    var map;

    var prev_infowindow;



    $('#ajax_form').submit(function( event ) {
      event.preventDefault();
      getResults(getParameterByName('page', window.location.href) ?? 1);
    });



  $(document).on('click', '.t-datepicker-day', function () {
    $('#ajax_form').submit();
  })



  if ($('#advSearchLocation').length) {

    $('#advSearchLocation').change(function() {

      $('#searchResults').val($(this).val());

    });

  }



  $('#cities').on('change', function () {

    var optionSelected = $("option:selected", this);

    var valueSelected = this.value;

    $("input[name='mphb_ra_grad']").val(valueSelected);

  });



  // $('#ajax_form select, #ajax_form input,#ajax_form select').on('change', function () {

  //   $('#ajax_form').submit();

  // });   



  $('#ajax_form .auto-submit').on('change', function () {

    $('#ajax_form').submit();

  }); 



  function getParameterByName(name, url) {

      if (!url) url = window.location.href;

      name = name.replace(/[\[\]]/g, '\\$&');

      var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),

      results = regex.exec(url);

      if (!results) return null;

      if (!results[2]) return '';

      return decodeURIComponent(results[2].replace(/\+/g, ' '));

  }



  $('body').on('click', '.pagination a', function(event) {
      event.preventDefault();
      getResults(getParameterByName('page', $(this).attr('href')));
      // $("html, body").animate({ scrollTop: 0 }, "slow");
  }); 

  function getResults(page = 1) {

    if(typeof(map) === 'undefined') { 
      initGoogleMap();
      getResults(page);
      return;
    };
    var data = $('#ajax_form').serializeArray();
    data[data.length]  = {
      'name': 'page', 
      'value': page
    };
    if($("input[name^='move_on_map']:checked:enabled").length) {
      var mapBounds = map.getBounds();
      data[data.length] = {
        'name': 'filter_by_map',
        'value': true,
      };
      data[data.length] = {
        'name': 'map_bounds_east',
        'value': mapBounds.toJSON().east,
      };
      data[data.length] = {
        'name': 'map_bounds_north',
        'value': mapBounds.toJSON().north,
      };
      data[data.length] = {
        'name': 'map_bounds_south',
        'value': mapBounds.toJSON().south,
      };
      data[data.length] = {
        'name': 'map_bounds_west',
        'value': mapBounds.toJSON().west,
      };
    }

    var currentRequest = $.ajax({
      url: '<?php echo get_rest_url( null, 'mphb_orioly/v1/accommodations' ) ?>',
      method: 'GET', 
      data: data,
      beforeSend: function() {
        window.history.pushState("", "", '<?php echo home_url( $wp->request ); ?>/?'+$('#ajax_form').find('input[name!=mphb_ra_grad]').serialize()+'&page='+page);
        markers = [];
        bounds = new google.maps.LatLngBounds();
        if(typeof currentRequest !== "undefined" && currentRequest != null) {
          currentRequest.abort();
        }
        $('#resourcePaginaton').html('');
        $('#resourceList').html('<div class="loading"><?php _e('Loading', 'molonew'); ?></div>');
      },
      success: function(data) {
       hideMarkers();
        let accommodationsHTML = '';
        accommodations = data.data;

        $.each(accommodations, function( index, object ) {
          accommodationsHTML += getAccommodationHTML(object);          
          var rentlioData= '';
          var rentlioDataTotalPrice = '';
          var totalPriceElement = ''; 
          if(object.rentlio_data && isNaN(object.rentlio_data))
          {
            rentlioDataTotalPrice = null;
            rentlioWabRate = null;
            rentlioData= object.rentlio_data.rates[0].DailyValues[0].price;

            object.rentlio_data.rates.forEach(function(d) {
               if(d.name === 'WEB rate' || d.name === 'web rate' || d.name === 'Web rate') {
                 rentlioWabRate = d;
               }
             })
            
            rentlioWabRate.DailyValues.forEach(function(d) {
              rentlioDataTotalPrice = rentlioDataTotalPrice + d.price;
            });
            totalPriceElement = `<span class="bold">Total `+rentlioDataTotalPrice+` EUR </span>`;
          } else if(object.total_price != 0 ) {
            rentlioDataTotalPrice = object.total_price;
            totalPriceElement = `<span class="bold">Total `+rentlioDataTotalPrice+` EUR </span>`;
          }
          
          if(object.basic_info_latitude && object.basic_info_longitude) {
            markers.push({
              id: object.id,
              coords:{lat:parseFloat(object.basic_info_latitude), lng:parseFloat(object.basic_info_longitude)},
              iconImage:'<?php bloginfo("template_directory");?>/images/pin-unselected.svg',
              content: `<a href="`+object.url+`">
              <img width="260" height="160" src="`+object.thumbnail+`" alt="">
              <p class="dd_map_title dd_map_title--or">`+object.title+`</p>
              <p class="dd_map_title dd_map_title--subtitle-or">`+object.basic_info_address+`</p>
              <p class="dd_map_title dd_map_title--subtitle-or">`+totalPriceElement+`</p>
              `
            })
            bounds.extend({lat:parseFloat(object.basic_info_latitude), lng:parseFloat(object.basic_info_longitude)});
          }
          if($("input[name^='move_on_map']:checked:enabled").length === 0) {
            map.fitBounds(bounds);
          }
        });
        if(data.total_results == 0) {
          $('#resourceList').html('<div class="loading"><?php _e('No results', 'molonew'); ?></div>');
        } else {
          $('#resourceList').html(accommodationsHTML);
        }
        $('#resourcePaginaton').html(generatePagination(page, data.total_results));
        $('.resource_count').html('<?php _e('Nađeni smještaji', 'molonew'); ?> ('+ data.total_results + ')');
        for(var i = 0; i < markers.length; i++){
          gmarkers.push(addMarker(markers[i]));
        }
      },
      error: function(xhr, status, error) {
        $('#resourceList').html('');
        $('#resourcePaginaton').html('');
        $('.resource_count').html('0 <?php _e('smještaja pronadjeno', 'molonew'); ?>');
      }
    });

  } 



function updateURLParameter(url, param, paramVal){

    var newAdditionalURL = "";

    var tempArray = url.split("?");

    var baseURL = tempArray[0];

    var additionalURL = tempArray[1];

    var temp = "";

    if (additionalURL) {

        tempArray = additionalURL.split("&");

        for (var i=0; i<tempArray.length; i++){

            if(tempArray[i].split('=')[0] != param){

                newAdditionalURL += temp + tempArray[i];

                temp = "&";

            }

        }

    }

    var rows_txt = temp + "" + param + "=" + paramVal;

    return baseURL + "?" + newAdditionalURL + rows_txt;

}



function generatePagination(currentPage, total_results, per_page = 50) {

  currentPage = parseInt(currentPage);

  var maxPage = parseInt(total_results / per_page);

  if(total_results % per_page) maxPage++;

  if(currentPage > maxPage && currentPage != 1) getResults(1);

  var listArray = [];

  var starPage = currentPage - 2;

  var nPage;

  if(starPage < 1) {

    starPage = 1;

  }



  var disabled = '';

  if(currentPage === 1) disabled = 'disabled';

  listArray.push(`

  <li class="page-item `+disabled+` pagination-prev--or">

    <a class="page-link" href="`+ updateURLParameter(window.location.search, 'page', currentPage-1) +`" tabindex="-1"><img src="<?php bloginfo('template_directory');?>/images/angle_right_w.svg" alt="address"></a>

  </li>

  `);



  for (let i = 1; 5 >= listArray.length && (nPage = starPage + i - 1) <= maxPage; i++) {

      var active = '';

      if(currentPage === nPage) active = 'active';

      listArray[i] = `

    <li class="page-item"><a class="page-link `+active+`" href="` + updateURLParameter(window.location.search, 'page', (nPage)) + `">` + nPage + `</a></li>

      `;

  }



  if(maxPage - currentPage > 3) {

    listArray.push(`

    <li class="page-item disabled"><a class="page-link" href="#">...</a></li>

    `);

  }



  if(maxPage - currentPage > 2 && maxPage > 4) {

  listArray.push(`

  <li class="page-item"><a class="page-link `+active+`" href="` + updateURLParameter(window.location.search, 'page', (maxPage)) + `">` + maxPage + `</a></li>

  `);

  }



  disabled = '';

  if(currentPage === maxPage) disabled = 'disabled';

  listArray.push(`

  <li class="page-item `+disabled+` pagination-next--or">

    <a class="page-link" href="` + updateURLParameter(window.location.search, 'page', (currentPage+1)) + `"><img src="<?php bloginfo('template_directory');?>/images/angle_right_w.svg" alt="address"></a>

  </li>

  `);



  var listHTML = '';
  listArray.forEach((element) => {
    listHTML += element;
  });

  if(total_results === 0) {
    listHTML = '';
  }
  return `<ul class="pagination pagination--or justify-content-center">`+listHTML+`</ul>`;
}



  $(document).ready(function() { 
    // AUTO LOAD RESULT
    $('#ajax_form').submit();
  });



  function initGoogleMap() {
    var options = {
      zoom:11,
      maxZoom: 20,
      center:{lat:45.08516653719224, lng:14.591997398391587},
      mapTypeId: "roadmap",
      disableDefaultUI: true,
      zoomControl: true,
      scrollwheel: true
    }
    map = new google.maps.Map(document.getElementById('search-map'),options);
    var mapUpdater;
    map.addListener("bounds_changed", () => {
      if($("input[name^='move_on_map']:checked:enabled").length) {
        clearTimeout(mapUpdater);
        mapUpdater = setTimeout(function(){
          filterByMap();
        }, 1000); //run this after 1 seconds
      }
    });
  }

  $("input[name^='move_on_map']").on('change', function () {
    filterByMap();
  })

  function addMarker(props) {

    var marker = new google.maps.Marker({

      position:props.coords,

      map:map,

    });



    if(props.iconImage){

      marker.setIcon(props.iconImage);

    }



    //Check content

    if(props.content){

      var infoWindow = new google.maps.InfoWindow({

        content:props.content

      });



      marker.addListener("click", () => {

        var focusAccommodation = $("ul").find(`[data-accommodation='${props.id}']`);

        // focusAccommodation.scrollIntoView();

        if(focusAccommodation.length) {

          boxShadow = document.getElementsByClassName("box-shadow");



          if ( boxShadow.length ) { // Check for class

            boxShadow[0].classList.remove("box-shadow");

          }

          // focusAccommodation[0].scrollIntoView();

          focusAccommodation[0].classList.add("box-shadow");

        }



        if( prev_infowindow ) {

          prev_infowindow.close();

        }



        prev_infowindow = infoWindow;

        infoWindow.open({

          anchor: marker,

          map,

          shouldFocus: false,

        });

      });

    }

    return marker;

  }  



  function filterByMap() {
    getResults();

    // if($("input[name^='move_on_map']:checked:enabled").length) {

    //   mapBounds = map.getBounds();

    //   $.each(accommodations, function( index, object ) {

    //     if(mapBounds.contains(new google.maps.LatLng(object.basic_info_latitude, object.basic_info_longitude))) {

    //       $('*[data-accommodation="'+object.id+'"]').show();

    //     } else {

    //       $('*[data-accommodation="'+object.id+'"]').hide();

    //     }

    //   });

    // } else {

    //   $.each(accommodations, function( index, object ) {

    //     $('*[data-accommodation="'+object.id+'"]').show();

    //   });
    // }

  }



  // Sets the map on all markers in the array.

  function setMapOnAll(map) {

    for (let i = 0; i < gmarkers.length; i++) {

      gmarkers[i].setMap(map);

      bounds.extend(gmarkers[i].position);

    }

  }



  // Removes the markers from the map, but keeps them in the array.

  function hideMarkers() {

    setMapOnAll(null);

  }



  function getAccommodationHTML(accommodation) {

    var spareCapacity = '';

    if(accommodation.basic_info_spare_capacity)

    {

      spareCapacity = ` + `;

    }



    var spareBeds = '';

    if(accommodation.basic_info_spare_beds)

    {

      spareBeds = ` + `;

    }



    var rentlioData= '';

    var rentlioDataTotalPrice = '';
    var rentlioWabRate = null;

    var totalPriceElement = '';



    if(accommodation.rentlio_data && isNaN(accommodation.rentlio_data))

    {

      rentlioDataTotalPrice = null;


      rentlioData= accommodation.rentlio_data.rates[0].DailyValues[0].price;

      accommodation.rentlio_data.rates.forEach(function(d) {
        if(d.name === 'WEB rate' || d.name === 'web rate' || d.name === 'Web rate') {
          rentlioWabRate = d;
        }
      })

      rentlioWabRate.DailyValues.forEach(function(d) {

        rentlioDataTotalPrice = rentlioDataTotalPrice + d.price;

      });

      totalPriceElement = `<p class="product-price-or">`+rentlioDataTotalPrice+` € total</p>`;

    } else if(accommodation.total_price != 0) {

      rentlioDataTotalPrice = accommodation.total_price;

      totalPriceElement = `<p class="product-price-or"> from `+rentlioDataTotalPrice+` € night</p>`;

    }

    return `

    <li class="app-list_single" data-accommodation="`+accommodation.id+`">

     <a href="`+accommodation.url+`" class="link-float"></a>

    <div class="app-image app-image--or">

    <div class="accommodation-img--or">

    <!-- `+totalPriceElement+` -->

    <img src="`+accommodation.thumbnail+`" alt="" />

    </div>

    </div>

    <div class="app-content accommodation-item">

    <h3 class="app-title h6">`+accommodation.title+`</h3>

    <div class="app-info app-info--or">`+accommodation.short_content+`</div>

    <ul class="app-icons app-icons--or">

    <li><img src="<?php bloginfo("template_directory");?>/images/pin-color.svg" alt="Lokacija"> `+accommodation.basic_info_city+`</li>

    <li><img src="<?php bloginfo("template_directory");?>/images/ico_guests-color.svg" alt="Kapacitet"> `+accommodation.basic_info_basic_capacity+` `+spareCapacity+` `+accommodation.basic_info_spare_capacity+`</li>

    <li><img src="<?php bloginfo("template_directory");?>/images/ico_bed-color.svg" alt="Broj kreveta"> `+accommodation.basic_info_beds+` `+spareBeds+` `+accommodation.basic_info_spare_beds+` </li>

    <li><img src="<?php bloginfo("template_directory");?>/images/bath.svg" alt="Broj kupaona"> `+accommodation.basic_info_bathrooms+` </li>

    </ul>

    <div class="app-single-price">
        `+totalPriceElement+`
    </div>

    <div class="app-button app-button--or">

    <a href="`+accommodation.url+`" class="button-primary"><?php _e('Rezerviraj', 'molonew'); ?></a>

    </div>

    </div>

    </li>

    `;

  }



});

</script>

<script>

  jQuery(function($){

    $('.app-container, .appartments-map').css({ height: $(window).innerHeight() });

    $(window).resize(function(){

      $('.app-container, .appartments-map').css({ height: $(window).innerHeight() });

    });



    $(".datepick--or").click(function(){

      $(".datepick-popup").addClass("datepick-popup--or");

    });



    $('.modal-toggle').on('click', function(e) {

      e.preventDefault();

      $('.modal--or').toggleClass('is-visible');

      $('.modal-overlay').toggleClass('is-visible');

      $('.map-button--or').toggleClass('map-button--or--hide');

      $('.header-transparent').toggleClass('header-transparent--hide');

    });



    $('.modal-toggle--off').on('click', function(e) {

      $('.modal--or').toggleClass('is-visible');

      $('.modal-overlay').toggleClass('is-visible');

      $('.map-button--or').toggleClass('map-button--or--hide');

      $('.header-transparent').toggleClass('header-transparent--hide');

    });



    $('#mphb_kapacitet-mphb-search-form--locations').each(function(){

      var $this = $(this), numberOfOptions = $(this).children('option').length;



      $this.addClass('select-hidden'); 

      $this.wrap('<div class="select"></div>');

      $this.after('<div class="select-styled"></div>');



      var $styledSelect = $this.next('div.select-styled');

      $styledSelect.text($this.children('option').eq(0).text());



      var $list = $('<ul />', {

        'class': 'select-options'

      }).insertAfter($styledSelect);

      

      for (var i = 0; i < numberOfOptions; i++) {

        $('<li />', {

          text: $this.children('option').eq(i).text(),

          rel: $this.children('option').eq(i).val()

        }).appendTo($list);

      }



      var $listItems = $list.children('li');

      $styledSelect.click(function(e) {

        e.stopPropagation();

        $('div.select-styled.active').not(this).each(function(){

          $(this).removeClass('active').next('ul.select-options').hide();

        });

        $(this).toggleClass('active').next('ul.select-options').toggle();

      });



      $listItems.click(function(e) {

        e.stopPropagation();

        $styledSelect.text($(this).text()).removeClass('active');

        $this.val($(this).attr('rel'));

        $this.trigger('change');

        $list.hide();

      });



      $(document).click(function() {

        $styledSelect.removeClass('active');

        $list.hide();

      });

    });



    var $locationOption = $("#mphb_kapacitet-mphb-search-form--locations").clone();

    $('.select-styled').text($locationOption.find(':selected').text());



    /*People*/

    $('#mphb_kapacitet-mphb-search-form').each(function(){

      var $this = $(this), numberOfOptions = $(this).children('option').length;



      $this.addClass('select-hidden'); 

      $this.wrap('<div class="select--people"></div>');

      $this.after('<div class="select-styled--people"></div>');



      var $styledSelect = $this.next('div.select-styled--people');

      $styledSelect.text($this.children('option').eq(0).text());

 

      var $list = $('<ul />', {

        'class': 'select-options'

      }).insertAfter($styledSelect);    



      for (var i = 0; i < numberOfOptions; i++) {

        $('<li />', {

          text: $this.children('option').eq(i).text(),

          rel: $this.children('option').eq(i).val()

        }).appendTo($list);

      }



      var $listItems = $list.children('li');    



      $styledSelect.click(function(e) {

        e.stopPropagation();

        $('div.select-styled--people.active').not(this).each(function(){

          $(this).removeClass('active').next('ul.select-options').hide();

        });

        $(this).toggleClass('active').next('ul.select-options').toggle();

      });



      $listItems.click(function(e) {

        e.stopPropagation();

        $styledSelect.text($(this).text()).removeClass('active');

        $this.val($(this).attr('rel'));

        $this.trigger('change');

        $list.hide();

      });



      $(document).click(function() {

        $styledSelect.removeClass('active');

        $list.hide();

      });

    });



    var $locationOption = $("#mphb_kapacitet-mphb-search-form").clone();

    $('.select-styled--people').text($locationOption.find(':selected').text());

  });



$('.modal-toggle-range').on('click', function(e) {

  e.preventDefault();

  $('.dropdown-menu--or').toggleClass('show');

});



$(document).on('click', function (e) {

    if ($(e.target).closest(".dropdown-menu--or").length === 0 && $(e.target).closest(".modal-toggle-range").length === 0 ) {

      $('.dropdown-menu--or').removeClass('show');

    }

});



$('<div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div>').insertAfter('.quantity input');

$('.quantity').each(function() {

  var spinner = $(this),

  input = spinner.find('input[type="number"]'),

  btnUp = spinner.find('.quantity-up'),

  btnDown = spinner.find('.quantity-down'),

  min = input.attr('min'),

  max = input.attr('max');



  btnUp.click(function() {

    var oldValue = parseFloat(input.val());



    if (oldValue >= max) {

      var newVal = oldValue;

    } else {     

      var step;

      var newVal = oldValue + 1;

      if(step = input.attr('step')){

        if(oldValue >= max) {

          newVal = oldValue;

        }

        newVal = oldValue + parseFloat(step);

      }

    }

    spinner.find("input").val(newVal);

    spinner.find("input").trigger("change");

  });



  btnDown.click(function() {

    var oldValue = parseFloat(input.val());

    if (oldValue <= min) {

      var newVal = oldValue;

    } else {

      var step;

      var newVal = oldValue - 1;

      if(step = input.attr('step')){

        if(oldValue <= min){

          newVal = oldValue;

        }

        newVal = oldValue - parseFloat(step);

      } 

    }

    spinner.find("input").val(newVal);

    spinner.find("input").trigger("change");

  });



  $(document).ready(function () {

      var n1 = parseInt($('#adults').val());

      var n2 = parseInt($('#children').val());

      var r = n1 + n2;

      var n3 = $('#capacity').val(r);

      var n4 = $('#mobileSearchCapacity').html(r);

      return false;

  });



  $('input.quantity__capacity').change(function () {

    var n1 = parseInt($('#adults').val());

    var n2 = parseInt($('#children').val());

    var r = n1 + n2;

    var n3 = $('#capacity').val(r);

    var n4 = $('#mobileSearchCapacity').html(r);
     // $('#mobileSearchCheckIn').html(fullDate);

    return false;

  });


  /* Mobile modal filters */
  $(document).ready(function(){
      $('#showFilters').click(function() {
        $('.modal-overlay--mobile').addClass("is-visible");
        $('.filter-box--or').addClass("filter-box--or--visible");
        $('.mobile-map-list-toggle').hide();
      });

      $('.hide-mobile-filters').click(function(e) {
        $('.modal-overlay--mobile').removeClass("is-visible");
        $('.filter-box--or').removeClass("filter-box--or--visible");
        $('.filter-box--or').removeClass("filter-box--or--max");
        $('.modal--or').removeClass('is-visible');
        $('.advanced-filters--or').removeClass("advanced-filters--or--mobile--hide");
        $('.mobile-map-list-toggle').show();
        e.preventDefault();
      });

      $('.search-res-submit').click(function(e) {
        $('.modal-overlay--mobile').removeClass("is-visible");
        $('.filter-box--or').removeClass("filter-box--or--visible");
        $('.filter-box--or').removeClass("filter-box--or--max");
        $('.modal--or').removeClass('is-visible');
        $('.advanced-filters--or').removeClass("advanced-filters--or--mobile--hide");
        $('.mobile-map-list-toggle').show();
      });

      $('.advanced-filters--or--mobile').click(function(e) {
        $('.filter-box--or').addClass("filter-box--or--max");
        $(this).addClass("advanced-filters--or--mobile--hide");
      });

      $('.modal-close--mobile').click(function(e) {
        $('.filter-box--or').removeClass("filter-box--or--max");
        $('.modal--or').removeClass('is-visible');
        $('.advanced-filters--or').removeClass("advanced-filters--or--mobile--hide");
      });

      $('.mobile-map--toggle').click(function(e) {
        $('.appartments-map').removeClass("appartments-map--hide");
        $('.app-container').hide();
      });

      $('.mobile-list--toggle').click(function(e) {
        $('.appartments-map').addClass("appartments-map--hide");
        $('.app-container').show();
      });
  
  });

});



</script>

<script>

  let sliderOne = document.getElementById("slider-1");

  let sliderTwo = document.getElementById("slider-2");

  let displayValOne = document.getElementById("range1");

  let displayValTwo = document.getElementById("range2");

  let minGap = 0;

  let sliderTrack = document.querySelector(".slider-track");

  let sliderMaxValue = document.getElementById("slider-1").max;

  let sliderMinValue = document.getElementById("slider-1").min;



  function slideOne(){

    if(parseInt(sliderTwo.value) - parseInt(sliderOne.value) <= minGap){

      sliderOne.value = parseInt(sliderTwo.value) - minGap;

    }

    displayValOne.textContent = sliderOne.value;

    fillColor();

  }



  function slideTwo(){

    if(parseInt(sliderTwo.value) - parseInt(sliderOne.value) <= minGap){

      sliderTwo.value = parseInt(sliderOne.value) + minGap;

    }

    displayValTwo.textContent = sliderTwo.value;

    fillColor();

  }



  function fillColor(){

    percent1 = ((sliderOne.value - sliderMinValue) * 100) / (sliderMaxValue - sliderMinValue);

    percent2 =  ((sliderTwo.value - sliderMinValue) * 100) / (sliderMaxValue - sliderMinValue);

    sliderTrack.style.background = `linear-gradient(to right, #dadae5 ${percent1}% , #deaf69 ${percent1}% , #deaf69 ${percent2}%, #dadae5 ${percent2}%)`;

  }



  slideOne();

  slideTwo();

</script>