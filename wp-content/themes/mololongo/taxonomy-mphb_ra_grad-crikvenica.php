<?php
/*
Template Name: Loop Grad Crikvenica
*/
?>

<?php get_header(); ?>	

<section class="map-padding or-search">
  <div class="appartments-grid">
    <div class="app-container f-left">
      <div class="mobile-search-filters--or">
        <div>
          <p class="mobile-search-filters--or--date">
            <span id="mobileSearchCheckIn"></span>-<span id="mobileSearchCheckOut"></span>
          </p>
          <span>&nbsp;|&nbsp;</span>
          <p class="mobile-search-filters--or--capacity">
            <span><?php _e('Broj gostiju', 'molonew'); ?></span>
            <span id="mobileSearchCapacity"></span>
          </p>
        </div>
        <button id="showFilters" class="btn"><img src="<?php bloginfo('template_directory');?>/images/filter.png" width="20" alt="filter"></a><?php _e(' Filteri', 'molonew'); ?></button>
      </div>
      <div class="modal-overlay modal-overlay--mobile"></div>
      <div class="intro filter-box filter-box--or">
        <div class="filter-box--or__mobile-header">
          <h2><?php _e('Filteri', 'molonew'); ?></h2>
          <button class="btn hide-mobile-filters"><img src="<?php bloginfo('template_directory');?>/images/closemolo.svg" alt="close"></button>
        </div>
        <div class="form_box">
<!--             <div class="region_intro">
              <h1>Crikvenica<span></span></h1>
                <p><?php _e('Jedan od najslikovitijih dijelova obale Jadranskoga mora definitivno je ovaj gradić koji će vam svaki puta kada ga posjetite ispričati neku novu priču.', 'molonew'); ?></p>
            </div> -->
          <div class="form_box_inner">
            <div class="mphb_sc_search-wrapper">

              <form method="GET" class="mphb_sc_search-form mphb_sc_search-form--result mphb_search-filter" id="ajax_form">
                <!-- <input type="hidden" name="mphb_ra_grad" value="crikvenica"> -->
                <input type="submit" name="submit" style="display: none;">

<!--                 <p class="mphb_sc_search-submit-button-wrapper">

                  <input id="searchResults" type="search" name="q" placeholder="<?php _e('Lokacija', 'molonew'); ?>" value="<?php echo $_GET['q'] ?>"/>

                </p> -->

                <select id="mphb_kapacitet-mphb-search-form--locations" name="mphb_ra_grad">
                    <option value=""><?php _e('Sve lokacije', 'molonew'); ?></option>
                    <option value="rijeka"><?php _e('Rijeka', 'molonew'); ?></option>
                    <option value="opatija"><?php _e('Opatija', 'molonew'); ?></option>
                    <option value="crikvenica" selected><?php _e('Crikvenica', 'molonew'); ?></option>
                    <option value="krk"><?php _e('Krk', 'molonew'); ?></option>
                  </select> 

                <div class="t-datepicker t-datepicker--search t-datepicker--cities">
                    <p class="mphb_sc_search-check-in-date t-check-in datepick--or"></p>
                    <p class="mphb_sc_search-check-out-date t-check-out datepick--or"></p>
                </div>  
                                                

                <div class="dropdown modal-toggle-range--wrapp modal-guests">
                  <button class="advanced-filters--or modal-toggle-range dropdown-toggle" type="button" id="dropdownMenuButton">
                    <?php _e('Broj gostiju', 'molonew'); ?> <span id="guestsOr"></span> <input type="text" id="Sum" value="" class="p0"/>
                  </button>
                  <div class="dropdown-menu dropdown-menu--or" >
                      <ul class="block detalji_smjestaja accommodation-details--or accommodation-details--or--homepage">
                          <li class="sf-field-post-meta-basic_info_beds">
                             <p><?php _e('Odrasli', 'molonew'); ?></p>
                             <label>
                              <div class="quantity">
                                <input class="quantity__capacity" id="adults" type="number" onchange="reSum();" name="adults" min="1" step="1" max="20" value="<?php echo ($_GET['adults'] ? $_GET['adults'] : '1') ?>">
                              </div>
                            </label>
                          </li>
                          <li class="sf-field-post-meta-basic_info_beds">
                             <p class="modal-guests__info">
                              <span><?php _e('Djeca', 'molonew'); ?></span>
                              <span><?php _e('ispod 18 godina', 'molonew'); ?></span>
                          </p>
                             <label>
                              <div class="quantity">
                                <input class="quantity__capacity" id="children" type="number" onchange="reSum();" name="children" min="0" step="1" max="20" value="<?php echo ($_GET['children'] ? $_GET['children'] : '0') ?>">
                              </div>
                            </label>
                          </li>
                          <li>
                              <input type="hidden" id="capacity" type="number" min="1" max="40" value="<?php echo ($_GET['mphb_attributes']['kapacitet'] ? $_GET['mphb_attributes']['kapacitet'] : '1') ?>" name="mphb_attributes[kapacitet]">
                          </li>
                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">
                            <div class="checkbox--or">
                              <input class="sf-input-checkbox" type="checkbox" value="pet_friendly" <?php if(in_array('pet_friendly', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-4922c9d1395f7191f2e7f935350a656d">
                              <label for="sf-input-4922c9d1395f7191f2e7f935350a656d"><?php _e('Pet friendly', 'molonew'); ?></label>
                            </div>
                          </li>
                      </ul>
                  </div>
                </div>
                <div class="search-res-submit-wrapp">
                  <button class="btn btn-close--or hide-mobile-filters"><?php _e('Zatvori', 'molonew'); ?></button>
                  <button class="search-res-submit auto-submit button_icon"><i class="fa fa-search"></i></button>
                </div>

                <button class="advanced-filters--or advanced-filters--or--mobile modal-toggle filter-absolute"><?php _e('Napredno pretraživanje', 'molonew'); ?></button>
                
                
        </form>
      </div>
    </div>
    <div id="resourceCount" class="resource_count"></div>
  </div>

</div>
<?php include('search-res-templates/adv-search-city.php')?>
<ul class="app-list" id="resourceList">

</ul>
<div class="pagination-wrapp-or" id="resourcePaginaton"></div>
</div>
<!-- <a href="#sidebarmap" class="map-button--or"><?php _e('Karta', 'molonew'); ?> <img src="<?php bloginfo('template_directory');?>/images/map-2.svg" alt="map"></a> -->
<div class="mobile-map-list-toggle">
  <button class="resource_count btn mobile-list--toggle"></button>
  <button class="btn mobile-map--toggle"><img src="<?php bloginfo('template_directory');?>/images/map.png" alt="map"><?php _e('Prikaz karte', 'molonew'); ?></button>
</div>
<div class="appartments-map f-right appartments-map--hide" id="sidebarmap">
  <div class="checkbox--or checkbox--or--search">
    <input type="checkbox" id="moveOnMap" value="1" name="move_on_map">
    <label for="moveOnMap"><?php _e('Pretražuj dok pomičeš kartu', 'molonew'); ?></label>
  </div>
  <div class="map">
    <div id="search-map" class="search-map--or" style="height: 100vh; width: 100%;"></div>
  </div>
</div>
</div>
</section>
<script>
var num1 = parseInt(document.getElementById("adults").value);
var num2 = parseInt(document.getElementById("children").value);
document.getElementById("Sum").value = num1 + num2;
function reSum()
{
    
    var num3 = parseInt(document.getElementById("adults").value);
    var num4 = parseInt(document.getElementById("children").value);
    document.getElementById("Sum").value = num3 + num4;

}
</script>
<?php include('scripts/page-booking-search.php')?>
<?php get_footer(); ?>	