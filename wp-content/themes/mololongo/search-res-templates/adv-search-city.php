                <div class="modal--or">

                  <div class="modal-overlay modal-toggle"></div>

                  <div class="modal-wrapper modal-transition">

                    <div class="modal-header">

                      <h2 class="h4"><?php _e('Napredno pretraživanje', 'molonew'); ?></h2>
                      <!-- <button class="modal-close modal-toggle"><img src="<?php bloginfo('template_directory');?>/images/closemolo.svg" alt="aboutUs"></button> -->
                      <button style="display: none;" class="modal-close--mobile modal-close modal-toggle"><img src="<?php bloginfo('template_directory');?>/images/closemolo.svg" alt="aboutUs"></button>

                    </div>



                    <div class="modal-body">

                      <div class="modal-content">

                        <div class='searchandfilter searchandfilter--or'>



                          <ul class="block detalji_smjestaja accommodation-details--or price-range--or"> 

                            <li>

                              <h3 class="h5"><?php _e('Cijena po danu', 'molonew'); ?></h3> 



                               <div class="values">

                                   <span id="range1">0</span> €
                                   <span> &dash; </span>
                                   <span id="range2">100</span> €

                               </div>

                               <div class="slider-track"></div>

                               <input name="price_min" value="<?php echo $_GET['price_min'] ?? 0 ?>" type="range" min="0" max="500" id="slider-1" oninput="slideOne()">

                               <input name="price_max" value="<?php echo $_GET['price_max'] ?? 500 ?>" type="range" min="0" max="500" id="slider-2" oninput="slideTwo()">   

                            </li>               

                          </ul>

                          <ul class="block detalji_smjestaja accommodation-details--or">       

                            <h3 class="h5"><?php _e('Detalji smještaja', 'molonew'); ?></h3>

<!--                             <?php if (is_page_template( 'page-booking-search.php' )):  ?>
                                <li class="adv-locations--or">

                                  <p class="adv-locations--or__label"><?php _e('Lokacija', 'molonew'); ?>:</p>

                                  <p class="mphb_sc_search-submit-button-wrapper">
                                    <input id="advSearchLocation" type="search" name="adv_search_location" placeholder="<?php _e('Lokacija', 'molonew'); ?>"/>
                                  </p>
                              
                                </li>               

                            <?php else: ?>
                                <li class="adv-locations--or">

                                  <p class="adv-locations--or__label"><?php _e('Lokacija', 'molonew'); ?>:</p>
                                  <div class="adv-locations--or__wrapper__select">
                                    <select name="cities" id="cities" class="adv-locations--or__select">
                                      <option value=""><?php _e('Izaberi grad', 'molonew'); ?></option>
                                      <option value="rijeka">Rijeka</option>
                                      <option value="opatija">Opatija</option>
                                      <option value="crikvenica">Crikvenica</option>
                                      <option value="krk">Krk</option>
                                    </select>
                                  </div>                              
                                </li>               
                            <?php endif ?> -->

                            <!-- <li class="adv-toggle--or">
                               <p class="adv-toggle--or__label"><?php _e('Instant rezervacija', 'molonew'); ?></p>
                               <div class="adv-toggle--or__wrapper">
                                 <div class="button-cover">
                                   <div class="toggle-button--or r" id="button-1">
                                     <input name="instant" type="checkbox" class="checkbox" <?php if($_GET['instant']) echo 'checked'; ?> />
                                     <div class="knobs"></div>
                                     <div class="layer"></div>
                                   </div>
                                 </div>
                               </div>
                            </li>-->

                            <!--<li class="adv-toggle--or">
                               <p class="adv-toggle--or__label"><?php _e('Usluga doručka', 'molonew'); ?></p>
                               <div class="adv-toggle--or__wrapper">
                                 <div class="button-cover">
                                   <div class="toggle-button--or r" id="button-1">
                                     <input type="checkbox" class="checkbox" value="breakfast" name="_sfm_search_amenities[]" <?php if(in_array('breakfast', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> />
                                     <div class="knobs"></div>
                                     <div class="layer"></div>
                                   </div>
                                 </div>
                               </div>
                            </li>-->
                            
                            <li class="sf-field-post-meta-basic_info_rooms" data-sf-field-name="_sfm_basic_info_rooms" 

                            data-sf-field-type="post_meta" data-sf-field-input-type="select" data-sf-meta-type="choice">

                            <p><?php _e('Broj soba', 'molonew'); ?></p>

                            <label>

                              <div class="quantity">

                                <input type="number" name="rooms" min="1" step="1" max="10" value="<?php echo $_GET['rooms'] ?? '1' ?>">

                              </div>

                            </label>

                            </li>

                            <li class="sf-field-post-meta-basic_info_beds" data-sf-field-name="_sfm_basic_info_beds" 

                            data-sf-field-type="post_meta" data-sf-field-input-type="select" data-sf-meta-type="choice">

                            <p><?php _e('Broj kreveta', 'molonew'); ?></p>

                            <label>

                              <div class="quantity">

                                <input type="number" name="beds" min="1" step="1" max="10" value="<?php echo $_GET['beds'] ?? '1' ?>">

                              </div>

                            </label>

                          </li>



                          <li class="sf-field-post-meta-basic_info_bathrooms" data-sf-field-name="_sfm_basic_info_bathrooms" 

                          data-sf-field-type="post_meta" data-sf-field-input-type="select" data-sf-meta-type="choice">

                          <p><?php _e('Broj kupaonica', 'molonew'); ?></p>

                          <label>

                            <div class="quantity">

                              <input type="number" name="bathrooms" min="1" step="0.5" max="10" value="<?php echo $_GET['bathrooms'] ?? '1' ?>">

                            </div>

                          </label>

                        </li>

                      </ul>



                      <ul class="block checkboxes checkboxes--or">

                        <li class="sf-field-post-meta-search_amenities" data-sf-field-name="_sfm_search_amenities" 

                        data-sf-field-type="post_meta" data-sf-field-input-type="checkbox" data-sf-meta-type="choice">

                        <h3 class="h5"><?php _e('Sadržaji', 'molonew'); ?></h3>

                        <ul data-operator="and" class="">

                            
                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                                <div class="checkbox--or">
    
                                  <input class="sf-input-checkbox" type="checkbox" value="kuhinja_kuhinja_cajna_kuhinja" <?php if(in_array('kuhinja_kuhinja_cajna_kuhinja', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-ef49de1d6e039b437d811b93f6957c53">
    
                                  <label for="sf-input-ef49de1d6e039b437d811b93f6957c53"><?php _e('Kuhinja', 'molonew'); ?></label>
    
                                </div>

                          </li>

                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input class="sf-input-checkbox" type="checkbox" value="opcenito_wi_fi" <?php if(in_array('opcenito_wi_fi', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-ef49de1d6e039b437d811b93f6957c52">

                              <label for="sf-input-ef49de1d6e039b437d811b93f6957c52"><?php _e('Wi-Fi', 'molonew'); ?></label>

                            </div>

                          </li>
                          
                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input class="sf-input-checkbox" type="checkbox" value="opcenito_jacuzzi" <?php if(in_array('opcenito_jacuzzi', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-ef49de1d6e039b437d811b93f6957c51">

                              <label for="sf-input-ef49de1d6e039b437d811b93f6957c51"><?php _e('Jacuzzi', 'molonew'); ?></label>

                            </div>

                          </li>



                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">
                              <input  class="sf-input-checkbox" type="checkbox" value="opcenito_lift" <?php if(in_array('opcenito_lift', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-74b9f260ddb7515451bfdc483e3ac407">

                              <label for="sf-input-74b9f260ddb7515451bfdc483e3ac407"><?php _e('Lift', 'molonew'); ?></label>

                            </div>

                          </li>



                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input  class="sf-input-checkbox" type="checkbox" value="dnevni_boravak_netflix" <?php if(in_array('dnevni_boravak_netflix', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-b75650763000fc06f6acb4013a2adeb4">

                              <label for="sf-input-b75650763000fc06f6acb4013a2adeb4"><?php _e('Netflix', 'molonew'); ?></label>

                            </div>

                          </li>



                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input  class="sf-input-checkbox" type="checkbox" value="parking" <?php if(in_array('parking', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-7597014bf01cf43045ecca07c499af97">

                              <label for="sf-input-7597014bf01cf43045ecca07c499af97"><?php _e('Privatni parking', 'molonew'); ?></label>

                            </div>

                          </li>



                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input  class="sf-input-checkbox" type="checkbox" value="opcenito_pool" <?php if(in_array('opcenito_pool', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-dc3d04965c670bd0466db24e543bfec4">

                              <label for="sf-input-dc3d04965c670bd0466db24e543bfec4"><?php _e('Bazen', 'molonew'); ?></label>

                            </div>

                          </li>



                          <!-- <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input  class="sf-input-checkbox" type="checkbox" value="pet_friendly" <?php if(in_array('pet_friendly', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-4922c9d1395f7191f2e7f935350a656d">

                              <label for="sf-input-4922c9d1395f7191f2e7f935350a656d"><?php _e('Pet friendly', 'molonew'); ?></label>

                            </div>

                          </li> -->



                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input  class="sf-input-checkbox" type="checkbox" value="opcenito_sauna" <?php if(in_array('opcenito_sauna', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-5ad24cd8a88d970dc0ad2dec46f410d9">

                              <label for="sf-input-5ad24cd8a88d970dc0ad2dec46f410d9"><?php _e('Sauna', 'molonew'); ?></label>

                            </div>

                          </li>



                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input  class="sf-input-checkbox" type="checkbox" value="opcenito_terasa" <?php if(in_array('opcenito_terasa', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-51d89822c7bd1b3c7fa7563676b6cf3d">

                              <label for="sf-input-51d89822c7bd1b3c7fa7563676b6cf3d"><?php _e('Terasa', 'molonew'); ?></label>

                            </div>

                          </li>



                         <!-- <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input  class="sf-input-checkbox" type="checkbox" value="concierge" <?php if(in_array('concierge', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-706c73ec8340242c8844f0a5893eb39a">

                              <label for="sf-input-706c73ec8340242c8844f0a5893eb39a"><?php _e('Concierge (vratar)', 'molonew'); ?></label>

                            </div>

                          </li>-->



                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input  class="sf-input-checkbox" type="checkbox" value="opcenito_airco" <?php if(in_array('opcenito_airco', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="acf-field_5e2855a9a4fb4-field_5e285659ad054">

                              <label for="acf-field_5e2855a9a4fb4-field_5e285659ad054"><?php _e('Klima', 'molonew'); ?></label>

                            </div>

                          </li>



                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input  class="sf-input-checkbox" type="checkbox" value="tv" <?php if(in_array('tv', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="acf-field_5e2855a9a4fb4-field_5e285657ad053">

                              <label for="acf-field_5e2855a9a4fb4-field_5e285657ad053"><?php _e('Kabelska TV', 'molonew'); ?></label>

                            </div>

                          </li>



                          <!-- <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input  class="sf-input-checkbox" type="checkbox" value="airport" <?php if(in_array('airport', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="acf-field_5e2855a9a4fb4-field_602e998a2a258">

                              <label for="acf-field_5e2855a9a4fb4-field_602e998a2a258"><?php _e('Prijevoz do/od aerodroma', 'molonew'); ?></label>

                            </div>

                          </li> -->

                          <!--<li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input  class="sf-input-checkbox" type="checkbox" value="sidebar_lista_self" <?php if(in_array('sidebar_lista_self', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="acf-field_5e2855a9a4fb4-field_602e998a2a2582">
                              <label for="acf-field_5e2855a9a4fb4-field_602e998a2a2582"><?php _e('Self check-in/out', 'molonew'); ?></label>

                            </div>

                          </li> -->

                        </ul>

                          </li>

                    </ul>
                    
                    <ul class="block checkboxes checkboxes--or">

                        <li class="sf-field-post-meta-search_amenities" data-sf-field-name="_sfm_search_amenities" 

                        data-sf-field-type="post_meta" data-sf-field-input-type="checkbox" data-sf-meta-type="choice">

                        <h3 class="h5"><?php _e('Značajke', 'molonew'); ?></h3>
                        
                        <ul data-operator="and" class="">
                            
                            <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                                <div class="checkbox--or">
    
                                  <input class="sf-input-checkbox" type="checkbox" value="breakfast" <?php if(in_array('breakfast', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-ef49de1d6e039b437d811b93f6957c54">
    
                                  <label for="sf-input-ef49de1d6e039b437d811b93f6957c54"><?php _e('Doručak', 'molonew'); ?></label>
    
                                </div>

                          </li>
                          
                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                                <div class="checkbox--or">
    
                                  <input class="sf-input-checkbox" type="checkbox" value="besplatno_otkazivanje" <?php if(in_array('besplatno_otkazivanje', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-ef49de1d6e039b437d811b93f6957c55">
    
                                  <label for="sf-input-ef49de1d6e039b437d811b93f6957c55"><?php _e('Besplatno otkazivanje', 'molonew'); ?></label>
    
                                </div>

                          </li>
                          
                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                                <div class="checkbox--or">
    
                                  <input class="sf-input-checkbox" type="checkbox" value="_sidebar_lista_self" <?php if(in_array('sidebar_lista_self', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-ef49de1d6e039b437d811b93f6957c58">
    
                                  <label for="sf-input-ef49de1d6e039b437d811b93f6957c58"><?php _e('Self check-in', 'molonew'); ?></label>
    
                                </div>

                          </li>
                          
                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                                <div class="checkbox--or">
    
                                  <input class="sf-input-checkbox" type="checkbox" value="pet_friendly" <?php if(in_array('pet_friendly', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-ef49de1d6e039b437d811b93f6957c56">
    
                                  <label for="sf-input-ef49de1d6e039b437d811b93f6957c56"><?php _e('Pet friendly', 'molonew'); ?></label>
    
                                </div>

                          </li>
                            
                        </ul>
                        
                        </li>

                    </ul>
                    
                    <ul class="block checkboxes checkboxes--or">

                        <li class="sf-field-post-meta-search_amenities" data-sf-field-name="_sfm_search_amenities" 

                        data-sf-field-type="post_meta" data-sf-field-input-type="checkbox" data-sf-meta-type="choice">

                        <h3 class="h5"><?php _e('Vrsta smještaja', 'molonew'); ?></h3>
                        
                        <ul data-operator="and" class="">
                            
                            <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                                <div class="checkbox--or">
    
                                  <input class="sf-input-checkbox" type="checkbox" value="vrsta_smjestaja_cijeli_prostor" <?php if(in_array('vrsta_smjestaja_cijeli_prostor', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-ef49de1d6e039b437d811b93f6957c12">
    
                                  <label for="sf-input-ef49de1d6e039b437d811b93f6957c12"><?php _e('Cijeli prostor', 'molonew'); ?></label>
    
                                </div>

                          </li>
                          
                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                                  <input class="sf-input-checkbox" type="checkbox" value="vrsta_smjestaja_privatni_apartman" <?php if(in_array('vrsta_smjestaja_privatni_apartman', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-ef49de1d6e039b437d811b93f6957c13">

                                  <label for="sf-input-ef49de1d6e039b437d811b93f6957c13"><?php _e('Privatni apartman', 'molonew'); ?></label>

                            </div>

                          </li>

                          
                            
                        </ul>

                      </li>

                    </ul>



                    <ul class="block buttons buttons--or">

                    <!--                       <li class="sf-field-reset" data-sf-field-name="reset" data-sf-field-type="reset" data-sf-field-input-type="button">

                        <input type="submit" class="search-filter-reset" name="_sf_reset" value="Poništi" data-search-form-id="828238" data-sf-submit-form="never">

                    </li> -->

                    <li class="buttons--or__count"><div class="resource_count"></div></li>



                      <li class="sf-field-submit sf-field-submit--or--homepage" data-sf-field-name="submit" data-sf-field-type="submit" data-sf-field-input-type="">

                        <button class="search-filter-submit modal-toggle"><?php _e('Zatvori', 'molonew'); ?></button>
                        <input type="submit" class="search-filter-submit modal-toggle--off search-filter-submit--or" name="_sf_submit" value="<?php _e('Pretraži', 'molonew'); ?>">

                      </li>

                    </ul>

                  </div>

                </div>

              </div>

            </div>

          </div>