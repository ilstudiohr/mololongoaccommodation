<?php
/*
Template Name: Loop Grad
*/
?>

<?php get_header(); ?>	

    <div class="content">
        <div class="content_inner">
        	<h4><?php if(ICL_LANGUAGE_CODE=='hr'): ?>Apartmani<?php elseif(ICL_LANGUAGE_CODE=='en'): ?>Apartments<?php elseif(ICL_LANGUAGE_CODE=='de'): ?>Wohnungen<?php elseif(ICL_LANGUAGE_CODE=='it'): ?>Appartamenti<?php endif; ?></strong> <span class="mphb-price"><?php the_sub_field('price'); ?> <?php single_term_title(); ?></h4>
            <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('dd-search')) : endif; ?>	
            <?php city_posts(); ?>	
        </div>
    </div>

<?php get_footer(); ?>	