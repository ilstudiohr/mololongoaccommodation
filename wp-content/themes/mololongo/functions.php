<?php

/**

 * MoloLongo functions and definitions

 *

 * @package MoloLongo

 */



// Exit if accessed directly.

defined( 'ABSPATH' ) || exit;



// MoloLongo's includes directory.

$mololongo_inc_dir = 'inc';



// Array of files to include.

$mololongo_includes = array(

	// '/helpers.php', 

	'/theme-settings.php',                  // Initialize theme default settings.

	'/setup.php',                           // Theme setup and custom theme supports.

	'/widgets.php',                         // Register widget area.

	'/enqueue.php',                         // Enqueue scripts and styles.

	'/template-tags.php',                   // Custom template tags for this theme.

	'/pagination.php',                      // Custom pagination for this theme.

	'/hooks.php',                           // Custom hooks.

	'/extras.php',                          // Custom functions that act independently of the theme templates.

	'/customizer.php',                      // Customizer additions.

	'/custom-comments.php',                 // Custom Comments file.

	'/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker. Trying to get deeper navigation? Check out: https://github.com/mololongo/mololongo/issues/567.

	'/editor.php',                          // Load Editor functions.

	'/deprecated.php',                      // Load deprecated functions.

);



// Load WooCommerce functions if WooCommerce is activated.

if ( class_exists( 'WooCommerce' ) ) {

	$mololongo_includes[] = '/woocommerce.php';

}



// Load Jetpack compatibility file if Jetpack is activiated.

if ( class_exists( 'Jetpack' ) ) {

	$mololongo_includes[] = '/jetpack.php';

}



// Include files.

foreach ( $mololongo_includes as $file ) {

	require_once get_theme_file_path( $mololongo_inc_dir . $file );

}



function url_get_contents ( $url ) {

    if ( ! function_exists( 'curl_init' ) ) {

        die( 'The cURL library is not installed.' );

    }



    $ch = curl_init();



    curl_setopt( $ch, CURLOPT_URL, $url );

    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );



    $output = curl_exec( $ch );



    curl_close( $ch );



    return $output;

}



add_image_size('search-size', 715, 475, true);

/**

 * Grab latest post title by an author!

 *

 * @param array $data Options for the function.

 * @return string|null Post title for the latest,

 * or null if none.

 */

function mphb_orioly_accommodations( $data ) {
	$args = [
		'posts_per_page' => -1,
		'orderby' => 'menu_order',
		'order' => 'ASC',
		'post_type' => 'mphb_room_type',
		's'	=>	$_GET['q'],
	];

	if($_GET['mphb_ra_grad']) {
		$args['tax_query'] = array(array('taxonomy' => 'mphb_ra_grad', 'field' => 'slug', 'terms' => array($_GET['mphb_ra_grad'])));
	}

	$posts = get_posts( $args);
	$data = [];

	if($_GET['mphb_attributes']['kapacitet'])
		$_GET['capacity'] = $_GET['mphb_attributes']['kapacitet'];


	if($_GET['mphb_attributes']['grad'] == '6') 
		$city = 'Rijeka';
	elseif($_GET['mphb_attributes']['grad'] == '7')
		$city = 'Opatija';
	elseif($_GET['mphb_attributes']['grad'] == '103')
		$city = 'Crikvenica';
	elseif($_GET['mphb_attributes']['grad'] == '111')
		$city = 'Krk';
	else 
		$city = $_GET['mphb_attributes']['grad'];
	if($_GET['t-start'] && $_GET['t-end']) {
		$rentlioIds = '';
		foreach($posts as $post)
		{
			if(get_post_meta($post->ID, 'basic_info_rentliopropertyid', true) && (! strpos($rentlioIds, get_post_meta($post->ID, 'basic_info_rentliopropertyid', true)))) {
				$rentlioIds = $rentlioIds.','.get_post_meta($post->ID, 'basic_info_rentliopropertyid', true);
			}
		}

		$rentlioIds = substr($rentlioIds, 1);
		$rentlioIdsApiUrl = "https://booking.mololongoaccommodation.com/property/availabilities?propertiesIds=".$rentlioIds;

		if($_GET['t-start'] && $_GET['t-end']) {
 			$rentlioIdsApiUrl = $rentlioIdsApiUrl."&dateFrom=" . date("Y-m-d", strtotime($_GET['t-start'])) . "&dateTo=" . date("Y-m-d", strtotime($_GET['t-end']));
 		}

		$rentlioJson = url_get_contents($rentlioIdsApiUrl);
		$rentlioObj = json_decode($rentlioJson);
		//var_dump($rentlioIdsApiUrl);
		//die();
	}
		

	foreach($posts as $post)
	{
		if($_GET['capacity'] && ((int)get_post_meta($post->ID, 'basic_info_basic_capacity', true) + (int)get_post_meta($post->ID, 'basic_info_spare_capacity', true) < (int)$_GET['capacity'])) {
			continue;
		}
		if($_GET['beds'] && ((int)get_post_meta($post->ID, 'basic_info_beds', true) + (int)get_post_meta($post->ID, 'basic_info_spare_beds', true) < (int)$_GET['beds'])) {
			continue;
		}
		if($_GET['bathrooms'] && (get_post_meta($post->ID, 'basic_info_bathrooms', true) < $_GET['bathrooms'])) {
			continue;
		}
		if($_GET['location'] && (stristr(get_post_meta($post->ID, 'basic_info_city', true), $_GET['location']) === FALSE)) {
			continue;
		}
		if($city && $city != get_post_meta($post->ID, 'basic_info_city', true)) {
			continue;
		}
		if($_GET['_sfm_search_amenities'] && is_array($_GET['_sfm_search_amenities'])) {
			foreach ($_GET['_sfm_search_amenities'] as $key => $filter) {
				if(empty(get_post_meta($post->ID, 'amenities_'.$filter, true))) {
					continue 2;
				}
			}
		}
		$rentlioUnitData = null;
		if($_GET['t-start'] && $_GET['t-end']) {
			foreach($rentlioObj ?? [] as $obj) {
				if($obj->id == get_post_meta($post->ID, 'basic_info_rentliopropertyid', true)) {
					foreach($obj->unitTypes as $unitType) {
						if($unitType->id == get_post_meta($post->ID, 'basic_info_rentlioUnitID', true)) {
							$rentlioUnitData = $unitType;
						}
					}
				}
			}
		}

		if($_GET['t-start'] && $_GET['t-end'] && is_null($rentlioUnitData) && $_GET['instant'] === 'on') {
			continue;
		}
		
		if (isset($_GET['price_min']) && isset($_GET['price_max'])) {
			if (is_null($rentlioUnitData) && $_GET['instant'] === 'on') {
				continue;
			} elseif (! is_null($rentlioUnitData)) {
				$priceOk = FALSE;
				foreach($rentlioUnitData->rates ?? [] as $rate) {
					if($rate->name === "WEB rate" || $rate->name === "web rate" || $rate->name === "Web rate") {
						foreach($rate->DailyValues as $dailyValue) {
							if($dailyValue->price >= $_GET['price_min'] && $dailyValue->price <= $_GET['price_max']) {
								$priceOk = TRUE;
							}
						}
					}
				}
				if($priceOk === FALSE) continue;
			}
		}
		if($_GET['t-start'] && $_GET['t-end'])  {
			$checkInDate = new DateTime($_GET['t-start']);
			$checkOutDate = new DateTime($_GET['t-end']);
			$date_diff = $checkOutDate->diff($checkInDate)->format("%a");
			if(get_post_meta($post->ID, 'basic_info_price', true)) {
				$total_price = get_post_meta($post->ID, 'basic_info_price', true) * $date_diff;
			} else {
				$total_price = 0;
			}
		} else {
			$total_price = get_post_meta($post->ID, 'basic_info_price', true);
		}

		if(get_post_meta($post->ID, 'basic_info_rentliopropertyid', true) && get_post_meta($post->ID, 'basic_info_rentlioUnitID', true) && is_null($rentlioUnitData)) {
			continue;
		}


		if($_GET['filter_by_map'] == true && $_GET['map_bounds_east'] && $_GET['map_bounds_north'] && $_GET['map_bounds_south'] && $_GET['map_bounds_west']) {
			$latitude = get_post_meta($post->ID, 'basic_info_latitude', true);
			$logitude = get_post_meta($post->ID, 'basic_info_longitude', true);
			if (! (
				(
					$logitude > $_GET['map_bounds_west'] && 
					$logitude < $_GET['map_bounds_east']
				) && (
					$latitude > $_GET['map_bounds_south'] && 
					$latitude < $_GET['map_bounds_north']
				)
			)) {
				continue;
			}
		}

		$unitUrl = get_permalink($post);
		if($_GET['t-start'] && $_GET['t-end']) {
			$unitUrl  .= "?t-start=".$_GET['t-start']."&t-end=".$_GET['t-end'];
			if(isset($_GET['adults']) && isset($_GET['children'])) {
				$unitUrl  .= "&adults=".$_GET['adults']."&children=".$_GET['children'];
			}
		}


		$unitData = [
			'id'	=>	$post->ID,
			'title'	=>	$post->post_title,
			'content'	=>	$post->post_content,
			'short_content'	=>	mb_strimwidth($post->post_content, 0, 100, "..."),
			'thumbnail'	=>	get_the_post_thumbnail_url($post->ID, 'search-size'),
			'aduts'	=>	1,
			'url'	=>	$unitUrl,
			'total_price'	=>	$total_price,
			'basic_info_address'	=>	get_post_meta($post->ID, 'basic_info_address', true),
			'basic_info_city'	=>	get_post_meta($post->ID, 'basic_info_city', true),
			'basic_info_stars'	=>	get_post_meta($post->ID, 'basic_info_stars', true),
			'basic_info_price'	=>	get_post_meta($post->ID, 'basic_info_price', true),
			'basic_info_latitude'	=>	get_post_meta($post->ID, 'basic_info_latitude', true),
			'basic_info_longitude'	=>	get_post_meta($post->ID, 'basic_info_longitude', true),
			'basic_info_basic_capacity'	=>	get_post_meta($post->ID, 'basic_info_basic_capacity', true),
			'basic_info_spare_capacity'	=>	get_post_meta($post->ID, 'basic_info_spare_capacity', true),
			'basic_info_beds'	=>	get_post_meta($post->ID, 'basic_info_beds', true),
			'basic_info_spare_beds'	=>	get_post_meta($post->ID, 'basic_info_spare_beds', true),
			'basic_info_bathrooms'	=>	get_post_meta($post->ID, 'basic_info_bathrooms', true),
			'basic_info_rentlio_engine'	=>	get_post_meta($post->ID, 'basic_info_rentlio_engine', true),
			'amenities_to_restaurant'	=>	get_post_meta($post->ID, 'amenities_breakfast', true),
			'amenities_to_restaurant'	=>	get_post_meta($post->ID, 'amenities_to_beach', true),
			'amenities_to_restaurant'	=>	get_post_meta($post->ID, 'amenities_to_restaurant', true),
			'amenities_to_atm'	=>	get_post_meta($post->ID, 'amenities_to_atm', true),
			'amenities_to_parking'	=>	get_post_meta($post->ID, 'amenities_to_parking', true),
			'mphb_adults_capacity'	=>	get_post_meta($post->ID, 'mphb_adults_capacity', true),
			'mphb_children_capacity'	=>	get_post_meta($post->ID, 'mphb_children_capacity', true),
			'mphb_size'	=>	get_post_meta($post->ID, 'mphb_size', true),
			'mphb_view'	=>	get_post_meta($post->ID, 'mphb_view', true),
			'mphb_bed'	=>	get_post_meta($post->ID, 'mphb_bed', true),
			'mphb_gallery'	=>	get_post_meta($post->ID, 'mphb_gallery', true),
			'mphb_services'	=>	get_post_meta($post->ID, 'mphb_services', true),
			'mphb_ra_grad'	=>	get_the_terms($post->ID, 'mphb_ra_grad'),
			'basic_info_rentliopropertyid' =>	 get_post_meta($post->ID, 'basic_info_rentliopropertyid', true),
			'basic_info_rentlioUnitID'	=> get_post_meta($post->ID, 'basic_info_rentlioUnitID', true),
			'rentlio_data'	=> $rentlioUnitData,
			'meta'	=>	get_post_meta($post->ID),
		];
		if($rentlioUnitData) {
			array_unshift($data, $unitData);
		} else {
			$data[] = $unitData;
		}
	}


	$response = [
		'request' => $_GET,
		// 'rentlio_api_url'	=>	$rentlioIdsApiUrl,
		// 'rentlio_api_response'	=>	$rentlioJson,
		'total_results'	=>	count($data),
		'current_page'	=>	$_GET['page'] ?? '1',
		'per_page'	=>	50,
		'data'	=>	array_slice($data, (($_GET['page'] ?? 1) - 1) * 50, 50),
        ];
	
	return $response;
}


add_action( 'rest_api_init', function () {
	register_rest_route( 'mphb_orioly/v1', '/accommodations', array(
		'methods' => 'GET',
		'callback' => 'mphb_orioly_accommodations',
	) );
} );

function mphb_orioly_login($data) {
	if($_POST['user_login'] && $_POST['user_password']) {
	  $user = wp_signon([
		    'user_login' => $_POST['user_login'],
		    'user_password' => $_POST['user_password'],
			'remember' => false,
	  ], false);
	  if($user->id) {
		wp_set_current_user($user->id);
	  	return wp_send_json([
			'nonce' =>	$nonce = wp_create_nonce('wp_rest'),
	  		'user'	=>	wp_get_current_user(),
			'loginCookie'	=>	wp_set_auth_cookie($user->ID, true),
		]); 
	  } else {
	  	wp_send_json(['message' => 'Login information is incorrect.'], 422);
	  }
	} else {
		wp_send_json(['message' => 'user_login and user_password is required.'], 422);
	}
}

add_action( 'rest_api_init', function () {
	register_rest_route( 'mphb_orioly/v1', '/login', array(
		'methods' => 'POST',
		'callback' => 'mphb_orioly_login',
	) );
} );
