<?php
/**
 * The header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package MoloLongo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'mololongo_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css"/>
    <script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> <?php mololongo_body_attributes(); ?>>
<?php do_action( 'wp_body_open' ); ?>
	<header id="wrapper-navbar" class="header-transparent">

	    <div class="top-header">
            <div class="container">
                <div class="row align-items-md-center">
                    <div class="col-md-6">
                    </div>
                    
                    <div class="col-md-6 d-flex justify-content-end align-items-center">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?><?php _e('iznajmite-nekretninu', 'molonew'); ?>" class="mr-4 button-primary"><?php _e('Iznajmite nekretninu', 'molonew'); ?></a>
                        <?php do_action('wpml_add_language_selector'); ?>
                    </div>
                </div>
            </div>
        </div>

		<a class="skip-link sr-only sr-only-focusable" href="#content"><?php esc_html_e( 'Skip to content', 'mololongo' ); ?></a>

		<nav id="main-nav" class="navbar navbar-expand-lg" aria-labelledby="main-nav-label">

		<?php if ( 'container' === $container ) : ?>
			<div class="container">
		<?php endif; ?>
        		<div class="col-md-4 mobile-hidden">
        		    <ul class="social-header">
        		        <li>
        		            <a href="https://www.instagram.com/mololongoapartments/" target="_blank">
        		            <?php if(is_home() || is_front_page()):?>
        		            <img width="18" src="<?php bloginfo('template_directory'); ?>/images/instagram_b.svg" class="img-fluid" alt="<?php the_title();?>">
        		            <?php else:?>
        		            <img width="18" src="<?php bloginfo('template_directory'); ?>/images/instagram.svg" class="img-fluid" alt="<?php the_title();?>">
        		            <?php endif;?>
        		            </a>
    		            </li>
        		        <li>
        		            <a href="https://www.facebook.com/mololongoapartments" target="_blank">
        		                <?php if(is_home() || is_front_page()):?>
        		                <img width="18" src="<?php bloginfo('template_directory'); ?>/images/facebook_b.svg" class="img-fluid" alt="<?php the_title();?>">
        		                <?php else:?>
            		            <img width="18" src="<?php bloginfo('template_directory'); ?>/images/facebook.svg" class="img-fluid" alt="<?php the_title();?>">
            		            <?php endif;?>
    		                </a>
		                </li>
        		        <li>
        		            <a href="https://www.linkedin.com/company/molo-longo" target="_blank">
        		                <?php if(is_home() || is_front_page()):?>
        		                <img width="18" src="<?php bloginfo('template_directory'); ?>/images/linkedin_b.svg" class="img-fluid" alt="<?php the_title();?>">
        		                 <?php else:?>
            		            <img width="18" src="<?php bloginfo('template_directory'); ?>/images/linkedin.svg" class="img-fluid" alt="<?php the_title();?>">
            		            <?php endif;?>
        		            </a>
    		            </li>
        		    </ul>
        		</div>
        		<div class="col-md-4 text-center text-left-sm">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand custom-logo-link" title="<?php the_title();?>" rel="home">
        			    <img width="255" src="<?php bloginfo('template_directory'); ?>/images/mololongoaccommodation_bijeli.png" class="img-fluid" alt="<?php the_title();?>">
			    </a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="<?php esc_attr_e( 'Toggle navigation', 'mololongo' ); ?>">
					<span class="navbar-toggler-icon"><img width="55" src="<?php bloginfo('template_directory'); ?>/images/hamb.svg" class="img-fluid" alt="<?php the_title();?>"></span>
				</button>
        	    </div>
                <div class="col-md-4">
				<!-- The WordPress Menu goes here -->
				<?php
				wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'container_class' => 'collapse navbar-collapse',
						'container_id'    => 'navbarNavDropdown',
						'menu_class'      => 'navbar-nav ml-auto',
						'fallback_cb'     => '',
						'menu_id'         => 'main-menu',
						'depth'           => 2,
						'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
					)
				);
				?>
				</div>
				

				
				<!-- <div class="safe-logo">
				    <img src="<?php echo get_template_directory_uri(); ?>/images/safe.png">
				</div> -->
			<?php if ( 'container' === $container ) : ?>
			</div><!-- .container -->
			<?php endif; ?>

		</nav><!-- .site-navigation -->

	</header><!-- #wrapper-navbar end -->
    <main>