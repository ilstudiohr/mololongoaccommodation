<?php
/**
 * Temeljna konfiguracija WordPressa.
 *
 * wp-config.php instalacijska skripta koristi ovaj zapis tijekom instalacije.
 * Ne morate koristiti web stranicu, samo kopirajte i preimenujte ovaj zapis
 * u "wp-config.php" datoteku i popunite tražene vrijednosti.
 *
 * Ovaj zapis sadrži sljedeće konfiguracije:
 *
 * * MySQL postavke
 * * Tajne ključeve
 * * Prefiks tablica baze podataka
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL postavke - Informacije možete dobiti od vašeg web hosta ** //
/** Ime baze podataka za WordPress */
define('DB_NAME', "mololong_stage-acc");

/** MySQL korisničko ime baze podataka */
define('DB_USER', "mololong_sta-acc");

/** MySQL lozinka baze podataka */
define('DB_PASSWORD', "n0tG~wd]PCy[");

/** MySQL naziv hosta */
define('DB_HOST', "localhost");

/** Kodna tablica koja će se koristiti u kreiranju tablica baze podataka. */
define('DB_CHARSET', 'utf8');

/** Tip sortiranja (collate) baze podataka. Ne mijenjate ako ne znate što radite. */
define('DB_COLLATE', '');

/**#@+
 * Jedinstveni Autentifikacijski ključevi (Authentication Unique Keys and Salts).
 *
 * Promijenite ovo u vaše jedinstvene fraze!
 * Ključeve možete generirati pomoću {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org servis tajnih-ključeva}
 * Ključeve možete promijeniti bilo kada s tim da će se svi korisnici morati ponovo prijaviti jer kolačići (cookies) neće više važiti nakon izmjene ključeva.
 *
 * @od inačice 2.6.0
 */
define('AUTH_KEY',         'lz+]L8#U(;p7z6&-L]]/MzIt-!5>~-TS/y6534oI_F+Ld$>pFYp7ZUjS#G)-gLPl');
define('SECURE_AUTH_KEY',  '=.+=EBJ<o]KX4TCS`N|jOtY /%TWSh+G0tghiC_}g[lS{?I?OAbW&Y*bwv=pUM/E');
define('LOGGED_IN_KEY',    '>k8+>:5D@Q(Ec8H}Ff!cc~^F_/[RjD[?9T3p6(*^^SZtdZOvM_O#y$`M#1]%e?GQ');
define('NONCE_KEY',        'x<[D*C#!DN:a-1I*46}u,qG$E)KZ4BA*!Ov?rFozg(w+Vf{wRq=~}iqVzK-||7W!');
define('AUTH_SALT',        '$o:<[*6r9`-N+%ao0J*^UZ]JxiA-WX)tY?R23=~aXw2|h/tQEcr8))TcVk|[{aoE');
define('SECURE_AUTH_SALT', 'F%5{1JCA~h8/w_ah-C?e--w>Idaj}#W[b~012c|;lHH|TY@<EL||M1;V)d=A~F0 ');
define('LOGGED_IN_SALT',   '9s9kp&S[B5&fDYj1.}{sP)BLk$0{+20|D%O&Q?<eK;}Z$D!Al.PRZG!c5PY@@Ax{');
define('NONCE_SALT',       '9[O 3Ysw#aVdqmyx;)zuw_Kc./(=3?w4!+>zbL!(;;MFv>[x--bQ`+@)O-W+z2bU');

/**#@-*/

/**
 * Prefix WordPress tablica baze podataka.
 *
 * Možete imati više instalacija unutar jedne baze podataka ukoliko svakoj dodjelite
 * jedinstveni prefiks. Koristite samo brojeve, slova, i donju crticu!
 */
$table_prefix  = 'wp_';

/**
 * Za programere: WordPress debugging mode.
 *
 * Promijenit ovo u true kako bi omogućili prikazivanje poruka tijekom razvoja.
 * Izrazito preporučujemo da programeri dodataka (plugin) i tema
 * koriste WP_DEBUG u njihovom razvojnom okružju.
 *
 * Za informacije o drugim konstantama koje se mogu koristiti za debugging,
 * posjetite Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
set_time_limit(600);

/** Memory Limit */
define('WP_MEMORY_LIMIT', '512M');
define( 'WP_MAX_MEMORY_LIMIT', '640M' );

/* To je sve, ne morate više ništa mijenjati! Sretno bloganje. */

/** Apsolutna putanja do WordPress mape. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Postavke za WordPress varijable i već uključene zapise. */
require_once(ABSPATH . 'wp-settings.php');
